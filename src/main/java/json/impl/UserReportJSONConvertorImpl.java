package json.impl;

import Domain.Employee;
import Domain.PlanCompareElement;
import Domain.ReportRecord;
import Domain.SuperViewerUnitedReportElement;
import Domain.UnitedReport;
import Domain.UnitedReportElement;
import Domain.UserReport;
import Domain.WorkInstance;
import static Service.Impl.SprUsrPnlExchSrvIml.saveDetailedReportToExcel_State;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import json.UserReportJSONConvertor;
import json.UserReportRecordsLineJSONConvertor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import yyy.objectPools.WorkInstancePool;
import yyy.util.DatesProcessor;
import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.json.simple.JSONArray;
import yyy.util.WebLogger;

public class UserReportJSONConvertorImpl implements UserReportJSONConvertor {

    private ServletContext servletContext;

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public JSONObject toJSON(UserReport userReport) {
        UserReportRecordsLineJSONConvertor userReportRecordsLineJSONConvertor = (UserReportRecordsLineJSONConvertor) servletContext
                .getAttribute("userReportRecordsLineJSONConvertor");

        JSONObject userReportJSON = new JSONObject();

        userReportJSON.put("date_from", userReport.DATE_FROM.getTime());
        userReportJSON.put("date_to", userReport.DATE_TO.getTime());

        Map userReportRow = userReport.getUserReportRows();
        for (Object object : userReportRow.keySet()) {
            WorkInstance workInstance = (WorkInstance) object;
            JSONObject userReportRecordsLineJSON = userReportRecordsLineJSONConvertor.toJSON(userReport, workInstance);
            userReportJSON.put("wi" + workInstance.getId(), userReportRecordsLineJSON);
        }
        return userReportJSON;
    }

    @Override
    public JSONObject unitedReportToJSON(UnitedReport unitedReport) {
        //ObjectMapper mapper = new ObjectMapper();
        JSONObject userReportJSON = new JSONObject();
        Map<String, UnitedReportElement> unRep = unitedReport.getUnRep();
        for (Map.Entry<String, UnitedReportElement> entry : unRep.entrySet()) {
            UnitedReportElement unitedReportElement = entry.getValue();
            JSONObject unitedReportElementObj = new JSONObject();
            unitedReportElementObj.put("union_row_id", unitedReportElement.getUnion_row_id());
            unitedReportElementObj.put("workType", unitedReportElement.getWorkType());
            unitedReportElementObj.put("workTitle", unitedReportElement.getWorkTitle());
            unitedReportElementObj.put("workForm", unitedReportElement.getWorkForm());
            unitedReportElementObj.put("workInstance", unitedReportElement.getWorkInstance());
            unitedReportElementObj.put("unionHours", unitedReportElement.getUnionHours());
            userReportJSON.put(entry.getKey(), unitedReportElementObj);
            //String jsonInString = mapper.writeValueAsString(unitedReportElement); // put /" instead " -> javasript can't read this object correct
            //userReportJSON.put(entry.getKey(), jsonInString);
        }

        return userReportJSON;
    }

    @Override
    public UserReport fromJSON(String json) {
        WorkInstancePool workInstancePool = (WorkInstancePool) servletContext.getAttribute("workInstancePool");
        DatesProcessor datesProcessor = DatesProcessor.getInstance();

        JSONParser parser = new JSONParser();
        Date dateFrom = null;
        Date dateTo = null;
        Set<ReportRecord> reportRecords = new HashSet<>();

        try {
            JSONObject userReportJSON = ((JSONObject) parser.parse(json));
            Set<Map.Entry> reportJSONEntries = userReportJSON.entrySet();

            for (Map.Entry<String, Object> reportJSONEntry : reportJSONEntries) {
                if (reportJSONEntry.getKey().startsWith("wi")) {

                    JSONObject userReportRowJSON = (JSONObject) reportJSONEntry.getValue();
                    Set<Map.Entry> rowJSONEntries = userReportRowJSON.entrySet();

                    for (Map.Entry<String, JSONObject> rowJSONEntry : rowJSONEntries) {
                        if (rowJSONEntry.getKey().startsWith("rr")) {

                            JSONObject reportRecordJSON = rowJSONEntry.getValue();
                            ReportRecord reportRecord = new ReportRecord();

                            Date dateReported = null;
                            try {
                                long dateReportedJSON = (long) reportRecordJSON.get("date_reported");
                                dateReported = new Date(dateReportedJSON);
                            } catch (ClassCastException e) {
                                //WebLogger.writeError(e.getMessage(), UserReportJSONConvertorImpl.class);
                                String dateReportedJSON = (String) reportRecordJSON.get("date_reported");
                                dateReported = datesProcessor.getLocalDate(dateReportedJSON);
                            }
                            reportRecord.setDateReported(dateReported);

                            reportRecord.setHoursNum((int) (long) reportRecordJSON.get("hours_num"));

                            int workInstanceReferredId = (int) (long) reportRecordJSON.get("work_instance_id");
                            WorkInstance workInstanceReferred = workInstancePool.get(workInstanceReferredId);
                            reportRecord.setWorkInstance(workInstanceReferred);

                            reportRecords.add(reportRecord);
                        }
                    }

                } else if (reportJSONEntry.getKey().equals("date_from")) {

                    String dateFromJSON = (String) reportJSONEntry.getValue();
                    dateFrom = datesProcessor.getLocalDate(dateFromJSON);

                } else if (reportJSONEntry.getKey().equals("date_to")) {

                    String dateToJSON = (String) reportJSONEntry.getValue();
                    dateTo = datesProcessor.getLocalDate(dateToJSON);

                }
            }
        } catch (ParseException e) {
            WebLogger.writeError(e.getMessage(), UserReportJSONConvertorImpl.class);
        }

        return new UserReport(dateFrom, dateTo, reportRecords);
    }

    @Override
    public UnitedReport fromJSONUnited(String json) {
        UnitedReport unitedReport = new UnitedReport();
        ObjectMapper mapper = new ObjectMapper();

        try {
            Map<String, UnitedReportElement> map = mapper.readValue(json, new TypeReference<Map<String, UnitedReportElement>>() {
            });
            unitedReport.setUnRep(map);
        } catch (IOException e) {
            WebLogger.writeError(e.getMessage(), UserReportJSONConvertorImpl.class);
        }

        return unitedReport;
    }

    @Override
    public JSONArray employeesListToJSON(List<Employee> employees) {
        JSONArray jsonArray = new JSONArray();
        for (Employee employee : employees) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", employee.getId());
            jsonObject.put("name", employee.getName());
            jsonObject.put("account", employee.getAccount());
            jsonObject.put("role", employee.getRole());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @Override
    public List<Integer> fromJSONSelectedEmployees(String selectedEmployeesID_JSON) {
        List<Integer> res = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            res = mapper.readValue(selectedEmployeesID_JSON, new TypeReference<List<Integer>>() {
            });
        } catch (IOException ex) {
            WebLogger.writeError(ex.getMessage(), UserReportJSONConvertorImpl.class);
        }
        return res;
    }

    @Override
    public JSONObject toJSON(Map<Integer, Integer> superViewerReport) {
        JSONObject userReportJSON = new JSONObject();
        userReportJSON.putAll(superViewerReport);
        return userReportJSON;
    }

    @Override
    public Map<String, SuperViewerUnitedReportElement> fromJSONSuperViewerUnited(String mySuperViewerUnitedReport_JSON) {
        Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport = new TreeMap<>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            superViewerUnitedReport = mapper.readValue(mySuperViewerUnitedReport_JSON, new TypeReference<Map<String, SuperViewerUnitedReportElement>>() {
            });
        } catch (IOException e) {
            WebLogger.writeError(e.getMessage(), UserReportJSONConvertorImpl.class);
            saveDetailedReportToExcel_State = e.getMessage();
        }
//        Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport = new TreeMap<>();
//
//        JSONParser parser = new JSONParser();
//        try {
//            JSONObject jsonObject1 = ((JSONObject) parser.parse(mySuperViewerUnitedReport_JSON));
//            Set<Map.Entry> reportJSONEntries1 = jsonObject1.entrySet();
//            for (Map.Entry<String, Object> entry1 : reportJSONEntries1) {
//                SuperViewerUnitedReportElement superViewerUnitedReportElement = new SuperViewerUnitedReportElement();
//                JSONObject jsonObject2 = (JSONObject) entry1.getValue();
//
//                superViewerUnitedReportElement.setUnion_row_id(jsonObject2.get("union_row_id").toString());
//                superViewerUnitedReportElement.setWorkType(jsonObject2.get("workType").toString());
//                superViewerUnitedReportElement.setWorkTitle(jsonObject2.get("workTitle").toString());
//                superViewerUnitedReportElement.setWorkForm(jsonObject2.get("workForm").toString());
//                superViewerUnitedReportElement.setWorkInstance(jsonObject2.get("workForm").toString());
//
//                JSONObject jsonObject3 = (JSONObject) jsonObject2.get("selectedEmployeesHoursInstance");
//                Map<String, Integer> selectedEmployeesHoursInstance = new TreeMap<>();
//                selectedEmployeesHoursInstance.putAll(jsonObject3);
//
//                superViewerUnitedReportElement.setSelectedEmployeesHoursInstance(selectedEmployeesHoursInstance);
//
//                superViewerUnitedReport.put(entry1.getKey(), superViewerUnitedReportElement);
//            }
//
//        } catch (ParseException e) {
//            WebLogger.writeError(e.getMessage(), UserReportJSONConvertorImpl.class);
//        }

        return superViewerUnitedReport;
    }

    @Override
    public JSONObject toJSONSuperViewerUnitedReport(Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport) {
        JSONObject superViewerUnitedReport_JSON = new JSONObject();
        for (Map.Entry<String, SuperViewerUnitedReportElement> entry : superViewerUnitedReport.entrySet()) {

            SuperViewerUnitedReportElement superViewerUnitedReportElement = entry.getValue();

            JSONObject superViewerUnitedReportElement_JSON = new JSONObject();
            superViewerUnitedReportElement_JSON.put("union_row_id", superViewerUnitedReportElement.getUnion_row_id());
            superViewerUnitedReportElement_JSON.put("workType", superViewerUnitedReportElement.getWorkType());
            superViewerUnitedReportElement_JSON.put("workTitle", superViewerUnitedReportElement.getWorkTitle());
            superViewerUnitedReportElement_JSON.put("workForm", superViewerUnitedReportElement.getWorkForm());
            superViewerUnitedReportElement_JSON.put("workInstance", superViewerUnitedReportElement.getWorkInstance());

            JSONObject getSelectedEmployeesHoursInstance_JSON = new JSONObject();
            getSelectedEmployeesHoursInstance_JSON.putAll(superViewerUnitedReportElement.getSelectedEmployeesHoursInstance());
            superViewerUnitedReportElement_JSON.put("selectedEmployeesHoursInstance", getSelectedEmployeesHoursInstance_JSON);

            superViewerUnitedReport_JSON.put(entry.getKey(), superViewerUnitedReportElement_JSON);
        }

        return superViewerUnitedReport_JSON;
    }

    @Override
    public JSONArray planCompareListToJSON(List<PlanCompareElement> planCompareList) {
        JSONArray jsonArray = new JSONArray();
        for (PlanCompareElement planCompareElement : planCompareList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("workTitle", planCompareElement.getWorkTitle());
            jsonObject.put("completedHours", planCompareElement.getCompletedHours());
            jsonObject.put("planHours", planCompareElement.getPlanHours());
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    @Override
    public JSONArray selectedEmployeesIDToJSON(List<Integer> selectedEmployeesID) {
        JSONArray jsonArray = new JSONArray();
//        for (Integer el : selectedEmployeesID) {
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("id", el);
//            jsonArray.add(jsonObject);
//        }
        jsonArray.addAll(selectedEmployeesID);
        return jsonArray;
    }
}
