package json;

import Domain.Employee;
import Domain.PlanCompareElement;
import Domain.SuperViewerUnitedReportElement;
import Domain.UnitedReport;
import Domain.UserReport;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONObject;
import javax.servlet.ServletContext;
import org.json.simple.JSONArray;

public interface UserReportJSONConvertor {
    public void setServletContext(ServletContext servletContext);
    public JSONObject toJSON (UserReport userReport);
    public JSONObject toJSON (Map<Integer, Integer> superViewerReport);
    public JSONObject unitedReportToJSON (UnitedReport unitedReport);
    public JSONArray employeesListToJSON (List<Employee> employees);
    public UserReport fromJSON (String json);
    public UnitedReport fromJSONUnited (String json);

    public List<Integer> fromJSONSelectedEmployees(String selectedEmployeesID_JSON);

    public Map<String, SuperViewerUnitedReportElement> fromJSONSuperViewerUnited(String mySuperViewerUnitedReport_JSON);

    public JSONObject toJSONSuperViewerUnitedReport(Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport);

    public JSONArray planCompareListToJSON(List<PlanCompareElement> planCompareList);

    public JSONArray selectedEmployeesIDToJSON(List<Integer> selectedEmployeesID);
}
