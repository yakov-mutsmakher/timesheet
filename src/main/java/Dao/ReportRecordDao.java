package Dao;

import Domain.Employee;
import Domain.ReportRecord;
import Domain.UnitedReport;

import javax.servlet.ServletContext;
import java.util.Date;
import java.util.Set;

public interface ReportRecordDao {

    Set<ReportRecord> getRecordSet(Employee employee, Date dateFrom, Date dateTo);

    Set<ReportRecord> getSummaryRecordSet(Employee employee, Date dateFrom, Date dateTo);

    UnitedReport getUnitedReport(Employee employee, Date dateFrom, Date dateTo, UnitedReport unitedReport);

    void setServletContext(ServletContext servletContext);

    Integer updateRecords(Set<ReportRecord> reportRecordSet, Employee employee);
}
