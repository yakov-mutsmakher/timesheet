package Dao;

import Domain.Employee;
import Domain.PlanCompareElement;
import Domain.ReportRecord;
import Domain.SuperViewerUnitedReportElement;
import Domain.UserReport;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletContext;

public interface SuperViewerDAO {
    
    void setServletContext(ServletContext servletContext);

    public List<Employee> getEmployees(String onlyActive, Employee currentEmployee);

    public Map<Integer, Integer> getSuperViewerReport(Date dateFrom, Date dateTo, List<Integer> selectedEmployeesID);

    public Map<String, SuperViewerUnitedReportElement> getSuperViewerUnitedReport(Date dateFrom, Date dateTo, List<Integer> selectedEmployeesID, Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport);

    public List<List<String>> getDetailedReport(Date dateFrom, Date dateTo, List<Employee> selectedEmployeesList, String groupBy);

    public List<Employee> getEmployeesByID(List<Integer> selectedEmployeesID);

    public List<PlanCompareElement> getPlanCompare(String yearPlanCompare);
    
}
