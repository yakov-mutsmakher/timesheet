package Dao.Impl;

import Dao.AdminPageDao;
import Domain.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import yyy.util.DatesProcessor;
import yyy.util.WebLogger;

public class AdminPageDaoImpl implements AdminPageDao {

    private ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    //this is test method: there was problem with many connections (but in DOCs getConnection() has already synchronized?)
    private synchronized Connection getConnection() throws SQLException {
        DataSource rootConnDataSource = (DataSource) servletContext.getAttribute("rootConnDataSource");
        return rootConnDataSource.getConnection();
    }

    @Override
    public String updateDataByPlanData(Map<String, Integer> planData) {
        DatesProcessor datesProcessor = DatesProcessor.getInstance();
        String MySQLFormatDate = datesProcessor.todayMySQLFormatDate();
        List<String> existTitles = new ArrayList<>();
        String res = "error_updating_DB";
        Connection conn = null;
        int updateRowsResult = 0;
        int insertRowsResult = 0;
        StringBuilder strb = new StringBuilder();
        Boolean firstValue = true;
        for (int i = 0; i < planData.size(); i++) {
            if (firstValue) {
                strb.append("?");
                firstValue = false;
            } else {
                strb.append(",?");
            }
        }

        try {
            conn = getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement("select title from work_title where title in (" + strb.toString() + ")");
            int counter = 0;
            for (Map.Entry<String, Integer> entry : planData.entrySet()) {
                counter++;
                preparedStatement.setString(counter, entry.getKey());
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                existTitles.add(resultSet.getString("title"));
            }
            preparedStatement.close();

            PreparedStatement ps_update = conn.prepareStatement("UPDATE work_title SET hours_from_plan =?, date_created = ? WHERE title = ? AND (hours_from_plan <> ? OR hours_from_plan is null)");
            PreparedStatement ps_insert = conn.prepareStatement("INSERT INTO work_title (title, work_type_id, hours_from_plan, date_created) VALUES (?,1,?,?)");
            for (Map.Entry<String, Integer> entry : planData.entrySet()) {
                if (existTitles.contains(entry.getKey())) {
                    ps_update.setInt(1, entry.getValue());
                    ps_update.setString(2, MySQLFormatDate);
                    ps_update.setString(3, entry.getKey());
                    ps_update.setInt(4, entry.getValue());
                    ps_update.addBatch();
                } else {
                    ps_insert.setString(1, entry.getKey());
                    ps_insert.setInt(2, entry.getValue());
                    ps_insert.setString(3, MySQLFormatDate);
                    ps_insert.addBatch();
                }
            }
            int[] checkUpdatedRows = ps_update.executeBatch();
            for (int el : checkUpdatedRows) {
                if (el == 1) {
                    updateRowsResult++;
                }
            }
            insertRowsResult = ps_insert.executeBatch().length;
            res = planData.size() + "," + updateRowsResult + "," + insertRowsResult;
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), AdminPageDaoImpl.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), AdminPageDaoImpl.class);
                }
            }
        }
        return res;
    }

    @Override
    public String changeRole(Integer id, String oldRole, String newRole, List<Integer> selectedEmployeesID) {
        String result = "error_updating_DB";
        int queryRes = 0;
        Boolean existOldAdminRole = false;
        Connection conn = null;
        try {
            conn = getConnection();
            if (oldRole.equals("admin")) {
                PreparedStatement psDeleteOldAdminRole = conn.prepareStatement("DELETE FROM super_viewer_show_tree WHERE super_viewer_id = ? ;");
                psDeleteOldAdminRole.setInt(1, id);
                queryRes = psDeleteOldAdminRole.executeUpdate();
                existOldAdminRole = true;
                psDeleteOldAdminRole.close();
            }
            if (queryRes > 0 || !existOldAdminRole) {
                PreparedStatement psChangeRole = conn.prepareStatement("UPDATE role_by_employee SET role_id = (SELECT id FROM role where title = ? ) WHERE employee_id= ? ;");
                psChangeRole.setString(1, newRole);
                psChangeRole.setInt(2, id);
                queryRes = psChangeRole.executeUpdate();
                psChangeRole.close();
                if (queryRes > 0) {
                    if (newRole.equals("admin")) {
                        Statement stmt = conn.createStatement();
                        for (Integer el : selectedEmployeesID) {
                            stmt.addBatch("INSERT INTO super_viewer_show_tree (super_viewer_id, id_for_showing) VALUES (" + id + ", " + el + ");");
                        }
                        int[] resAdding = stmt.executeBatch();
                        stmt.close();
                        if (resAdding.length == selectedEmployeesID.size()) {
                            result = "successfully_changed";
                        } else {
                            result = "error_with_adding_new_admin_roles";
                        }
                    } else {
                        result = "successfully_changed";
                    }
                }
            } else {
                result = "error_with_deleting_old_admin_roles";
            }
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), AdminPageDaoImpl.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), AdminPageDaoImpl.class);
                }
            }
        }

        return result;
    }

    @Override
    public List<Integer> adminSelectedID(Integer id) {
        List<Integer> result = new ArrayList<>();
        Connection conn = null;
        try {
            conn = getConnection();
            PreparedStatement statement = conn.prepareStatement("SELECT id_for_showing FROM super_viewer_show_tree WHERE super_viewer_id = ? ;");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(resultSet.getInt(1));
            }
            statement.close();
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), AdminPageDaoImpl.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), AdminPageDaoImpl.class);
                }
            }
        }
        return result;
    }

    @Override
    public String addNewEmployee(String login, String newEmployeeName, String newRoleAdd, List<Integer> selectedEmployeesIDAdd) {
        String result = "Error: updating DB";
        boolean userAlreadyExist = false;
        int roleID = -1;

        Connection conn = null;
        try {
            conn = getConnection();

            PreparedStatement statement = conn.prepareStatement("SELECT * FROM employee WHERE account = ? ;");
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                userAlreadyExist = true;
            }
            statement.close();

            PreparedStatement psGetRoleID = conn.prepareStatement("SELECT id FROM role WHERE title = ? ;");
            psGetRoleID.setString(1, newRoleAdd);
            resultSet = psGetRoleID.executeQuery();
            while (resultSet.next()) {
                roleID = resultSet.getInt("id");
            }
            psGetRoleID.close();

            if (userAlreadyExist) {
                result = "userAlreadyExist";
            } else {
                conn.setAutoCommit(false);
                PreparedStatement psUpdateEmployee = conn.prepareStatement("INSERT INTO employee (name, account) VALUES (?, ?);", Statement.RETURN_GENERATED_KEYS);
                psUpdateEmployee.setString(1, newEmployeeName);
                psUpdateEmployee.setString(2, login);
                int countInsertedRows = psUpdateEmployee.executeUpdate();
                if (countInsertedRows == 1) {
                    ResultSet generatedEmployeeID = psUpdateEmployee.getGeneratedKeys();
                    if (generatedEmployeeID.next()) {
                        int employee_id = generatedEmployeeID.getInt(1);
                        PreparedStatement psUpdateRole = conn.prepareStatement("INSERT INTO role_by_employee (employee_id, role_id) VALUES (?, ?);");
                        psUpdateRole.setInt(1, employee_id);
                        psUpdateRole.setInt(2, roleID);
                        psUpdateRole.executeUpdate();
                        psUpdateRole.close();

                        if (newRoleAdd.equals("admin")) {
                            PreparedStatement psUpdateAdminRoleTree = conn.prepareStatement("INSERT INTO super_viewer_show_tree (super_viewer_id, id_for_showing) VALUES (?, ?);");
                            for (Integer el : selectedEmployeesIDAdd) {
                                psUpdateAdminRoleTree.setInt(1, employee_id);
                                psUpdateAdminRoleTree.setInt(2, el);
                                psUpdateAdminRoleTree.addBatch();
                            }
                            int[] resAdding = psUpdateAdminRoleTree.executeBatch();
                            psUpdateAdminRoleTree.close();
                            if (resAdding.length == selectedEmployeesIDAdd.size()) {
                                result = "Successfully added";
                            }
                            psUpdateAdminRoleTree.close();
                        } else {
                            result = "Successfully added";
                        }
                    }
                }
                psUpdateEmployee.close();
                conn.setAutoCommit(true);
            }

        } catch (SQLException e) {
            try {
                conn.rollback();
                conn.setAutoCommit(true);
            } catch (SQLException ex) {
                WebLogger.writeError(e.getMessage(), AdminPageDaoImpl.class);
            }
            WebLogger.writeError(e.getMessage(), AdminPageDaoImpl.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), AdminPageDaoImpl.class);
                }
            }
        }

        return result;
    }

}
