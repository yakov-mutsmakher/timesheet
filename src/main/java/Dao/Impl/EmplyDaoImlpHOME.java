package Dao.Impl;

import Dao.EmployeeDao;
import Domain.Employee;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import yyy.util.WebLogger;

public class EmplyDaoImlpHOME implements EmployeeDao {

    private ServletContext servletContext;
    
    //this is test method: there was problem with many connections (but in DOCs getConnection() has already synchronized?)
    private synchronized Connection getConnection() throws SQLException{
        DataSource rootConnDataSource = (DataSource) servletContext.getAttribute("rootConnDataSource");
        return rootConnDataSource.getConnection();
    }

    @Override
    public Employee getEmployeeByCode(String emplAccount) {
        int id = 0;
        String name = "unknown";
        String role = "unknown";
        
        Connection conn = null;
        try {
            conn = getConnection();
            PreparedStatement statement = conn.prepareStatement("SELECT employee.id, employee.name, employee.account, role.title "
                    + "FROM employee, role_by_employee, role where employee.account= ? and "
                    + "employee.id=role_by_employee.employee_id and role_by_employee.role_id=role.id;");
            statement.setString(1, emplAccount);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getInt("id");
                name = resultSet.getString("name");
                role = resultSet.getString("title");
            }
            statement.close();
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), EmplyDaoImlpHOME.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), EmplyDaoImlpHOME.class);
                }
            }
        }
        Employee employee = new Employee(id, name, emplAccount, role);
        WebLogger.writeInfo(employee.getAccount() + " logged", EmplyDaoImlpHOME.class);
        return employee;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
}
