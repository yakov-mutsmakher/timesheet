package Dao.Impl;

import Dao.DBProceduresManager;
import static Service.Impl.SprUsrPnlExchSrvIml.saveDetailedReportToExcel_State;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import yyy.util.WebLogger;

public class DBProceduresManagerImpl implements DBProceduresManager {

    private ServletContext servletContext;

    //this is test method: there was problem with many connections (but in DOCs getConnection() has already synchronized?)
    private synchronized Connection getConnection() throws SQLException {
        DataSource rootConnDataSource = (DataSource) servletContext.getAttribute("rootConnDataSource");
        return rootConnDataSource.getConnection();
    }

    @Override
    public void createProcedure(String queryDrop, String createProcedure) {
        Connection conn = null;

        try {
            conn = getConnection();

            Statement stmtDrop = conn.createStatement();
            stmtDrop.execute(queryDrop);

            Statement createStmt = conn.createStatement();
            createStmt.executeUpdate(createProcedure);

        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), DBProceduresManagerImpl.class);
            saveDetailedReportToExcel_State = e.getMessage();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), DBProceduresManagerImpl.class);
                    saveDetailedReportToExcel_State = e.getMessage();
                }
            }
        }
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
}
