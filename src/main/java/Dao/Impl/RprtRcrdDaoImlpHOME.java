package Dao.Impl;

import Dao.DBProceduresManager;
import Dao.ReportRecordDao;
import Domain.Employee;
import Domain.Poolable;
import Domain.ReportRecord;
import Domain.UnitedReport;
import Domain.UnitedReportElement;
import Domain.WorkInstance;
import yyy.objectPools.WorkFormPool;
import yyy.objectPools.WorkInstancePool;
import yyy.objectPools.WorkTitlePool;
import yyy.objectPools.WorkTypePool;
import yyy.util.DatesProcessor;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import java.sql.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import yyy.util.WebLogger;

public class RprtRcrdDaoImlpHOME implements ReportRecordDao {

    private ServletContext servletContext;
    private boolean getReportRecordEagerProcedureCreated = false;
    private boolean getReportRecordEagerProcedureCreatedSum = false;
    private boolean getReportRecordEagerProcedureCreatedUnited = false;
    private String DBName = "timesheet";

    //this is test method: there was problem with many connections (but in DOCs getConnection() has already synchronized?)
    private synchronized Connection getConnection() throws SQLException {
        DataSource rootConnDataSource = (DataSource) servletContext.getAttribute("rootConnDataSource");
        return rootConnDataSource.getConnection();
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
        DBName = servletContext.getInitParameter("DBName");
    }

    @Override
    public Integer updateRecords(Set<ReportRecord> reportRecordSet, Employee employee) {
        DatesProcessor datesProcessor = DatesProcessor.getInstance();
        Connection conn = null;
        int result = -1;

        Set<ReportRecord> deleteSet = new HashSet<>();
        Set<ReportRecord> insertOrUpdateSet = new HashSet<>();

        for (ReportRecord reportRecord : reportRecordSet) {
            if (reportRecord.getHoursNum() == 0) {
                deleteSet.add(reportRecord);
            } else {
                insertOrUpdateSet.add(reportRecord);
            }
        }

        try {
            conn = getConnection();
            Statement stmt = conn.createStatement();
            for (ReportRecord reportRecord : deleteSet) {
                String MySQLFormatDate = datesProcessor.toMySQLFormatDate(reportRecord.getDateReported());
                stmt.addBatch("DELETE FROM report_record "
                        + "WHERE date_reported = '" + MySQLFormatDate + "' "
                        + "AND work_instance_id = " + reportRecord.getWorkInstance().getId() + " "
                        + "AND author_employee_id = " + employee.getId());
            }

            for (ReportRecord reportRecord : insertOrUpdateSet) {
                String MySQLFormatDate = datesProcessor.toMySQLFormatDate(reportRecord.getDateReported());
                stmt.addBatch("INSERT INTO report_record "
                        + "(date_reported, hours_num, work_instance_id, author_employee_id)"
                        + "VALUES ('"
                        + MySQLFormatDate + "', "
                        + reportRecord.getHoursNum() + ", "
                        + reportRecord.getWorkInstance().getId() + ", "
                        + employee.getId() + ") "
                        + "ON DUPLICATE KEY UPDATE "
                        + "hours_num = " + reportRecord.getHoursNum());
            }
            int[] rows = stmt.executeBatch();
            result = rows.length;
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), RprtRcrdDaoImlpHOME.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), RprtRcrdDaoImlpHOME.class);
                }
            }
        }

        return result;
    }

    @Override
    public Set<ReportRecord> getRecordSet(Employee employee, Date dateFrom, Date dateTo) {
        Set<ReportRecord> result = new HashSet<>();

        if (!getReportRecordEagerProcedureCreated) {
            DBProceduresManager dbProceduresManager
                    = (DBProceduresManager) servletContext.getAttribute("dbProceduresManager");

            String queryDrop = "DROP PROCEDURE IF EXISTS GET_REPORT_RECORD_EXPLODED";

            String createProcedure
                    = "create procedure GET_REPORT_RECORD_EXPLODED (\n"
                    + "IN employeeAccount varchar(64),\n"
                    + "IN dateFrom date,\n"
                    + "IN dateTo date)\n"
                    + "BEGIN\n"
                    + "SELECT\n"
                    + "t1.id AS report_record_id,\n"
                    + "t1.date_reported AS date_reported,\n"
                    + "t1.hours_num AS hours_num,\n"
                    + "t2.id AS work_instance_id,\n"
                    + "t2.details AS work_instance_details,\n"
                    + "t3.id AS work_title_id,\n"
                    + "t3.title AS work_title_title,\n"
                    + "t4.id AS work_form_id,\n"
                    + "t4.title AS work_form_title,\n"
                    + "t6.id AS work_type_id,\n"
                    + "t6.title AS work_type_title\n"
                    + "FROM\n"
                    + "(((((report_record t1\n"
                    + "JOIN work_instance t2 ON (t1.work_instance_id = t2.id))\n"
                    + "JOIN work_title t3 ON (t2.work_title_id = t3.id))\n"
                    + "JOIN work_form t4 ON (t2.work_form_id = t4.id))\n"
                    + "JOIN employee t5 ON (t1.author_employee_id = t5.id)))\n"
                    + "JOIN work_type t6 ON (t3.work_type_id = t6.id)\n"
                    + "WHERE t5.account = employeeAccount\n"
                    + "AND t1.date_reported >= dateFrom\n"
                    + "AND t1.date_reported <= dateTo;\n"
                    + "END";

            dbProceduresManager.createProcedure(queryDrop, createProcedure);

            getReportRecordEagerProcedureCreated = true;
        }

        WorkTypePool workTypePool = (WorkTypePool) servletContext.getAttribute("workTypePool");
        WorkTitlePool workTitlePool = (WorkTitlePool) servletContext.getAttribute("workTitlePool");
        WorkFormPool workFormPool = (WorkFormPool) servletContext.getAttribute("workFormPool");
        WorkInstancePool workInstancePool = (WorkInstancePool) servletContext.getAttribute("workInstancePool");

        Connection conn = null;
        CallableStatement callableStatement = null;

        try {
            conn = getConnection();
            callableStatement = conn.prepareCall("{call " + DBName + ".GET_REPORT_RECORD_EXPLODED(?,?,?)}");
            callableStatement.setString(1, employee.getAccount());
            callableStatement.setDate(2, new java.sql.Date(dateFrom.getTime()));
            callableStatement.setDate(3, new java.sql.Date(dateTo.getTime()));
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {
                ReportRecord reportRecord = new ReportRecord();
                reportRecord.setDateReported(new Date(resultSet.getDate("date_reported").getTime()));
                reportRecord.setHoursNum(resultSet.getInt("hours_num"));

                //the order matters for the calls below
                Poolable workType = workTypePool
                        .add(resultSet.getInt("work_type_id"),
                                resultSet.getString("work_type_title"));
                Poolable workTitle = workTitlePool
                        .add(resultSet.getInt("work_title_id"),
                                resultSet.getString("work_title_title"),
                                workType);
                Poolable workForm = workFormPool
                        .add(resultSet.getInt("work_form_id"),
                                resultSet.getString("work_form_title"));
                Poolable workInstance = workInstancePool
                        .add(resultSet.getInt("work_instance_id"),
                                resultSet.getString("work_instance_details"),
                                workTitle,
                                workForm);

                reportRecord.setWorkInstance((WorkInstance) workInstance);
                result.add(reportRecord);

            }
            callableStatement.clearParameters();
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), RprtRcrdDaoImlpHOME.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), RprtRcrdDaoImlpHOME.class);
                }
            }
        }

        return result;
    }

    @Override
    public Set<ReportRecord> getSummaryRecordSet(Employee employee, Date dateFrom, Date dateTo) {
        Set<ReportRecord> result = new HashSet<>();

        if (!getReportRecordEagerProcedureCreatedSum) {

            DBProceduresManager dbProceduresManager
                    = (DBProceduresManager) servletContext.getAttribute("dbProceduresManager");

            String queryDrop = "DROP PROCEDURE IF EXISTS YYY_GET_REPORT_RECORD_EXPLODED";

            String createProcedure
                    = "create procedure YYY_GET_REPORT_RECORD_EXPLODED (\n"
                    + "IN employeeAccount varchar(64),\n"
                    + "IN dateFrom date,\n"
                    + "IN dateTo date)\n"
                    + "BEGIN\n"
                    + "DROP TEMPORARY TABLE IF EXISTS YYY_GET_REPORT_RECORD_EXPLODED;\n"
                    + "CREATE TEMPORARY TABLE YYY_GET_REPORT_RECORD_EXPLODED SELECT\n"
                    + "t1.id AS report_record_id,\n"
                    + "t1.date_reported AS date_reported,\n"
                    + "t1.hours_num AS hours_num,\n"
                    + "t2.id AS work_instance_id,\n"
                    + "t2.details AS work_instance_details,\n"
                    + "t3.id AS work_title_id,\n"
                    + "t3.title AS work_title_title,\n"
                    + "t4.id AS work_form_id,\n"
                    + "t4.title AS work_form_title,\n"
                    + "t6.id AS work_type_id,\n"
                    + "t6.title AS work_type_title\n"
                    + "FROM\n"
                    + "(((((report_record t1\n"
                    + "JOIN work_instance t2 ON (t1.work_instance_id = t2.id))\n"
                    + "JOIN work_title t3 ON (t2.work_title_id = t3.id))\n"
                    + "JOIN work_form t4 ON (t2.work_form_id = t4.id))\n"
                    + "JOIN employee t5 ON (t1.author_employee_id = t5.id)))\n"
                    + "JOIN work_type t6 ON (t3.work_type_id = t6.id)\n"
                    + "WHERE t5.account = employeeAccount\n"
                    + "AND t1.date_reported >= dateFrom\n"
                    + "AND t1.date_reported <= dateTo;\n"
                    + "SELECT * , SUM(hours_num) AS sum_h from " + DBName + ".YYY_GET_REPORT_RECORD_EXPLODED group by work_instance_id;\n"
                    + "END";

            dbProceduresManager.createProcedure(queryDrop, createProcedure);
            getReportRecordEagerProcedureCreatedSum = true;
        }

        WorkTypePool workTypePool = (WorkTypePool) servletContext.getAttribute("workTypePool");
        WorkTitlePool workTitlePool = (WorkTitlePool) servletContext.getAttribute("workTitlePool");
        WorkFormPool workFormPool = (WorkFormPool) servletContext.getAttribute("workFormPool");
        WorkInstancePool workInstancePool = (WorkInstancePool) servletContext.getAttribute("workInstancePool");

        Connection conn = null;
        CallableStatement callableStatement = null;

        try {
            conn = getConnection();
            callableStatement = conn.prepareCall("{call " + DBName + ".YYY_GET_REPORT_RECORD_EXPLODED(?,?,?)}");
            callableStatement.setString(1, employee.getAccount());
            callableStatement.setDate(2, new java.sql.Date(dateFrom.getTime()));
            callableStatement.setDate(3, new java.sql.Date(dateTo.getTime()));
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {
                ReportRecord reportRecord = new ReportRecord();
                reportRecord.setDateReported(dateFrom); //all result hours are putting in datefrom column
                reportRecord.setHoursNum(resultSet.getInt("sum_h"));

                //the order matters for the calls below
                Poolable workType = workTypePool
                        .add(resultSet.getInt("work_type_id"),
                                resultSet.getString("work_type_title"));
                Poolable workTitle = workTitlePool
                        .add(resultSet.getInt("work_title_id"),
                                resultSet.getString("work_title_title"),
                                workType);
                Poolable workForm = workFormPool
                        .add(resultSet.getInt("work_form_id"),
                                resultSet.getString("work_form_title"));
                Poolable workInstance = workInstancePool
                        .add(resultSet.getInt("work_instance_id"),
                                resultSet.getString("work_instance_details"),
                                workTitle,
                                workForm);

                reportRecord.setWorkInstance((WorkInstance) workInstance);
                result.add(reportRecord);

            }
            callableStatement.clearParameters();
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), RprtRcrdDaoImlpHOME.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), RprtRcrdDaoImlpHOME.class);
                }
            }
        }

        return result;
    }

    @Override
    public UnitedReport getUnitedReport(Employee employee, Date dateFrom, Date dateTo, UnitedReport unitedReport) {
        Map<String, UnitedReportElement> unRep = unitedReport.getUnRep();

        if (!getReportRecordEagerProcedureCreatedUnited) {

            DBProceduresManager dbProceduresManager
                    = (DBProceduresManager) servletContext.getAttribute("dbProceduresManager");

            String queryDrop = "DROP PROCEDURE IF EXISTS YYY_GET_REPORT_RECORD_EXPLODED_PRE";

            String createProcedure
                    = "create procedure YYY_GET_REPORT_RECORD_EXPLODED_PRE (\n"
                    + "IN employeeAccount varchar(64),\n"
                    + "IN dateFrom date,\n"
                    + "IN dateTo date)\n"
                    + "BEGIN\n"
                    + "DROP TEMPORARY TABLE IF EXISTS YYY_GET_REPORT_RECORD_EXPLODED_PRE;\n"
                    + "CREATE TEMPORARY TABLE YYY_GET_REPORT_RECORD_EXPLODED_PRE SELECT\n"
                    + "t1.id AS report_record_id,\n"
                    + "t1.date_reported AS date_reported,\n"
                    + "t1.hours_num AS hours_num,\n"
                    + "t2.id AS work_instance_id,\n"
                    + "t2.details AS work_instance_details,\n"
                    + "t3.id AS work_title_id,\n"
                    + "t3.title AS work_title_title,\n"
                    + "t4.id AS work_form_id,\n"
                    + "t4.title AS work_form_title,\n"
                    + "t6.id AS work_type_id,\n"
                    + "t6.title AS work_type_title\n"
                    + "FROM\n"
                    + "(((((report_record t1\n"
                    + "JOIN work_instance t2 ON (t1.work_instance_id = t2.id))\n"
                    + "JOIN work_title t3 ON (t2.work_title_id = t3.id))\n"
                    + "JOIN work_form t4 ON (t2.work_form_id = t4.id))\n"
                    + "JOIN employee t5 ON (t1.author_employee_id = t5.id)))\n"
                    + "JOIN work_type t6 ON (t3.work_type_id = t6.id)\n"
                    + "WHERE t5.account = employeeAccount\n"
                    + "AND t1.date_reported >= dateFrom\n"
                    + "AND t1.date_reported <= dateTo;\n"
                    + "END";

            dbProceduresManager.createProcedure(queryDrop, createProcedure);
            getReportRecordEagerProcedureCreatedSum = true;
        }

        Connection conn = null;
        CallableStatement callableStatement = null;
        PreparedStatement preparedStatement = null;

        try {
            conn = getConnection();
            callableStatement = conn.prepareCall("{call " + DBName + ".YYY_GET_REPORT_RECORD_EXPLODED_PRE(?,?,?)}");
            callableStatement.setString(1, employee.getAccount());
            callableStatement.setDate(2, new java.sql.Date(dateFrom.getTime()));
            callableStatement.setDate(3, new java.sql.Date(dateTo.getTime()));
            callableStatement.executeQuery();
            callableStatement.clearParameters();

            for (Map.Entry<String, UnitedReportElement> entry : unRep.entrySet()) {
                String key = entry.getKey();
                UnitedReportElement unitedReportElement = entry.getValue();
                String requestedFieldName = "work_type_title";
                String requestedFieldValue = unitedReportElement.getWorkType();
                String addingConditionWorkForm = "";
                if (!unitedReportElement.getWorkInstance().equals("")) {
                    requestedFieldName = "work_instance_details";
                    requestedFieldValue = unitedReportElement.getWorkInstance();
                }
                if (unitedReportElement.getWorkInstance().equals("") && !unitedReportElement.getWorkForm().equals("")) {
                    requestedFieldName = "work_form_title";
                    requestedFieldValue = unitedReportElement.getWorkForm();
                    addingConditionWorkForm = " AND work_type_title = '" + unitedReportElement.getWorkType() + "' AND work_title_title = '" + unitedReportElement.getWorkTitle() + "'";
                }
                if (unitedReportElement.getWorkForm().equals("") && !unitedReportElement.getWorkTitle().equals("")) {
                    requestedFieldName = "work_title_title";
                    requestedFieldValue = unitedReportElement.getWorkTitle();
                }

                preparedStatement = conn.prepareStatement("select SUM(hours_num) AS sum_h from " + DBName + ".YYY_GET_REPORT_RECORD_EXPLODED_PRE where " + requestedFieldName + " = ? " + addingConditionWorkForm);
                preparedStatement.setString(1, requestedFieldValue);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    unitedReportElement.setUnionHours(resultSet.getInt("sum_h"));
                }
            }
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), RprtRcrdDaoImlpHOME.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), RprtRcrdDaoImlpHOME.class);
                }
            }
        }

        return unitedReport;
    }
}
