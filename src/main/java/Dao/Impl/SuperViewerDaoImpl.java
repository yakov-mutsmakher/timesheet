package Dao.Impl;

import Dao.DBProceduresManager;
import Dao.SuperViewerDAO;
import Domain.Employee;
import Domain.PlanCompareElement;
import Domain.SuperViewerUnitedReportElement;
import static Service.Impl.SprUsrPnlExchSrvIml.saveDetailedReportToExcel_State;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import yyy.util.WebLogger;

public class SuperViewerDaoImpl implements SuperViewerDAO {

    private ServletContext servletContext;
    private boolean getSuperViewerUnitedEagerProcedureCreated = false;
    private boolean getDetailedReportEagerProcedureCreated = false;
    private String DBName = "timesheet";

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
        DBName = servletContext.getInitParameter("DBName");
    }

    //this is test method: there was problem with many connections (but in DOCs getConnection() has already synchronized?)
    private synchronized Connection getConnection() throws SQLException {
        DataSource rootConnDataSource = (DataSource) servletContext.getAttribute("rootConnDataSource");
        return rootConnDataSource.getConnection();
    }

    @Override
    public List<Employee> getEmployees(String onlyActive, Employee currentEmployee) {
        List<Employee> employees = new ArrayList<>();
        Connection conn = null;
        String needOnlyActiv = onlyActive.equals("true") ? " and role_by_employee.role_id != '3'" : "";

        try {
            conn = getConnection();
            PreparedStatement statement = conn.prepareStatement("SELECT id_for_showing FROM super_viewer_show_tree "
                    + "WHERE super_viewer_id = ?;");
            statement.setInt(1, currentEmployee.getId());
            ResultSet resultSet = statement.executeQuery();

            StringBuilder strb = new StringBuilder();
            Boolean firstValue = true;
            while (resultSet.next()) {
                if (resultSet.getInt(1) != 0) {   //if id_for_showing == 0 - show all
                    if (firstValue) {
                        strb.append(" AND employee.id in ('" + resultSet.getInt(1) + "'");
                        firstValue = false;
                    } else {
                        strb.append(",'" + resultSet.getInt(1) + "'");
                    }
                }
            }
            if (!firstValue) {
                strb.append(") ");
            }
            statement.close();
            statement = conn.prepareStatement("SELECT employee.id, employee.name, employee.account, role.title "
                    + "FROM employee, role_by_employee, role "
                    + "where employee.id=role_by_employee.employee_id and role_by_employee.role_id=role.id"
                    + needOnlyActiv + strb.toString() + " order by employee.name;");
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Employee employee = new Employee(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("account"),
                        resultSet.getString("title"));
                employees.add(employee);
            }
            statement.close();
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
                }
            }
        }
        return employees;
    }

    @Override
    public Map<Integer, Integer> getSuperViewerReport(Date dateFrom, Date dateTo, List<Integer> selectedEmployeesID) {
        DBProceduresManager dbProceduresManager
                = (DBProceduresManager) servletContext.getAttribute("dbProceduresManager");
        Map<Integer, Integer> result = new TreeMap<>();

        StringBuilder strb = new StringBuilder();
        Boolean firstValue = true;
        for (Integer el : selectedEmployeesID) {
            if (firstValue) {
                strb.append("'" + el + "'");
                firstValue = false;
            } else {
                strb.append(",'" + el + "'");
            }
        }

        String queryDrop = "DROP PROCEDURE IF EXISTS GET_SUPERUSER_SUM_USERS_REPORT";
        String createProcedure
                = "create procedure GET_SUPERUSER_SUM_USERS_REPORT (\n"
                + "IN dateFrom date,\n"
                + "IN dateTo date)\n"
                + "BEGIN\n"
                + "DROP TEMPORARY TABLE IF EXISTS GET_SUPERUSER_SUM_USERS_REPORT;\n"
                + "CREATE TEMPORARY TABLE GET_SUPERUSER_SUM_USERS_REPORT SELECT\n"
                + "t1.id AS report_record_id,\n"
                + "t5.id AS employee_id,\n"
                + "t5.name AS employee_name,\n"
                + "t5.account AS employee_account,\n"
                + "t1.date_reported AS date_reported,\n"
                + "t1.hours_num AS hours_num,\n"
                + "t2.id AS work_instance_id,\n"
                + "t2.details AS work_instance_details,\n"
                + "t3.id AS work_title_id,\n"
                + "t3.title AS work_title_title,\n"
                + "t4.id AS work_form_id,\n"
                + "t4.title AS work_form_title,\n"
                + "t6.id AS work_type_id,\n"
                + "t6.title AS work_type_title\n"
                + "FROM\n"
                + "(((((report_record t1\n"
                + "JOIN work_instance t2 ON (t1.work_instance_id = t2.id))\n"
                + "JOIN work_title t3 ON (t2.work_title_id = t3.id))\n"
                + "JOIN work_form t4 ON (t2.work_form_id = t4.id))\n"
                + "JOIN employee t5 ON (t1.author_employee_id = t5.id)))\n"
                + "JOIN work_type t6 ON (t3.work_type_id = t6.id)\n"
                + "WHERE t5.id in (" + strb.toString() + ") AND\n"
                + "t1.date_reported >= dateFrom AND\n"
                + "t1.date_reported <= dateTo;\n"
                + "select employee_id, SUM(hours_num) AS sum_h from " + DBName + ".GET_SUPERUSER_SUM_USERS_REPORT group by employee_name;\n"
                + "END";

        dbProceduresManager.createProcedure(queryDrop, createProcedure);

        Connection conn = null;
        CallableStatement callableStatement = null;

        try {
            conn = getConnection();
            callableStatement = conn.prepareCall("{call " + DBName + ".GET_SUPERUSER_SUM_USERS_REPORT(?,?)}");
            callableStatement.setDate(1, new java.sql.Date(dateFrom.getTime()));
            callableStatement.setDate(2, new java.sql.Date(dateTo.getTime()));
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {
                result.put(resultSet.getInt("employee_id"), resultSet.getInt("sum_h"));
            }
            callableStatement.clearParameters();
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
                }
            }
        }
        return result;
    }

    @Override
    public Map<String, SuperViewerUnitedReportElement> getSuperViewerUnitedReport(Date dateFrom, Date dateTo, List<Integer> selectedEmployeesID, Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport) {

        if (!getSuperViewerUnitedEagerProcedureCreated) {

            DBProceduresManager dbProceduresManager
                    = (DBProceduresManager) servletContext.getAttribute("dbProceduresManager");

            String queryDrop = "DROP PROCEDURE IF EXISTS YYY_GET_SUPER_VIEWER_UNITED_REPORT_EXPLODED";

            String createProcedure
                    = "create procedure YYY_GET_SUPER_VIEWER_UNITED_REPORT_EXPLODED (\n"
                    + "IN dateFrom date,\n"
                    + "IN dateTo date)\n"
                    + "BEGIN\n"
                    + "DROP TEMPORARY TABLE IF EXISTS YYY_GET_SUPER_VIEWER_UNITED_REPORT_EXPLODED;\n"
                    + "CREATE TEMPORARY TABLE YYY_GET_SUPER_VIEWER_UNITED_REPORT_EXPLODED SELECT\n"
                    + "t1.id AS report_record_id,\n"
                    + "t1.date_reported AS date_reported,\n"
                    + "t1.hours_num AS hours_num,\n"
                    + "t2.id AS work_instance_id,\n"
                    + "t2.details AS work_instance_details,\n"
                    + "t3.id AS work_title_id,\n"
                    + "t3.title AS work_title_title,\n"
                    + "t4.id AS work_form_id,\n"
                    + "t4.title AS work_form_title,\n"
                    + "t5.name AS employee_name,\n"
                    + "t5.id AS employee_id,\n"
                    + "t6.id AS work_type_id,\n"
                    + "t6.title AS work_type_title\n"
                    + "FROM\n"
                    + "(((((report_record t1\n"
                    + "JOIN work_instance t2 ON (t1.work_instance_id = t2.id))\n"
                    + "JOIN work_title t3 ON (t2.work_title_id = t3.id))\n"
                    + "JOIN work_form t4 ON (t2.work_form_id = t4.id))\n"
                    + "JOIN employee t5 ON (t1.author_employee_id = t5.id)))\n"
                    + "JOIN work_type t6 ON (t3.work_type_id = t6.id)\n"
                    + "WHERE t1.date_reported >= dateFrom\n"
                    + "AND t1.date_reported <= dateTo;\n"
                    + "END";

            dbProceduresManager.createProcedure(queryDrop, createProcedure);
            getSuperViewerUnitedEagerProcedureCreated = true;
        }

        Connection conn = null;
        CallableStatement callableStatement = null;
        PreparedStatement preparedStatement = null;

        StringBuilder strb = new StringBuilder();
        Boolean firstValue = true;
        for (Integer el : selectedEmployeesID) {
            if (firstValue) {
                strb.append("SUM(if(employee_id='" + el + "', hours_num, 0)) as '" + el + "'");
                firstValue = false;
            } else {
                strb.append(", SUM(if(employee_id='" + el + "', hours_num, 0)) as '" + el + "'");
            }
        }

        try {
            conn = getConnection();
            callableStatement = conn.prepareCall("{call " + DBName + ".YYY_GET_SUPER_VIEWER_UNITED_REPORT_EXPLODED(?,?)}");
            callableStatement.setDate(1, new java.sql.Date(dateFrom.getTime()));
            callableStatement.setDate(2, new java.sql.Date(dateTo.getTime()));
            callableStatement.executeQuery();
            callableStatement.clearParameters();

            for (Map.Entry<String, SuperViewerUnitedReportElement> entry : superViewerUnitedReport.entrySet()) {
                String key = entry.getKey();
                SuperViewerUnitedReportElement superViewerUnitedReportElement = entry.getValue();
                String requestedFieldName = "work_type_title";
                String requestedFieldValue = superViewerUnitedReportElement.getWorkType();
                if (superViewerUnitedReportElement.getWorkInstance() != "") {
                    requestedFieldName = "work_instance_details";
                    requestedFieldValue = superViewerUnitedReportElement.getWorkInstance();
                }
                if (superViewerUnitedReportElement.getWorkInstance() == "" && superViewerUnitedReportElement.getWorkForm() != "") {
                    requestedFieldName = "work_form_title";
                    requestedFieldValue = superViewerUnitedReportElement.getWorkForm();
                }
                if (superViewerUnitedReportElement.getWorkForm() == "" && superViewerUnitedReportElement.getWorkTitle() != "") {
                    requestedFieldName = "work_title_title";
                    requestedFieldValue = superViewerUnitedReportElement.getWorkTitle();
                }

                preparedStatement = conn.prepareStatement("select " + strb.toString() + " from " + DBName + ".YYY_GET_SUPER_VIEWER_UNITED_REPORT_EXPLODED where " + requestedFieldName + " = ?");
                preparedStatement.setString(1, requestedFieldValue);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Map<Integer, Integer> selectedEmployeesHoursInstance = new TreeMap<>();
                    for (Integer el : selectedEmployeesID) {
                        selectedEmployeesHoursInstance.put(el, resultSet.getInt(el.toString()));
                    }
                    superViewerUnitedReportElement.setSelectedEmployeesHoursInstance(selectedEmployeesHoursInstance);
                }
                superViewerUnitedReport.replace(key, superViewerUnitedReportElement);
            }
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
                }
            }
        }

        return superViewerUnitedReport;
    }

    @Override
    public List<List<String>> getDetailedReport(Date dateFrom, Date dateTo, List<Employee> selectedEmployeesList, String groupBy) {
        List<List<String>> res = new ArrayList<List<String>>();
        String group_by = "work_instance_id";
        String selectedColumns = "work_type_title as 'Work type', work_title_title as 'Work title', "
                + "work_form_title as 'Work form', work_instance_details as 'Details'";
        int quantityOfStringColumns = 4;
        switch (groupBy) {
            case "work_form_id":
                group_by = "work_type_id, work_title_id, work_form_id";
                selectedColumns = "work_type_title as 'Work type', work_title_title as 'Work title', "
                        + "work_form_title as 'Work form'";
                quantityOfStringColumns = 3;
                break;
            case "work_title_id":
                group_by = "work_type_id, work_title_id";
                selectedColumns = "work_type_title as 'Work type', work_title_title as 'Work title'";
                quantityOfStringColumns = 2;
                break;
            case "work_type_id":
                group_by = "work_type_id";
                selectedColumns = "work_type_title as 'Work type'";
                quantityOfStringColumns = 1;
                break;
        }

        if (!getDetailedReportEagerProcedureCreated) {

            DBProceduresManager dbProceduresManager
                    = (DBProceduresManager) servletContext.getAttribute("dbProceduresManager");

            String queryDrop = "DROP PROCEDURE IF EXISTS YYY_GET_DETAILED_REPORT_EXPLODED";

            String createProcedure
                    = "create procedure YYY_GET_DETAILED_REPORT_EXPLODED (\n"
                    + "IN dateFrom date,\n"
                    + "IN dateTo date)\n"
                    + "BEGIN\n"
                    + "DROP TEMPORARY TABLE IF EXISTS YYY_GET_DETAILED_REPORT_EXPLODED;\n"
                    + "CREATE TEMPORARY TABLE YYY_GET_DETAILED_REPORT_EXPLODED SELECT\n"
                    + "t1.id AS report_record_id,\n"
                    + "t1.date_reported AS date_reported,\n"
                    + "t1.hours_num AS hours_num,\n"
                    + "t2.id AS work_instance_id,\n"
                    + "t2.details AS work_instance_details,\n"
                    + "t3.id AS work_title_id,\n"
                    + "t3.title AS work_title_title,\n"
                    + "t4.id AS work_form_id,\n"
                    + "t4.title AS work_form_title,\n"
                    + "t5.name AS employee_name,\n"
                    + "t5.id AS employee_id,\n"
                    + "t5.account AS employee_account,\n"
                    + "t6.id AS work_type_id,\n"
                    + "t6.title AS work_type_title\n"
                    + "FROM\n"
                    + "(((((report_record t1\n"
                    + "JOIN work_instance t2 ON (t1.work_instance_id = t2.id))\n"
                    + "JOIN work_title t3 ON (t2.work_title_id = t3.id))\n"
                    + "JOIN work_form t4 ON (t2.work_form_id = t4.id))\n"
                    + "JOIN employee t5 ON (t1.author_employee_id = t5.id)))\n"
                    + "JOIN work_type t6 ON (t3.work_type_id = t6.id)\n"
                    + "WHERE t1.date_reported >= dateFrom\n"
                    + "AND t1.date_reported <= dateTo;\n"
                    + "END";

            dbProceduresManager.createProcedure(queryDrop, createProcedure);
            getDetailedReportEagerProcedureCreated = true;
        }

        Connection conn = null;
        CallableStatement callableStatement = null;
        PreparedStatement preparedStatement = null;

        StringBuilder selectedEmployees = new StringBuilder();
        for (Employee el : selectedEmployeesList) {
            selectedEmployees.append(", SUM(if(employee_id='" + el.getId() + "', hours_num, 0)) as '" + el.getName() + "'");
        }

        try {
            conn = getConnection();
            callableStatement = conn.prepareCall("{call " + DBName + ".YYY_GET_DETAILED_REPORT_EXPLODED(?,?)}");
            callableStatement.setDate(1, new java.sql.Date(dateFrom.getTime()));
            callableStatement.setDate(2, new java.sql.Date(dateTo.getTime()));
            callableStatement.executeQuery();
            callableStatement.clearParameters();

            preparedStatement = conn.prepareStatement("select " + selectedColumns + selectedEmployees.toString()
                    + " from " + DBName + ".YYY_GET_DETAILED_REPORT_EXPLODED "
                    + "group by " + group_by + " order by work_type_title, work_title_title, work_form_title, work_instance_details");
            ResultSet resultSet = preparedStatement.executeQuery();

            ResultSetMetaData rsmd = resultSet.getMetaData();
            List<String> header = new ArrayList<>();
            for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
                header.add(rsmd.getColumnLabel(i));
            }
            res.add(header);
            while (resultSet.next()) {
                List<String> row = new ArrayList<>();
                Boolean emptyHoursRow = true;
                for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
                    row.add(resultSet.getString(i));
                    if (!resultSet.getString(i).equals("0") && i > quantityOfStringColumns) {
                        emptyHoursRow = false;
                    }
                }
                if (!emptyHoursRow) {
                    res.add(row);
                }
            }
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
            saveDetailedReportToExcel_State = e.getMessage();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
                    saveDetailedReportToExcel_State = e.getMessage();
                }
            }
        }

        return res;
    }

    @Override
    public List<Employee> getEmployeesByID(List<Integer> selectedEmployeesID) {
        List<Employee> employees = new ArrayList<>();
        Connection conn = null;

        StringBuilder strb = new StringBuilder();
        Boolean firstValue = true;
        for (Integer id : selectedEmployeesID) {
            if (firstValue) {
                strb.append(" AND employee.id in ('" + id + "'");
                firstValue = false;
            } else {
                strb.append(",'" + id + "'");
            }
        }
        if (!firstValue) {
            strb.append(") ");
        }

        try {
            conn = getConnection();
            PreparedStatement statement = conn.prepareStatement("SELECT employee.id, employee.name, employee.account, role.title "
                    + "FROM employee, role_by_employee, role "
                    + "where employee.id=role_by_employee.employee_id and role_by_employee.role_id=role.id"
                    + strb.toString() + " order by employee.name;");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Employee employee = new Employee(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("account"),
                        resultSet.getString("title"));
                employees.add(employee);
            }
            statement.close();
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
            saveDetailedReportToExcel_State = e.getMessage();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
                    saveDetailedReportToExcel_State = e.getMessage();
                }
            }
        }
        return employees;
    }

    @Override
    public List<PlanCompareElement> getPlanCompare(String yearPlanCompare) {
        List<PlanCompareElement> listPlanCompareElement = new ArrayList<>();
        Connection conn = null;
        try {
            conn = getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT t3.title, sum(t1.hours_num) as 'completedHours', t3.hours_from_plan "
                    + "from ((work_title t3 left JOIN work_instance t2 ON (t2.work_title_id = t3.id)) "
                    + "left JOIN report_record t1 ON (t1.work_instance_id = t2.id)) "
                    + "where t3.title like ('" + yearPlanCompare + "%') group by t3.title order by t3.title;");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                listPlanCompareElement.add(new PlanCompareElement(rs.getString("title"),
                        rs.getInt("completedHours"),
                        (Integer) rs.getObject("hours_from_plan")));
            }
            preparedStatement.close();
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), SuperViewerDaoImpl.class);
                }
            }
        }
        return listPlanCompareElement;
    }

}
