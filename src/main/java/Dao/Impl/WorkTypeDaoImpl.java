package Dao.Impl;

import Dao.DBProceduresManager;
import Dao.WorkTypeDao;
import Domain.WorkType;
import yyy.objectPools.WorkTypePool;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import yyy.util.WebLogger;

public class WorkTypeDaoImpl implements WorkTypeDao {

    private ServletContext servletContext;
    private boolean getReportRecordEagerProcedureCreated = false;
    private String DBName = "timesheet";

    //this is test method: there was problem with many connections (but in DOCs getConnection() has already synchronized?)
    private synchronized Connection getConnection() throws SQLException {
        DataSource rootConnDataSource = (DataSource) servletContext.getAttribute("rootConnDataSource");
        return rootConnDataSource.getConnection();
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
        DBName = servletContext.getInitParameter("DBName");
    }

    @Override
    public List<WorkType> getWorkTypes() {
        List<WorkType> result = new ArrayList<>();

        if (!getReportRecordEagerProcedureCreated) {
            DBProceduresManager dbProceduresManager
                    = (DBProceduresManager) servletContext.getAttribute("dbProceduresManager");

            String queryDrop = "DROP PROCEDURE IF EXISTS GET_WORK_TYPES";

            String createProcedure
                    = "CREATE PROCEDURE `GET_WORK_TYPES` ()\n"
                    + "BEGIN\n"
                    + "select id, title from work_type order by title;\n"
                    + "END";

            dbProceduresManager.createProcedure(queryDrop, createProcedure);

            getReportRecordEagerProcedureCreated = true;
        }

        DataSource rootConnDataSource = (DataSource) servletContext.getAttribute("rootConnDataSource");
        WorkTypePool workTypePool = (WorkTypePool) servletContext.getAttribute("workTypePool");

        Connection conn = null;
        CallableStatement callableStatement = null;

        try {
            conn = getConnection();
            callableStatement = conn.prepareCall("{call " + DBName + ".GET_WORK_TYPES}");
            ResultSet resultSet = callableStatement.executeQuery();
            while (resultSet.next()) {
                WorkType workType = workTypePool
                        .add(resultSet.getInt("id"),
                                resultSet.getString("title"));
                result.add(workType);
            }
            callableStatement.clearParameters();
        } catch (SQLException e) {
            WebLogger.writeError(e.getMessage(), WorkTypeDaoImpl.class);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    WebLogger.writeError(e.getMessage(), WorkTypeDaoImpl.class);
                }
            }
        }
        return result;
    }
}
