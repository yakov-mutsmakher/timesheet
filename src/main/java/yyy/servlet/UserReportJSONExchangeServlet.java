package yyy.servlet;

import Domain.Employee;
import Domain.UserReport;
import Service.ReportsExchangeService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import json.UserReportJSONConvertor;
import org.json.simple.JSONObject;
import yyy.util.DatesProcessor;
import yyy.util.WebLogger;

@WebServlet("/user-report")
public class UserReportJSONExchangeServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserReportJSONConvertor userReportJSONConvertor
                = (UserReportJSONConvertor) getServletContext().getAttribute("userReportJSONConvertor");
        ReportsExchangeService reportsExchangeService
                = (ReportsExchangeService) getServletContext().getAttribute("reportsExchangeService");
        Employee employee = (Employee) request.getSession().getAttribute("employee");
        DatesProcessor datesProcessor = DatesProcessor.getInstance();

        String json = request.getParameter("user_report");
        UserReport userReport = userReportJSONConvertor.fromJSON(json);
        Integer quantityOfChangedElements = reportsExchangeService.saveReport(userReport, employee);
        WebLogger.writeInfo(employee.getAccount() + " has saved " + quantityOfChangedElements + " fields from "
                + datesProcessor.toSimpleFormatString(userReport.DATE_FROM) + " to "
                + datesProcessor.toSimpleFormatString(userReport.DATE_TO), UserReportJSONExchangeServlet.class);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        JSONObject quantityOfChangedElementsJS = new JSONObject();
        quantityOfChangedElementsJS.put("quantityOfChangedElements", quantityOfChangedElements);
        quantityOfChangedElementsJS.writeJSONString(out);
        out.flush();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserReportJSONConvertor userReportJSONConvertor
                = (UserReportJSONConvertor) getServletContext().getAttribute("userReportJSONConvertor");
        ReportsExchangeService reportsExchangeService
                = (ReportsExchangeService) getServletContext().getAttribute("reportsExchangeService");
        DatesProcessor datesProcessor = DatesProcessor.getInstance();

        Employee employee = (Employee) request.getSession().getAttribute("employee");

        String ISOdateToStartFrom = request.getParameter("dateToStartFrom");
        Date dateFrom = null;
        UserReport userReport = null;
        if (ISOdateToStartFrom != null
                && (dateFrom = datesProcessor.getLocalDate(ISOdateToStartFrom)) != null) {
            userReport = reportsExchangeService.getUserReport(employee, dateFrom);
        } else {
            userReport = reportsExchangeService.getUserReport(employee);
        }

        JSONObject userReportJSON = userReportJSONConvertor.toJSON(userReport);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        userReportJSON.writeJSONString(out);
        out.flush();
    }
}
