package yyy.servlet;

import Domain.Employee;
import Domain.PlanCompareElement;
import Domain.SuperViewerUnitedReportElement;
import static Service.Impl.SprUsrPnlExchSrvIml.saveDetailedReportToExcel_State;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import Service.SuperUserPanelExchangeService;
import java.util.Date;
import java.util.Map;
import json.UserReportJSONConvertor;
import org.json.simple.JSONObject;
import yyy.util.DatesProcessor;
import yyy.util.WebLogger;

@WebServlet({"/employees", "/superViewerReport", "/saveDetailedReportToExcel", "/planCompare"})
public class SuperUserServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserReportJSONConvertor userReportJSONConvertor
                = (UserReportJSONConvertor) getServletContext().getAttribute("userReportJSONConvertor");
        SuperUserPanelExchangeService superUserPanelExchangeService
                = (SuperUserPanelExchangeService) getServletContext().getAttribute("superUserPanelExchangeService");
        switch (req.getServletPath()) {
            case "/employees":
                actionEmployees(req, resp, userReportJSONConvertor, superUserPanelExchangeService);
                break;
            case "/superViewerReport":
                actionSuperViewerReport(req, resp, userReportJSONConvertor, superUserPanelExchangeService);
                break;
            case "/saveDetailedReportToExcel":
                actionSaveDetailedReportToExcel(req, resp, userReportJSONConvertor, superUserPanelExchangeService);
                break;
            case "/planCompare":
                actionPlanCompare(req, resp, userReportJSONConvertor, superUserPanelExchangeService);
                break;
        }
    }

    private void actionEmployees(HttpServletRequest req, HttpServletResponse resp,
            UserReportJSONConvertor userReportJSONConvertor, SuperUserPanelExchangeService superUserPanelExchangeService) throws IOException {
        String onlyActive = req.getParameter("onlyActive");
        Employee currentEmployee = (Employee) req.getSession().getAttribute("employee");

        List<Employee> employees = superUserPanelExchangeService.getEmployees(onlyActive, currentEmployee);
        JSONArray JSONresponse = userReportJSONConvertor.employeesListToJSON(employees);

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        JSONresponse.writeJSONString(out);
        out.flush();
    }

    private void actionSuperViewerReport(HttpServletRequest req, HttpServletResponse resp,
            UserReportJSONConvertor userReportJSONConvertor, SuperUserPanelExchangeService superUserPanelExchangeService) throws IOException {
        DatesProcessor datesProcessor = DatesProcessor.getInstance();

        String selectedEmployeesID_JSON = req.getParameter("selectedEmployeesID");
        String mySuperViewerUnitedReport_JSON = req.getParameter("mySuperViewerUnitedReport");

        String dateFromStr = req.getParameter("dateFrom");
        String dateToStr = req.getParameter("dateTo");
        Date dateFrom = null;
        Date dateTo = null;
        Map<Integer, Integer> superViewerReport = null;
        List<Integer> selectedEmployeesID = userReportJSONConvertor.fromJSONSelectedEmployees(selectedEmployeesID_JSON);
        Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport = userReportJSONConvertor.fromJSONSuperViewerUnited(mySuperViewerUnitedReport_JSON);

        if (dateFromStr != null && dateToStr != null
                && (dateFrom = datesProcessor.getSimpleDate(dateFromStr)) != null
                && (dateTo = datesProcessor.getSimpleDate(dateToStr)) != null) {
            superViewerReport = superUserPanelExchangeService.getSuperViewerReport(dateFrom, dateTo, selectedEmployeesID);
            superViewerUnitedReport = superUserPanelExchangeService.getSuperViewerUnitedReport(dateFrom, dateTo, selectedEmployeesID, superViewerUnitedReport);
        } else {
            superViewerReport = superUserPanelExchangeService.getSuperViewerReport(selectedEmployeesID);
            superViewerUnitedReport = superUserPanelExchangeService.getSuperViewerUnitedReport(selectedEmployeesID, superViewerUnitedReport);
        }

        Employee currentEmployee = (Employee) req.getSession().getAttribute("employee");
        WebLogger.writeInfo(currentEmployee.getAccount() + " open SuperViewerReport from " + datesProcessor.toSimpleFormatString(dateFrom) + " to "
                + datesProcessor.toSimpleFormatString(dateTo), SuperUserServlet.class);

        JSONObject superViewerReport_JSON = userReportJSONConvertor.toJSON(superViewerReport);
        JSONObject superViewerUnitedReport_JSON = userReportJSONConvertor.toJSONSuperViewerUnitedReport(superViewerUnitedReport);

        JSONObject answer = new JSONObject();
        answer.put("superViewerReportTable", superViewerReport_JSON);
        answer.put("superViewerUnitedReportTable", superViewerUnitedReport_JSON);

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        answer.writeJSONString(out);
        out.flush();
    }

    private void actionSaveDetailedReportToExcel(HttpServletRequest req, HttpServletResponse resp,
            UserReportJSONConvertor userReportJSONConvertor, SuperUserPanelExchangeService superUserPanelExchangeService) throws IOException {
        DatesProcessor datesProcessor = DatesProcessor.getInstance();
        String selectedEmployeesID_JSON = req.getParameter("selectedEmployeesID");
        String dateFromStr = req.getParameter("dateFrom");
        String dateToStr = req.getParameter("dateTo");
        String groupBy = req.getParameter("groupBy");
        Date dateFrom = null;
        Date dateTo = null;
        saveDetailedReportToExcel_State = "loaded";
        List<Integer> selectedEmployeesID = userReportJSONConvertor.fromJSONSelectedEmployees(selectedEmployeesID_JSON);
        String webPath = "";
        if (dateFromStr != null && dateToStr != null
                && (dateFrom = datesProcessor.getSimpleDate(dateFromStr)) != null
                && (dateTo = datesProcessor.getSimpleDate(dateToStr)) != null) {
            webPath = superUserPanelExchangeService.saveDetailedReportToExcel(dateFrom, dateTo, selectedEmployeesID, groupBy);
        } else {
            webPath = superUserPanelExchangeService.saveDetailedReportToExcel(selectedEmployeesID, groupBy);
        }

        JSONObject answer = new JSONObject();
        answer.put("webPath", webPath);
        answer.put("result", saveDetailedReportToExcel_State);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        answer.writeJSONString(out);
        out.flush();

        Employee employee = (Employee) req.getSession().getAttribute("employee");
        WebLogger.writeInfo(employee.getAccount() + " generated file: " + webPath, SuperUserServlet.class);
    }

    private void actionPlanCompare(HttpServletRequest req, HttpServletResponse resp,
            UserReportJSONConvertor userReportJSONConvertor, SuperUserPanelExchangeService superUserPanelExchangeService) throws IOException {

        String yearPlanCompare = req.getParameter("yearPlanCompare");
        List<PlanCompareElement> planCompareList = superUserPanelExchangeService.getPlanCompare(yearPlanCompare);

        JSONArray planCompareList_JSON = userReportJSONConvertor.planCompareListToJSON(planCompareList);

        Employee employee = (Employee) req.getSession().getAttribute("employee");
        WebLogger.writeInfo(employee.getAccount() + " open actionPlanCompare for " + yearPlanCompare, SuperUserServlet.class);

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        planCompareList_JSON.writeJSONString(out);
        out.flush();
    }

}
