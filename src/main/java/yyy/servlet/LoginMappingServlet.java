package yyy.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet({"/checkLogin", "/index.html"})
public class LoginMappingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getServletPath()) {
            case "/checkLogin":
                String errors = "Incorrect login or password";
                req.setAttribute("errors", errors);
                req.getRequestDispatcher("/loginPage.jsp").forward(req, resp);
                break;
            case "/index.html":
                req.getRequestDispatcher("jsp/userReport.jsp").forward(req, resp);
                break;
        }
    }

}
