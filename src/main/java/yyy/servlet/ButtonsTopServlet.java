package yyy.servlet;

import Domain.Employee;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import yyy.util.WebLogger;

@WebServlet({"/superViewerPage", "/logout", "/reports", "/main", "/adminPage"})
public class ButtonsTopServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getServletPath()) {
            case "/logout":
                Employee employee = (Employee) req.getSession().getAttribute("employee");
                WebLogger.writeInfo(employee.getAccount() + " logout", ButtonsTopServlet.class);
                req.getSession().invalidate();
                req.logout();
                resp.sendRedirect(req.getContextPath() + "/");
                //req.getRequestDispatcher("/loginPage.jsp").forward(req, resp);
                break;
            case "/superViewerPage":
                req.getRequestDispatcher("jsp/superViewerPage.jsp").forward(req, resp);
                break;
            case "/reports":
                req.getRequestDispatcher("jsp/reports.jsp").forward(req, resp);
                break;
            case "/main":
                resp.sendRedirect(req.getContextPath() + "/");
                break;
            case "/adminPage":
                req.getRequestDispatcher("jsp/adminPage.jsp").forward(req, resp);
                break;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
