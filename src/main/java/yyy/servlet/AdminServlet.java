package yyy.servlet;

import Domain.Employee;
import Service.AdminPanelExchangeService;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import json.UserReportJSONConvertor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import yyy.util.ActivDirectoryChecker;
import yyy.util.ExcelTools;
import yyy.util.WebLogger;

@WebServlet({"/updatingPlanData", "/changeRole", "/adminSelectedID", "/searchEmployee", "/addNewEmployee"})
@MultipartConfig
public class AdminServlet extends HttpServlet {

    private String planYear = "";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part filePart = req.getPart("file");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        InputStream fileContent = filePart.getInputStream();
        String fileNameError = checkFileNameError(fileName);
        if (fileNameError.equals("")) {
            ExcelTools excelTools = (ExcelTools) getServletContext().getAttribute("excelTools");
            AdminPanelExchangeService adminPanelExchangeService
                    = (AdminPanelExchangeService) getServletContext().getAttribute("adminPanelExchangeService");
            Map<String, Integer> planData = excelTools.getPlanData(fileContent, planYear);
            fileNameError = adminPanelExchangeService.updateDataByPlanData(planData);
        }

        req.setAttribute("fileError", fileNameError);
        req.setAttribute("fileName", fileName);
        req.getRequestDispatcher("jsp/adminPage.jsp").forward(req, resp);
        //resp.sendRedirect(req.getContextPath() + "/adminPage");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserReportJSONConvertor userReportJSONConvertor
                = (UserReportJSONConvertor) getServletContext().getAttribute("userReportJSONConvertor");
        AdminPanelExchangeService adminPanelExchangeService
                = (AdminPanelExchangeService) getServletContext().getAttribute("adminPanelExchangeService");
        switch (req.getServletPath()) {
            case "/changeRole":
                changeRole(req, resp, userReportJSONConvertor, adminPanelExchangeService);
                break;
            case "/adminSelectedID":
                adminSelectedID(req, resp, userReportJSONConvertor, adminPanelExchangeService);
                break;
            case "/searchEmployee":
                searchEmployee(req, resp);
                break;
            case "/addNewEmployee":
                addNewEmployee(req, resp, userReportJSONConvertor, adminPanelExchangeService);
                break;
        }
    }

    private String checkFileNameError(String fileName) {
        String fileNameError = "";
        if (fileName.equals("")) {
            fileNameError = "file_was_not_selected";
            return fileNameError;
        }
        if (!fileName.endsWith(".xlsx") && !fileName.endsWith(".xls")) {
            fileNameError = "incorrect_file_format";
            return fileNameError;
        }
        if (fileName.toLowerCase().contains("ukr")) {
            fileNameError = "ukrainian_version_was_selected";
            return fileNameError;
        }
        String fileNameYearCheck = fileName.replaceAll("[^0-9]", "#");
        String[] arr = fileNameYearCheck.split("#");
        int matches = 0;
        for (String s : arr) {
            if (s.matches("^[0-9]{4}$")) {
                matches++;
                planYear = s;
            }
        }
        if (matches != 1) {
            fileNameError = "incorrect_year";
        }
        return fileNameError;
    }

    private void changeRole(HttpServletRequest req, HttpServletResponse resp, UserReportJSONConvertor userReportJSONConvertor, AdminPanelExchangeService adminPanelExchangeService) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("id"));
        String newRole = req.getParameter("newRole");
        String oldRole = req.getParameter("oldRole");
        String changingAccount = req.getParameter("changingAccount");
        String selectedEmployeesID_JSON = req.getParameter("selectedEmployeesID");
        Employee currentEmployee = (Employee) req.getSession().getAttribute("employee");
        List<Integer> selectedEmployeesID = userReportJSONConvertor.fromJSONSelectedEmployees(selectedEmployeesID_JSON);

        String result = adminPanelExchangeService.changeRole(id, oldRole, newRole, selectedEmployeesID);
        if (result.equals("Successfully changed")) {
            WebLogger.writeInfo(currentEmployee.getAccount() + " has changed role for " + changingAccount + " from "
                    + oldRole + " to " + newRole, AdminServlet.class);
        }

        JSONObject answer = new JSONObject();
        answer.put("result", result);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        answer.writeJSONString(out);
        out.flush();
    }

    private void adminSelectedID(HttpServletRequest req, HttpServletResponse resp, UserReportJSONConvertor userReportJSONConvertor, AdminPanelExchangeService adminPanelExchangeService) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("id"));
        List<Integer> selectedEmployeesID = adminPanelExchangeService.adminSelectedID(id);

        JSONArray selectedEmployeesID_JSON = userReportJSONConvertor.selectedEmployeesIDToJSON(selectedEmployeesID);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        selectedEmployeesID_JSON.writeJSONString(out);
        out.flush();
    }

    private void searchEmployee(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = req.getParameter("login");

        ActivDirectoryChecker activDirectoryChecker = (ActivDirectoryChecker) getServletContext().getAttribute("activDirectoryChecker");
        String result = activDirectoryChecker.searchEmployee(login);

        JSONObject answer = new JSONObject();
        answer.put("result", result);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        answer.writeJSONString(out);
        out.flush();
    }

    private void addNewEmployee(HttpServletRequest req, HttpServletResponse resp, UserReportJSONConvertor userReportJSONConvertor, AdminPanelExchangeService adminPanelExchangeService) throws IOException {
        String newRoleAdd = req.getParameter("newRoleAdd");
        String login = req.getParameter("login");
        String newEmployeeName = req.getParameter("newEmployeeName");
        String selectedEmployeesIDAdd_JSON = req.getParameter("selectedEmployeesIDAdd");
        Employee currentEmployee = (Employee) req.getSession().getAttribute("employee");
        List<Integer> selectedEmployeesIDAdd = userReportJSONConvertor.fromJSONSelectedEmployees(selectedEmployeesIDAdd_JSON);

        String result = adminPanelExchangeService.addNewEmployee(login, newEmployeeName, newRoleAdd, selectedEmployeesIDAdd);
        if (result.equals("Successfully added")) {
            WebLogger.writeInfo(currentEmployee.getAccount() + " added new employee " + login + " : " + newEmployeeName + " to DB", AdminServlet.class);
        }

        JSONObject answer = new JSONObject();
        answer.put("result", result);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        answer.writeJSONString(out);
        out.flush();
    }
}
