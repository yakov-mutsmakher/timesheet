package yyy.servlet;

import Domain.Employee;
import Domain.UnitedReport;
import Domain.UserReport;
import Service.ReportsExchangeService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import json.UserReportJSONConvertor;
import org.json.simple.JSONObject;
import yyy.util.DatesProcessor;

@WebServlet("/reports-my-report")
public class ReportsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserReportJSONConvertor userReportJSONConvertor
                = (UserReportJSONConvertor) getServletContext().getAttribute("userReportJSONConvertor");
        ReportsExchangeService reportsExchangeService
                = (ReportsExchangeService) getServletContext().getAttribute("reportsExchangeService");
        DatesProcessor datesProcessor = DatesProcessor.getInstance();

        Employee employee = (Employee) req.getSession().getAttribute("employee");

        String myUnitedReport = req.getParameter("myUnitedReport");
        
        String dateFromStr = req.getParameter("dateFrom");
        String dateToStr = req.getParameter("dateTo");
        Date dateFrom = null;
        Date dateTo = null;
        UserReport userReportSummary = null;
        UnitedReport unitedReport = userReportJSONConvertor.fromJSONUnited(myUnitedReport);
                
        if (dateFromStr != null && dateToStr != null
                && (dateFrom = datesProcessor.getSimpleDate(dateFromStr)) != null
                && (dateTo = datesProcessor.getSimpleDate(dateToStr)) != null) {
            userReportSummary = reportsExchangeService.getSummaryReport(employee, dateFrom, dateTo);
            unitedReport = reportsExchangeService.getUnitedReport(employee, dateFrom, dateTo, unitedReport);
        } else {
            userReportSummary = reportsExchangeService.getSummaryReport(employee);
            unitedReport = reportsExchangeService.getUnitedReport(employee, unitedReport);
        }

        JSONObject userReportJSONFirst = userReportJSONConvertor.toJSON(userReportSummary);
        JSONObject userReportJSONSecond = userReportJSONConvertor.unitedReportToJSON(unitedReport);

        JSONObject userReportJSON = new JSONObject();
        userReportJSON.put("firstTable", userReportJSONFirst);
        userReportJSON.put("secondTable", userReportJSONSecond);

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        userReportJSON.writeJSONString(out);
        out.flush();
    }

}
