package yyy.util;

import static Service.Impl.SprUsrPnlExchSrvIml.saveDetailedReportToExcel_State;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelTools {

    private ServletContext servletContext;

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public String saveDetailedReportToExcel(String dateFrom, String dateTo, List<List<String>> res_detailed, List<List<String>> res_groupedBy, String groupBy) {

        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheetDetailed = wb.createSheet("Timesheet detailed report");
        sheetDetailed.setDefaultColumnWidth(18);
        sheetDetailed.setColumnWidth(0, 6000);
        sheetDetailed.setColumnWidth(1, 8000);
        sheetDetailed.setColumnWidth(2, 6000);
        sheetDetailed.setColumnWidth(3, 6000);
        sheetDetailed.createFreezePane(4, 1);

        CellStyle cssHeader = wb.createCellStyle();
        cssHeader.setBorderBottom(BorderStyle.THIN);
        cssHeader.setBorderLeft(BorderStyle.THIN);
        cssHeader.setBorderRight(BorderStyle.THIN);
        cssHeader.setBorderTop(BorderStyle.THIN);
        cssHeader.setAlignment(HorizontalAlignment.CENTER);
        cssHeader.setVerticalAlignment(VerticalAlignment.CENTER);
        cssHeader.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
        cssHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cssHeader.setWrapText(true);
        Font f = wb.createFont();
        f.setBold(true);
        f.setFontHeightInPoints((short) 13);
        cssHeader.setFont(f);

        CellStyle css = wb.createCellStyle();
        css.setBorderBottom(BorderStyle.THIN);
        css.setBorderLeft(BorderStyle.THIN);
        css.setBorderRight(BorderStyle.THIN);
        css.setBorderTop(BorderStyle.THIN);
        css.setAlignment(HorizontalAlignment.CENTER);

        CellStyle cssBold = wb.createCellStyle();
        cssBold.setBorderBottom(BorderStyle.THIN);
        cssBold.setBorderLeft(BorderStyle.THIN);
        cssBold.setBorderRight(BorderStyle.THIN);
        cssBold.setBorderTop(BorderStyle.THIN);
        cssBold.setAlignment(HorizontalAlignment.CENTER);
        cssBold.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cssBold.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        f = wb.createFont();
        f.setBold(true);
        cssBold.setFont(f);

        CellStyle cssWorks = wb.createCellStyle();
        cssWorks.setBorderBottom(BorderStyle.THIN);
        cssWorks.setBorderLeft(BorderStyle.THIN);
        cssWorks.setBorderRight(BorderStyle.THIN);
        cssWorks.setBorderTop(BorderStyle.THIN);
        cssWorks.setAlignment(HorizontalAlignment.LEFT);
        cssWorks.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cssWorks.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cssWorks.setVerticalAlignment(VerticalAlignment.CENTER);

        // **************************** detailed part ********************
        int rowNum = 0;
        int quantityOfColumn = 0;
        List<Integer> rowSumByRows = new ArrayList<>();
        for (List<String> rowRes : res_detailed) {
            quantityOfColumn = rowRes.size();
        }
        for (int i = 0; i < quantityOfColumn - 4; i++) {
            rowSumByRows.add(0);
        }

        String nameTempA = "";
        Integer sameRowStartA = 0;
        Boolean startEqualA = false;
        String nameTempB = "";
        Integer sameRowStartB = 0;
        Boolean startEqualB = false;
        String nameTempC = "";
        Integer sameRowStartC = 0;
        Boolean startEqualC = false;

        for (List<String> rowRes : res_detailed) {
            Row row = sheetDetailed.createRow(rowNum++);
            int colNum = 0;
            int sumByColumns = 0;
            for (String elem : rowRes) {
                Cell cell = row.createCell(colNum++);
                if (rowNum == 1) {
                    row.setHeightInPoints(55);
                    cell.setCellValue(elem);
                    cell.setCellStyle(cssHeader);
                } else {
                    if (rowNum > 1 && colNum > 4) {
                        cell.setCellValue(Integer.parseInt(elem));
                        cell.setCellStyle(css);
                        sumByColumns += Integer.parseInt(elem);
                        rowSumByRows.set(colNum - 5, rowSumByRows.get(colNum - 5) + Integer.parseInt(elem));
                    } else {
                        cell.setCellValue(elem);
                        cell.setCellStyle(cssWorks);

                        //merge cells A column
                        if (colNum == 1) {
                            if (elem.equals(nameTempA)) {
                                startEqualA = true;
                            } else {
                                if (startEqualA) {
                                    sheetDetailed.addMergedRegion(CellRangeAddress.valueOf("$A$" + sameRowStartA + ":$A$" + (rowNum - 1)));
                                    startEqualA = false;
                                }
                                nameTempA = elem;
                                sameRowStartA = rowNum;
                            }
                        }
                        //merge cells B column
                        if (colNum == 2) {
                            if (elem.equals(nameTempB)) {
                                startEqualB = true;
                            } else {
                                if (startEqualB) {
                                    sheetDetailed.addMergedRegion(CellRangeAddress.valueOf("$B$" + sameRowStartB + ":$B$" + (rowNum - 1)));
                                    startEqualB = false;
                                }
                                nameTempB = elem;
                                sameRowStartB = rowNum;
                            }
                        }
                        //merge cells C column
                        if (colNum == 3) {
                            if (elem.equals(nameTempC)) {
                                startEqualC = true;
                            } else {
                                if (startEqualC) {
                                    sheetDetailed.addMergedRegion(CellRangeAddress.valueOf("$C$" + sameRowStartC + ":$C$" + (rowNum - 1)));
                                    startEqualC = false;
                                }
                                nameTempC = elem;
                                sameRowStartC = rowNum;
                            }
                        }
                    }
                }
            }
            if (rowNum > 1) {
                Cell cell = row.createCell(colNum++);
                cell.setCellValue(sumByColumns);
                cell.setCellStyle(cssBold);
            }
        }
        Row row = sheetDetailed.createRow(rowNum++);
        for (int i = 4; i < quantityOfColumn; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(rowSumByRows.get(i - 4));
            cell.setCellStyle(cssBold);
        }

        // **************************** grouped part ********************
        XSSFSheet sheetGroupedBy = wb.createSheet("Timesheet grouped report");
        sheetGroupedBy.setDefaultColumnWidth(18);
        sheetGroupedBy.setColumnWidth(0, 6000);
        int quantityOfStringColumns = 3;
        Boolean mergeByA = false;
        Boolean mergeByB = false;

        switch (groupBy) {
            case "work_form_id":
                sheetGroupedBy.setColumnWidth(1, 8000);
                sheetGroupedBy.setColumnWidth(2, 6000);
                sheetGroupedBy.createFreezePane(3, 1);
                quantityOfStringColumns = 3;
                mergeByA = true;
                mergeByB = true;
                break;
            case "work_title_id":
                sheetGroupedBy.setColumnWidth(1, 8000);
                sheetGroupedBy.createFreezePane(2, 1);
                quantityOfStringColumns = 2;
                mergeByA = true;
                break;
            case "work_type_id":
                sheetGroupedBy.createFreezePane(1, 1);
                quantityOfStringColumns = 1;
                break;
        }

        rowNum = 0;
        quantityOfColumn = 0;
        rowSumByRows = new ArrayList<>();
        for (List<String> rowRes : res_groupedBy) {
            quantityOfColumn = rowRes.size();
        }
        for (int i = 0; i < quantityOfColumn - quantityOfStringColumns; i++) {
            rowSumByRows.add(0);
        }

        nameTempA = "";
        sameRowStartA = 0;
        startEqualA = false;
        nameTempB = "";
        sameRowStartB = 0;
        startEqualB = false;

        for (List<String> rowRes : res_groupedBy) {
            row = sheetGroupedBy.createRow(rowNum++);
            int colNum = 0;
            int sumByColumns = 0;
            for (String elem : rowRes) {
                Cell cell = row.createCell(colNum++);
                if (rowNum == 1) {
                    row.setHeightInPoints(55);
                    cell.setCellValue(elem);
                    cell.setCellStyle(cssHeader);
                } else {
                    if (rowNum > 1 && colNum > quantityOfStringColumns) {
                        cell.setCellValue(Integer.parseInt(elem));
                        cell.setCellStyle(css);
                        sumByColumns += Integer.parseInt(elem);
                        rowSumByRows.set(colNum - quantityOfStringColumns - 1, rowSumByRows.get(colNum - quantityOfStringColumns - 1) + Integer.parseInt(elem));
                    } else {
                        cell.setCellValue(elem);
                        cell.setCellStyle(cssWorks);

                        //merge cells A column
                        if (colNum == 1 && mergeByA) {
                            if (elem.equals(nameTempA)) {
                                startEqualA = true;
                            } else {
                                if (startEqualA) {
                                    sheetGroupedBy.addMergedRegion(CellRangeAddress.valueOf("$A$" + sameRowStartA + ":$A$" + (rowNum - 1)));
                                    startEqualA = false;
                                }
                                nameTempA = elem;
                                sameRowStartA = rowNum;
                            }
                        }
                        //merge cells B column
                        if (colNum == 2 && mergeByB) {
                            if (elem.equals(nameTempB)) {
                                startEqualB = true;
                            } else {
                                if (startEqualB) {
                                    sheetGroupedBy.addMergedRegion(CellRangeAddress.valueOf("$B$" + sameRowStartB + ":$B$" + (rowNum - 1)));
                                    startEqualB = false;
                                }
                                nameTempB = elem;
                                sameRowStartB = rowNum;
                            }
                        }
                    }
                }
            }
            if (rowNum > 1) {
                Cell cell = row.createCell(colNum++);
                cell.setCellValue(sumByColumns);
                cell.setCellStyle(cssBold);
            }
        }
        row = sheetGroupedBy.createRow(rowNum++);
        for (int i = quantityOfStringColumns; i < quantityOfColumn; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(rowSumByRows.get(i - quantityOfStringColumns));
            cell.setCellStyle(cssBold);
        }
        String webPath = "DetailedReport from " + dateFrom + " to " + dateTo + ".xlsx";
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(servletContext.getRealPath("/") + "\\" + webPath);
            wb.write(fileOutputStream);
            wb.close();
        } catch (FileNotFoundException ex) {
            WebLogger.writeError(ex.getMessage(), ExcelTools.class);
            saveDetailedReportToExcel_State = ex.getMessage();
        } catch (IOException ex) {
            WebLogger.writeError(ex.getMessage(), ExcelTools.class);
            saveDetailedReportToExcel_State = ex.getMessage();
        }
        return webPath;
    }

    public Map<String,Integer> getPlanData(InputStream fileContent, String planYear) {
        Map<String,Integer> res = new TreeMap<String,Integer>();
        try {
            Workbook workbook = new XSSFWorkbook(fileContent);
            Sheet sheet = workbook.getSheetAt(2);

            int rowNum = 5;
            while (!sheet.getRow(rowNum).getCell(1).getStringCellValue().equals("")) {
                String planWorkTitle = planYear + "_" + sheet.getRow(rowNum).getCell(1).getStringCellValue().trim();
                Integer planHours = ((int) sheet.getRow(rowNum).getCell(4).getNumericCellValue()) * 8;
                res.put(planWorkTitle, planHours);
                rowNum++;
            }
        } catch (IOException ex) {
            WebLogger.writeError(ex.getMessage(), ExcelTools.class);
        } finally {
            if (fileContent != null) {
                try {
                    fileContent.close();
                } catch (IOException e) {
                    WebLogger.writeError(e.getMessage(), ExcelTools.class);
                }
            }
        }
        return res;
    }
}
