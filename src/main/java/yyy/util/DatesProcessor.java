package yyy.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DatesProcessor {

    private static DatesProcessor ourInstance = new DatesProcessor();

    public static DatesProcessor getInstance() {
        return ourInstance;
    }

    private Calendar calendar;
    private DateFormat utcFormat;
    private DateFormat mySQLFormat;
    private SimpleDateFormat simpleDateFormat;

    private DatesProcessor() {
        calendar = Calendar.getInstance();
        utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        mySQLFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    }

    public Date increment(Date date, int days) {
        //Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }

    public Date getTodayRoundedShifted() {
        calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == 1) {
            return calendar.getTime();
        }
        return increment(calendar.getTime(), 8 - dayOfWeek);
    }

    public Date getLocalDate(String dateString) {
        Date date = null;

        try {
            date = utcFormat.parse(dateString);
        } catch (ParseException e) {
            WebLogger.writeError(e.getMessage(), DatesProcessor.class);
        }

        return date;
    }

    public Date getSimpleDate(String dateString) {
        Date date = null;

        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            WebLogger.writeError(e.getMessage(), DatesProcessor.class);
        }

        return date;
    }

    public String toSimpleFormatString(Date dateReported) {
        return simpleDateFormat.format(dateReported);
    }

    public String toMySQLFormatDate(Date dateReported) {
        return mySQLFormat.format(dateReported);
    }

    public String todayMySQLFormatDate() {
        Calendar calendarToday =  Calendar.getInstance();
        return mySQLFormat.format(calendarToday.getTime());
    }
}
