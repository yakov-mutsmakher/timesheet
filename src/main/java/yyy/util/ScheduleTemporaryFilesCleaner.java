package yyy.util;

import java.io.File;
import java.util.Calendar;
import javax.servlet.ServletContext;

public class ScheduleTemporaryFilesCleaner implements Runnable {

    private ServletContext servletContext;

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public void run() {
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        if (hour >= 0 && hour < 6) {
            cleanTemporaryFiles();
        }
    }

    private void cleanTemporaryFiles() {
        try {
            String path = servletContext.getRealPath("/");
            File file = new File(path);
            File[] items = file.listFiles();
            int countDeleted = 0;
            int countXlSX = 0;
            for (File item : items) {
                if (item.getName().endsWith(".xlsx")) {
                    countXlSX++;
                    if (item.delete()) {
                        countDeleted++;
                    }
                }
            }
            if (countXlSX > 0) {
                WebLogger.writeInfo(countDeleted + " from " + countXlSX + " temporary xlsx files has been successfully deleted", ScheduleTemporaryFilesCleaner.class);
            }
            if (countXlSX == 0) {
                WebLogger.writeInfo("There are not temporary xlsx files to deleting", ScheduleTemporaryFilesCleaner.class);
            }
        } catch (Exception e) {
            WebLogger.writeError(e.getMessage(), ScheduleTemporaryFilesCleaner.class);
        }
    }
}
