package yyy.util;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.ServletContext;

public class ActivDirectoryChecker {

    private ServletContext servletContext;

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public String searchEmployee(String login) {
        String result = "notExist";

        try {
            Hashtable<String, String> ldapEnv = new Hashtable<String, String>(11);
            ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            ldapEnv.put(Context.PROVIDER_URL, servletContext.getInitParameter("PROVIDER_URL"));
            ldapEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
            ldapEnv.put(Context.SECURITY_PRINCIPAL, servletContext.getInitParameter("SECURITY_PRINCIPAL"));
            ldapEnv.put(Context.SECURITY_CREDENTIALS, servletContext.getInitParameter("SECURITY_CREDENTIALS"));
            DirContext ldapContext = new InitialDirContext(ldapEnv);

            // Create the search controls         
            SearchControls searchCtls = new SearchControls();
            String returnedAttr[] = {"sn", "givenName", "sAMAccountName"};
            searchCtls.setReturningAttributes(returnedAttr);
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            //specify the LDAP search filter
            String searchFilter = "(&(sAMAccountName=" + login + "))";

            //Specify the Base for the search
            String searchBase = servletContext.getInitParameter("searchBase");

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, searchFilter, searchCtls);

            int totalResults = 0;
            String firstName = "", secondName = "";
            //Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                totalResults++;
                Attributes attrs = sr.getAttributes();
                firstName = (String) attrs.get("sn").get(0);
                secondName = (String) attrs.get("givenName").get(0);
            }

            if (totalResults > 0) {
                result = firstName + " " + secondName;
            }

            ldapContext.close();
        } catch (Exception e) {
            WebLogger.writeError(e.getMessage(), ActivDirectoryChecker.class);
        }

        return result;
    }

}
