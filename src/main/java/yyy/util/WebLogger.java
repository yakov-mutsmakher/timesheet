package yyy.util;

import org.apache.log4j.Logger;

public class WebLogger {

    public static void writeInfo(String message, Class<?> clazz) {
        Logger.getLogger(clazz).info(Thread.currentThread().getStackTrace()[2].getLineNumber() + " - " + message);
    }

    public static void writeError(String message, Class<?> clazz) {
        Logger.getLogger(clazz).error(Thread.currentThread().getStackTrace()[2].getLineNumber() + " - " + message);
    }
}
