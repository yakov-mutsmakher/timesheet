package yyy.listener;

import Dao.*;
import Dao.Impl.*;
import Service.AdminPanelExchangeService;
import Service.DomainObjectsManager;
import Service.Impl.DomainObjectsManagerImpl;
import json.*;
import json.impl.*;
import Service.EmployeesExchangeService;
import Service.Impl.AdminPnlExchSrvImpl;
import Service.Impl.SprUsrPnlExchSrvIml;
import Service.Impl.EmplysExchSrvImpl;
import yyy.objectPools.WorkFormPool;
import yyy.objectPools.WorkInstancePool;
import yyy.objectPools.WorkTitlePool;
import yyy.objectPools.WorkTypePool;
import Service.ReportsExchangeService;
import Service.Impl.RprtsExchSrvImpl;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import Service.SuperUserPanelExchangeService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import yyy.util.ActivDirectoryChecker;
import yyy.util.ExcelTools;
import yyy.util.ScheduleTemporaryFilesCleaner;
import yyy.util.WebLogger;

public class InitializerListener implements ServletContextListener {

    private ScheduledExecutorService scheduler;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();

        DBProceduresManager dbProceduresManager = new DBProceduresManagerImpl();
        dbProceduresManager.setServletContext(servletContext);
        servletContext.setAttribute("dbProceduresManager", dbProceduresManager);

        EmployeeDao employeeDao = new EmplyDaoImlpHOME();
        employeeDao.setServletContext(servletContext);
        servletContext.setAttribute("employeeDao", employeeDao);

        ReportRecordDao reportRecordDao = new RprtRcrdDaoImlpHOME();
        reportRecordDao.setServletContext(servletContext);
        servletContext.setAttribute("reportRecordDao", reportRecordDao);

        SuperViewerDAO superViewerDAO = new SuperViewerDaoImpl();
        superViewerDAO.setServletContext(servletContext);
        servletContext.setAttribute("superViewerDAO", superViewerDAO);

        AdminPageDao adminPageDao = new AdminPageDaoImpl();
        adminPageDao.setServletContext(servletContext);
        servletContext.setAttribute("adminPageDao", adminPageDao);

        WorkTypeDao workTypeDao = new WorkTypeDaoImpl();
        workTypeDao.setServletContext(servletContext);
        servletContext.setAttribute("workTypeDao", workTypeDao);

        WorkTitleDao workTitleDao = new WorkTitleDaoImpl();
        workTitleDao.setServletContext(servletContext);
        servletContext.setAttribute("workTitleDao", workTitleDao);

        WorkFormDao workFormDao = new WorkFormDaoImpl();
        workFormDao.setServletContext(servletContext);
        servletContext.setAttribute("workFormDao", workFormDao);

        WorkInstanceDao workInstanceDao = new WorkInstanceDaoImpl();
        workInstanceDao.setServletContext(servletContext);
        servletContext.setAttribute("workInstanceDao", workInstanceDao);

        ReportsExchangeService reportsExchangeService = new RprtsExchSrvImpl();
        reportsExchangeService.setServletContext(servletContext);
        servletContext.setAttribute("reportsExchangeService", reportsExchangeService);

        EmployeesExchangeService employeesExchangeService = new EmplysExchSrvImpl();
        employeesExchangeService.setServletContext(servletContext);
        servletContext.setAttribute("employeesExchangeService", employeesExchangeService);

        SuperUserPanelExchangeService superUserPanelExchangeService = new SprUsrPnlExchSrvIml();
        superUserPanelExchangeService.setServletContext(servletContext);
        servletContext.setAttribute("superUserPanelExchangeService", superUserPanelExchangeService);

        AdminPanelExchangeService adminPanelExchangeService = new AdminPnlExchSrvImpl();
        adminPanelExchangeService.setServletContext(servletContext);
        servletContext.setAttribute("adminPanelExchangeService", adminPanelExchangeService);

        DomainObjectsManager domainObjectsManager = new DomainObjectsManagerImpl();
        domainObjectsManager.setServletContext(servletContext);
        servletContext.setAttribute("domainObjectsManager", domainObjectsManager);

        servletContext.setAttribute("workTypePool", new WorkTypePool());

        servletContext.setAttribute("workTitlePool", new WorkTitlePool());

        servletContext.setAttribute("workFormPool", new WorkFormPool());

        servletContext.setAttribute("workInstancePool", new WorkInstancePool());

        UserReportJSONConvertor userReportJSONConvertor = new UserReportJSONConvertorImpl();
        userReportJSONConvertor.setServletContext(servletContext);
        servletContext.setAttribute("userReportJSONConvertor", userReportJSONConvertor);

        WorkInstanceJSONConvertor workInstanceJSONConvertor = new WorkInstanceJSONConvertorImlp();
        workInstanceJSONConvertor.setServletContext(servletContext);
        servletContext.setAttribute("workInstanceJSONConvertor", workInstanceJSONConvertor);

        UserReportRecordsLineJSONConvertor userReportRecordsLineJSONConvertor = new UserReportRecordsLineJSONConvertorImpl();
        userReportRecordsLineJSONConvertor.setServletContext(servletContext);
        servletContext.setAttribute("userReportRecordsLineJSONConvertor", userReportRecordsLineJSONConvertor);

        ReportRecordJSONConvertor reportRecordJSONConvertor = new ReportRecordJSONConvertorImlp();
        reportRecordJSONConvertor.setServletContext(servletContext);
        servletContext.setAttribute("reportRecordJSONConvertor", reportRecordJSONConvertor);

        WorkTypeJSONConvertor workTypeJSONConvertor = new WorkTypeJSONConvertorImpl();
        workTypeJSONConvertor.setServletContext(servletContext);
        servletContext.setAttribute("workTypeJSONConvertor", workTypeJSONConvertor);

        WorkTitleJSONConvertor workTitleJSONConvertor = new WorkTitleJSONConvertorImpl();
        workTitleJSONConvertor.setServletContext(servletContext);
        servletContext.setAttribute("workTitleJSONConvertor", workTitleJSONConvertor);

        WorkFormJSONConvertor workFormJSONConvertor = new WorkFormJSONConvertorImpl();
        workFormJSONConvertor.setServletContext(servletContext);
        servletContext.setAttribute("workFormJSONConvertor", workFormJSONConvertor);

        ExcelTools excelTools = new ExcelTools();
        excelTools.setServletContext(servletContext);
        servletContext.setAttribute("excelTools", excelTools);

        ActivDirectoryChecker activDirectoryChecker = new ActivDirectoryChecker();
        activDirectoryChecker.setServletContext(servletContext);
        servletContext.setAttribute("activDirectoryChecker", activDirectoryChecker);

        scheduler = Executors.newSingleThreadScheduledExecutor();
        ScheduleTemporaryFilesCleaner scheduleTemporaryFilesCleaner = new ScheduleTemporaryFilesCleaner();
        scheduleTemporaryFilesCleaner.setServletContext(servletContext);
        scheduler.scheduleAtFixedRate(scheduleTemporaryFilesCleaner, 0, 6, TimeUnit.HOURS);

        WebLogger.writeInfo("!!! Servlet context has been initialized !!!", InitializerListener.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        scheduler.shutdownNow();
        WebLogger.writeInfo("!!! Servlet context has been destroyed !!!", InitializerListener.class);
    }
}
