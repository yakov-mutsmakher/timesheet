package yyy.listener;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.*;
import javax.sql.DataSource;
import yyy.util.WebLogger;

public class CRUDConnLoader implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        try {
            Context initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:/comp/env");

            DataSource dataSource = (DataSource) envCtx.lookup("/jdbc/rootConn");
            ServletContext servletContext = servletContextEvent.getServletContext();
            servletContext.setAttribute("rootConnDataSource", dataSource);
        } catch (Exception e) {
            WebLogger.writeError(e.getMessage(), CRUDConnLoader.class);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }

}