package Service;

import Domain.Employee;
import Domain.UnitedReport;
import Domain.UserReport;
import javax.servlet.ServletContext;
import java.util.Date;

public interface ReportsExchangeService {

    final int DEFAULT_NUM_DAYS_REPORTED = 13;

    public void setServletContext(ServletContext servletContext);

    public UserReport getUserReport(Employee employee);

    public UserReport getUserReport(Employee employee, int daysReported);

    public UserReport getUserReport(Employee employee, Date dateFrom);

    public UserReport getUserReport(Employee employee, Date dateFrom, Date dateTo);

    public Integer saveReport(UserReport userReport, Employee employee);
    
    public UserReport getSummaryReport(Employee employee);

    public UserReport getSummaryReport(Employee employee, Date dateFrom, Date dateTo);
    
    public UnitedReport getUnitedReport(Employee employee, UnitedReport unitedReport);
    
    public UnitedReport getUnitedReport(Employee employee, Date dateFrom, Date dateTo, UnitedReport unitedReport);
}
