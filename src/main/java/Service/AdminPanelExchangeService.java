package Service;

import Domain.Employee;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;

public interface AdminPanelExchangeService {

    void setServletContext(ServletContext servletContext);

    public String updateDataByPlanData(Map<String, Integer> planData);

    public String changeRole(Integer id, String oldRole, String newRole, List<Integer> selectedEmployeesID);

    public List<Integer> adminSelectedID(Integer id);

    public String addNewEmployee(String login, String newEmployeeName, String newRoleAdd, List<Integer> selectedEmployeesIDAdd);

}
