package Service.Impl;

import Dao.ReportRecordDao;
import Domain.Employee;
import Domain.UserReport;
import Domain.ReportRecord;
import Domain.UnitedReport;
import Service.ReportsExchangeService;
import java.util.ArrayList;
import yyy.util.DatesProcessor;
import javax.servlet.ServletContext;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class RprtsExchSrvImpl implements ReportsExchangeService {

    private ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public UserReport getUserReport(Employee employee) {
        UserReport result = getUserReport(employee, DEFAULT_NUM_DAYS_REPORTED);
        return result;
    }

    @Override
    public UserReport getUserReport(Employee employee, int daysReported) {

        DatesProcessor datesProcessor = DatesProcessor.getInstance();

        Date dateTo = datesProcessor.getTodayRoundedShifted();
        Date dateFrom = datesProcessor.increment(dateTo, -DEFAULT_NUM_DAYS_REPORTED);

        UserReport result = getUserReport(employee, dateFrom, dateTo);
        return result;
    }

    @Override
    public UserReport getUserReport(Employee employee, Date dateFrom) {
        DatesProcessor datesProcessor = DatesProcessor.getInstance();

        Date dateTo = datesProcessor.increment(dateFrom, DEFAULT_NUM_DAYS_REPORTED);

        UserReport result = getUserReport(employee, dateFrom, dateTo);

        return result;
    }

    @Override
    public UserReport getUserReport(Employee employee, Date dateFrom, Date dateTo) {
        ReportRecordDao reportRecordDao = (ReportRecordDao) servletContext.getAttribute("reportRecordDao");
        Set<ReportRecord> recordSet = reportRecordDao.getRecordSet(employee, dateFrom, dateTo);

        return new UserReport(dateFrom, dateTo, recordSet);
    }

    @Override
    public Integer saveReport(UserReport userReport, Employee employee) {
        ReportRecordDao reportRecordDao = (ReportRecordDao) servletContext.getAttribute("reportRecordDao");
        Set<ReportRecord> reportRecordSet = userReport.getReportRecords();

        UserReport userReportBeforeChange = getUserReport(employee, userReport.DATE_FROM, userReport.DATE_TO);
        Set<ReportRecord> reportRecordSetBeforeChange = userReportBeforeChange.getReportRecords();

        int changedInNewRow = 0;
        int changedInExistingRow = 0;
        boolean isNewRow;
        List<Integer> newId = new ArrayList<>();
        for (ReportRecord reportRecord : reportRecordSet) {
            isNewRow = true;
            for (ReportRecord reportRecordBeforChange : reportRecordSetBeforeChange) {
                if (reportRecord.getWorkInstance().getId() == reportRecordBeforChange.getWorkInstance().getId()) {
                    isNewRow = false;
                    if (reportRecord.getDateReported().compareTo(reportRecordBeforChange.getDateReported()) == 0
                            && reportRecord.getHoursNum() != reportRecordBeforChange.getHoursNum()) {
                        changedInExistingRow++;
                    }
                }
            }
            if (isNewRow && reportRecord.getHoursNum() != 0) {
                changedInNewRow++;
            }
        }
        Integer processedField = reportRecordDao.updateRecords(reportRecordSet, employee);
        if (processedField == reportRecordSet.size()) {
            return changedInExistingRow + changedInNewRow;
        } else {
            return processedField;
        }
    }

    @Override
    public UserReport getSummaryReport(Employee employee) {
        DatesProcessor datesProcessor = DatesProcessor.getInstance();

        Date dateTo = datesProcessor.getTodayRoundedShifted();
        Date dateFrom = datesProcessor.increment(dateTo, -DEFAULT_NUM_DAYS_REPORTED);

        UserReport result = getSummaryReport(employee, dateFrom, dateTo);
        return result;
    }

    @Override
    public UserReport getSummaryReport(Employee employee, Date dateFrom, Date dateTo) {
        ReportRecordDao reportRecordDao = (ReportRecordDao) servletContext.getAttribute("reportRecordDao");
        Set<ReportRecord> recordSet = reportRecordDao.getSummaryRecordSet(employee, dateFrom, dateTo);
        return new UserReport(dateFrom, dateFrom, recordSet); //all result hours have been put in datefrom column
    }

    @Override
    public UnitedReport getUnitedReport(Employee employee, UnitedReport unitedReport) {
        DatesProcessor datesProcessor = DatesProcessor.getInstance();

        Date dateTo = datesProcessor.getTodayRoundedShifted();
        Date dateFrom = datesProcessor.increment(dateTo, -DEFAULT_NUM_DAYS_REPORTED);

        UnitedReport result = getUnitedReport(employee, dateFrom, dateTo, unitedReport);
        return result;
    }

    @Override
    public UnitedReport getUnitedReport(Employee employee, Date dateFrom, Date dateTo, UnitedReport unitedReport) {
        ReportRecordDao reportRecordDao = (ReportRecordDao) servletContext.getAttribute("reportRecordDao");
        unitedReport = reportRecordDao.getUnitedReport(employee, dateFrom, dateTo, unitedReport);

        return unitedReport;
    }
}
