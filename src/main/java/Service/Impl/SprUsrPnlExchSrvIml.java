package Service.Impl;

import Dao.SuperViewerDAO;
import Domain.Employee;
import Domain.PlanCompareElement;
import Domain.SuperViewerUnitedReportElement;
import static Service.ReportsExchangeService.DEFAULT_NUM_DAYS_REPORTED;
import java.util.List;
import javax.servlet.ServletContext;
import Service.SuperUserPanelExchangeService;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import yyy.util.DatesProcessor;
import yyy.util.ExcelTools;

public class SprUsrPnlExchSrvIml implements SuperUserPanelExchangeService {

    private ServletContext servletContext;
    public static String saveDetailedReportToExcel_State;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public List<Employee> getEmployees(String onlyActive, Employee currentEmployee) {
        SuperViewerDAO superViewerDAO = (SuperViewerDAO) servletContext.getAttribute("superViewerDAO");
        return superViewerDAO.getEmployees(onlyActive, currentEmployee);
    }

    @Override
    public Map<Integer, Integer> getSuperViewerReport(List<Integer> selectedEmployeesID) {
        DatesProcessor datesProcessor = DatesProcessor.getInstance();
        Date dateTo = datesProcessor.getTodayRoundedShifted();
        Date dateFrom = datesProcessor.increment(dateTo, -DEFAULT_NUM_DAYS_REPORTED);
        return getSuperViewerReport(dateFrom, dateTo, selectedEmployeesID);
    }

    @Override
    public Map<Integer, Integer> getSuperViewerReport(Date dateFrom, Date dateTo, List<Integer> selectedEmployeesID) {
        SuperViewerDAO superViewerDAO = (SuperViewerDAO) servletContext.getAttribute("superViewerDAO");
        Map<Integer, Integer> res = superViewerDAO.getSuperViewerReport(dateFrom, dateTo, selectedEmployeesID);
        if (res.size() < selectedEmployeesID.size()) {
            for (Integer selectedEmployeesIDElem : selectedEmployeesID) {
                res.putIfAbsent(selectedEmployeesIDElem, 0);
            }
        }
        return res;
    }

    @Override
    public Map<String, SuperViewerUnitedReportElement> getSuperViewerUnitedReport(List<Integer> selectedEmployeesID, Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport) {
        DatesProcessor datesProcessor = DatesProcessor.getInstance();
        Date dateTo = datesProcessor.getTodayRoundedShifted();
        Date dateFrom = datesProcessor.increment(dateTo, -DEFAULT_NUM_DAYS_REPORTED);
        return getSuperViewerUnitedReport(dateFrom, dateTo, selectedEmployeesID, superViewerUnitedReport);
    }

    @Override
    public Map<String, SuperViewerUnitedReportElement> getSuperViewerUnitedReport(Date dateFrom, Date dateTo, List<Integer> selectedEmployeesID, Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport) {
        SuperViewerDAO superViewerDAO = (SuperViewerDAO) servletContext.getAttribute("superViewerDAO");
        superViewerUnitedReport = superViewerDAO.getSuperViewerUnitedReport(dateFrom, dateTo, selectedEmployeesID, superViewerUnitedReport);
        return superViewerUnitedReport;
    }

    @Override
    public String saveDetailedReportToExcel(List<Integer> selectedEmployeesID, String groupBy) {
        DatesProcessor datesProcessor = DatesProcessor.getInstance();
        Date dateTo = datesProcessor.getTodayRoundedShifted();
        Date dateFrom = datesProcessor.increment(dateTo, -DEFAULT_NUM_DAYS_REPORTED);
        return saveDetailedReportToExcel(dateFrom, dateTo, selectedEmployeesID, groupBy);
    }

    @Override
    public String saveDetailedReportToExcel(Date dateFrom, Date dateTo, List<Integer> selectedEmployeesID, String groupBy) {
        SuperViewerDAO superViewerDAO = (SuperViewerDAO) servletContext.getAttribute("superViewerDAO");
        List<Employee> selectedEmployeesList = superViewerDAO.getEmployeesByID(selectedEmployeesID);

        String groupByDetailed = "work_instance_id";
        List<List<String>> res_detailed = superViewerDAO.getDetailedReport(dateFrom, dateTo, selectedEmployeesList, groupByDetailed);
        List<List<String>> res_groupedBy = superViewerDAO.getDetailedReport(dateFrom, dateTo, selectedEmployeesList, groupBy);

        ExcelTools excelTools = (ExcelTools) servletContext.getAttribute("excelTools");
        DatesProcessor datesProcessor = DatesProcessor.getInstance();
        String webPath = excelTools.saveDetailedReportToExcel(datesProcessor.toSimpleFormatString(dateFrom), datesProcessor.toSimpleFormatString(dateTo), res_detailed, res_groupedBy, groupBy);

        return webPath;
    }

    @Override
    public List<PlanCompareElement> getPlanCompare(String yearPlanCompare) {
        SuperViewerDAO superViewerDAO = (SuperViewerDAO) servletContext.getAttribute("superViewerDAO");
        List<PlanCompareElement> listPlanCompareElement = superViewerDAO.getPlanCompare(yearPlanCompare);
        return listPlanCompareElement;
    }

}
