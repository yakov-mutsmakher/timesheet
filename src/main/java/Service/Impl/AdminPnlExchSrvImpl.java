package Service.Impl;

import Dao.AdminPageDao;
import Service.AdminPanelExchangeService;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;

public class AdminPnlExchSrvImpl implements AdminPanelExchangeService {

    private ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @Override
    public String updateDataByPlanData(Map<String, Integer> planData) {
        AdminPageDao adminPageDao = (AdminPageDao) servletContext.getAttribute("adminPageDao");
        return adminPageDao.updateDataByPlanData(planData);
    }

    @Override
    public String changeRole(Integer id, String oldRole, String newRole, List<Integer> selectedEmployeesID) {
        AdminPageDao adminPageDao = (AdminPageDao) servletContext.getAttribute("adminPageDao");
        return adminPageDao.changeRole(id, oldRole, newRole, selectedEmployeesID);
    }

    @Override
    public List<Integer> adminSelectedID(Integer id) {
        AdminPageDao adminPageDao = (AdminPageDao) servletContext.getAttribute("adminPageDao");
        return adminPageDao.adminSelectedID(id);
    }

    @Override
    public String addNewEmployee(String login, String newEmployeeName, String newRoleAdd, List<Integer> selectedEmployeesIDAdd) {
        AdminPageDao adminPageDao = (AdminPageDao) servletContext.getAttribute("adminPageDao");
        return adminPageDao.addNewEmployee(login, newEmployeeName, newRoleAdd, selectedEmployeesIDAdd);
    }

}
