package Service;

import Domain.Employee;
import Domain.PlanCompareElement;
import Domain.SuperViewerUnitedReportElement;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;

public interface SuperUserPanelExchangeService {

    void setServletContext(ServletContext servletContext);

    public List<Employee> getEmployees(String onlyActive, Employee currentEmployee);

    public Map<Integer, Integer> getSuperViewerReport(List<Integer> selectedEmployeesID);

    public Map<Integer, Integer> getSuperViewerReport(Date dateFrom, Date dateTo, List<Integer> selectedEmployeesID);

    public Map<String, SuperViewerUnitedReportElement> getSuperViewerUnitedReport(List<Integer> selectedEmployeesID, Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport);

    public Map<String, SuperViewerUnitedReportElement> getSuperViewerUnitedReport(Date dateFrom, Date dateTo, List<Integer> selectedEmployeesID, Map<String, SuperViewerUnitedReportElement> superViewerUnitedReport);

    public String saveDetailedReportToExcel(List<Integer> selectedEmployeesID, String groupBy);

    public String saveDetailedReportToExcel(Date dateFrom, Date dateTo, List<Integer> selectedEmployeesID, String groupBy);

    public List<PlanCompareElement> getPlanCompare(String yearPlanCompare);
}
