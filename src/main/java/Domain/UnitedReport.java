package Domain;

import java.util.Map;
import java.util.Objects;

public class UnitedReport {

    private Map<String, UnitedReportElement> unRep;

    public Map<String, UnitedReportElement> getUnRep() {
        return unRep;
    }

    public void setUnRep(Map<String, UnitedReportElement> unRep) {
        this.unRep = unRep;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.unRep);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnitedReport other = (UnitedReport) obj;
        return Objects.equals(this.unRep, other.unRep);
    }

    @Override
    public String toString() {
        return "UnitedReport{" + "unRep : " + unRep + '}';
    }

}
