package Domain;

import java.util.Objects;

public class PlanCompareElement {

    private String workTitle;
    private int completedHours;
    private Integer planHours;

    public PlanCompareElement() {
    }
    
    public PlanCompareElement(String workTitle, int completedHours, Integer planHours) {
        this.workTitle = workTitle;
        this.completedHours = completedHours;
        this.planHours = planHours;
    }

    public String getWorkTitle() {
        return workTitle;
    }

    public void setWorkTitle(String workTitle) {
        this.workTitle = workTitle;
    }

    public int getCompletedHours() {
        return completedHours;
    }

    public void setCompletedHours(int completedHours) {
        this.completedHours = completedHours;
    }

    public Integer getPlanHours() {
        return planHours;
    }

    public void setPlanHours(Integer planHours) {
        this.planHours = planHours;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.workTitle);
        hash = 37 * hash + this.completedHours;
        hash = 37 * hash + Objects.hashCode(this.planHours);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlanCompareElement other = (PlanCompareElement) obj;
        if (this.completedHours != other.completedHours) {
            return false;
        }
        if (!Objects.equals(this.workTitle, other.workTitle)) {
            return false;
        }
        if (!Objects.equals(this.planHours, other.planHours)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PlanCompareElement{" + "workTitle=" + workTitle + ", completedHours=" + completedHours + ", planHours=" + planHours + '}';
    }

}
