package Domain;

public class Employee implements Poolable {

    private final int id;
    private final String name;
    private final String account;
    private final String role;

    public Employee(int id, String name, String account, String role) {
        this.id = id;
        this.name = name;
        this.account = account;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public String getAccount() {
        return account;
    }

    public int getId() {
        return id;
    }

    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", account=" + account + ", role=" + role + '}';
    }

}
