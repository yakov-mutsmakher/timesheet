package Domain;

import java.util.Objects;

public class UnitedReportElement {

    private String union_row_id;
    private String workType;
    private String workTitle;
    private String workForm;
    private String workInstance;
    private int unionHours;

    public String getUnion_row_id() {
        return union_row_id;
    }

    public void setUnion_row_id(String union_row_id) {
        this.union_row_id = union_row_id;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getWorkTitle() {
        return workTitle;
    }

    public void setWorkTitle(String workTitle) {
        this.workTitle = workTitle;
    }

    public String getWorkForm() {
        return workForm;
    }

    public void setWorkForm(String workForm) {
        this.workForm = workForm;
    }

    public String getWorkInstance() {
        return workInstance;
    }

    public void setWorkInstance(String workInstance) {
        this.workInstance = workInstance;
    }

    public int getUnionHours() {
        return unionHours;
    }

    public void setUnionHours(int unionHours) {
        this.unionHours = unionHours;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.union_row_id);
        hash = 17 * hash + Objects.hashCode(this.workType);
        hash = 17 * hash + Objects.hashCode(this.workTitle);
        hash = 17 * hash + Objects.hashCode(this.workForm);
        hash = 17 * hash + Objects.hashCode(this.workInstance);
        hash = 17 * hash + this.unionHours;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnitedReportElement other = (UnitedReportElement) obj;
        if (this.unionHours != other.unionHours) {
            return false;
        }
        if (!Objects.equals(this.union_row_id, other.union_row_id)) {
            return false;
        }
        if (!Objects.equals(this.workType, other.workType)) {
            return false;
        }
        if (!Objects.equals(this.workTitle, other.workTitle)) {
            return false;
        }
        if (!Objects.equals(this.workForm, other.workForm)) {
            return false;
        }
        if (!Objects.equals(this.workInstance, other.workInstance)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UnitedReportElement{" + "union_row_id=" + union_row_id + ", workType=" + workType + ", workTitle=" + workTitle + ", workForm=" + workForm + ", workInstance=" + workInstance + ", unionHours=" + unionHours + '}';
    }

}
