<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="favicon.png">
        <link rel="stylesheet" type="text/css" href="css\login.css">
        <title>TimeSheet - Login</title>
    </head>
    <body>
        <img src="favicon.png" alt="" id="logo"/>
        <h1>Login to Timesheet</h1>
        <div id="ramka">
            <form method="POST" action="j_security_check">
                <input type="text" name="j_username" placeholder="login">
                <input type="password" name="j_password" placeholder="password">  
                <input type="submit" value="LOGIN"> 
                <span>${errors}</span> 
            </form>
        </div>
    </body>
</html>
