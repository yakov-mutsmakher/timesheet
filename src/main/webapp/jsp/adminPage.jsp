<%@ page import="Domain.Employee" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Administration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="favicon.png">
        <link rel="stylesheet" type="text/css" href="css\userReport.css">
        <link rel="stylesheet" type="text/css" href="css\reports.css">
        <link rel="stylesheet" type="text/css" href="css\adminPage.css">
        <link rel="stylesheet" type="text/css" href="css\superViewerPage.css">
        <link rel="stylesheet" type="text/css" href="css\jquery.alertable.css">
        <link rel="stylesheet" type="text/css" href="css\jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="css\jquery.multiselectAdmin.css">
        <script src="js\js_libraries\jquery-3.1.1.min.js"></script>
        <script src="js\js_libraries\jquery-ui.min.js"></script>   
        <script src="js\js_libraries\spin.js"></script>  
        <script src="js\js_libraries\jquery.multiselect.js"></script>
        <script src="js\util.js"></script>
        <script src="js\adminPage.js"></script>
        <script src="js\languages.js"></script>
        <script>
            var mySuperViewerUnitedReport = {};
            var selectedEmployeesID = [];
            var selectedEmployeesIDAdd = [];
            var employeesList = [];

            var lang;
            if (sessionStorage.getItem('lang') == "ua") {
                lang = ua;
            } else {
                if (sessionStorage.getItem('lang') == "ru") {
                    lang = ru;
                } else {
                    lang = en;
                }
            }

            $(document).ready(function () {
                applyWindowResolution();
                prepareTableEmployee();
            });
        </script>
        <script src="js\js_libraries\jquery.alertable.js"></script> 
    </head>
    <body>
        <div class="divFixPart">
            <H2><%= ((Employee) session.getAttribute("employee")).getName()%></H2>

            <form id="ref_logout" class="headButtonLogout" action="logout" method="POST">       
                <input type="image" value="Logout" name="logout" id="logoutImageButton" src="images/logout.png"/>
            </form>

            <select class="headButtonLang" id="lang_select" onchange="updateLanguage()">
            </select>
            <!--
            <div id="langPseudoButton">
                <img src="images/lang.png" alt="" id="languageImageButton"/>
            </div>
            -->

            <form id="ref_main" class="headButton" action="main" method="POST">       
                <input type="submit" value="Main" name="main" />
            </form>

            <form id="ref_reports" class="headButton" action="reports" method="POST">       
                <input type="submit" value="My report" name="reports" />
            </form>

            <form id="ref_superViewerPage" class="headButton" action="superViewerPage" method="POST">
                <% if (((Employee) session.getAttribute("employee")).getRole().equals("admin")) { %>
                <input type="submit" value="SuperViewer" name="to_superViewerPage" />
                <% }%>             
            </form>   

            <form id="ref_adminPage" class="headButton" action="adminPage" method="POST">
                <% if (((Employee) session.getAttribute("employee")).getRole().equals("admin")) { %>
                <input type="submit" value="AdminPage" name="to_adminPage" />
                <% }%>             
            </form>

            <hr class="hrBetweenBlocksTop1">
            <button type="button" id="myReportLoad" onclick="changeRole()" disabled>Change role</button>
            <div id="post_message" style="display: none;"><span id="post_message_span"></span></div>

            <div id="divSearchNewEmployee">
                <p id="headerSearchNewEmployee">Add new employee: </p>
                <hr class="hrInsideSearchBlock">
                <p id="inputSearchNewEmployee"> Input login: </p> 
                <input type="text" id="inputSearch" placeholder="E0000000">
                <button type="button" id="buttonSearchNewEmployee" onclick="searchNewEmployee()">Search employee</button>
            </div>

        </div>

        <div id="id_divScrollPart" class="divScrollPart">
            <table id="employeeStatusChanging_table"></table>
        </div>

        <div class="divFixPartBottom">
            <hr class="hrBetweenBlocksBottom1">
            <p id="updatePlanDataFromFile">Choose file to import year's plan data: </p>
            <form action="updatingPlanData" method="post" enctype="multipart/form-data">
                <input id="file" type="file" name="file" style="display:none"/>
                <input id="fileSubmit" type="submit" style="display:none"/>
            </form> 
            <div id="fileChooser">
                <img src="images/fileLoad.jpg" id="fileImg"/>
                <div id="verticalLineFile"></div>
                <p id="fileName">${fileName}</p>
            </div>
            <div<span id="spanFile">${fileError}</span>
            </div>
        </div>

        <div id="searchDialogMessage"> </div>
        <div id="addNewUserBox"> 
            <table id="addNewUser_table"></table>
            <button type="button" id="buttonAddNewEmployee" onclick="addNewEmployee()" disabled>Add employee</button>
        </div>
        <div id="adminRoleList" ></div>

        <div id="hideLinePartAdminPage"></div>
    </body>
</html>
