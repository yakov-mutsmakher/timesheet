<%@ page import="Domain.Employee" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Another reports</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="favicon.png">
        <link rel="stylesheet" type="text/css" href="css\userReport.css">
        <link rel="stylesheet" type="text/css" href="css\reports.css">
        <link rel="stylesheet" type="text/css" href="css\superViewerPage.css">
        <link rel="stylesheet" type="text/css" href="css\jquery.alertable.css">
        <link rel="stylesheet" type="text/css" href="css\jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="css\jquery.multiselect.css">
        <script src="js\js_libraries\jquery-3.1.1.min.js"></script>
        <script src="js\js_libraries\jquery-ui.min.js"></script>   
        <script src="js\js_libraries\moment.js"></script>  
        <script src="js\js_libraries\datepicker-uk.js"></script> 
        <script src="js\js_libraries\datepicker-ru.js"></script> 
        <script src="js\js_libraries\spin.js"></script>  
        <script src="js\js_libraries\jquery.multiselect.js"></script>
        <script src="js\js_libraries\jquery.table2excel.js"></script>
        <script src="js\util.js"></script>
        <script src="js\superViewerReport.js"></script>
        <script src="js\myReport.js"></script>
        <script src="js\languages.js"></script>
        <script>
            var myReport; //holds myReport
            var mySuperViewerUnitedReport = {};
            var noDatesError = 0; //flag incorrect input dates: 0 - ok, 1 - err_ordering, 2 - err_format
            var showOnlyActiveUsers = "true";
            var selectedEmployeesID = [];
            var employeesList = [];

            var lang;
            if (sessionStorage.getItem('lang') == "ua") {
                lang = ua;
            } else {
                if (sessionStorage.getItem('lang') == "ru") {
                    lang = ru;
                } else {
                    lang = en;
                }
            }

            if (sessionStorage.getItem('showOnlyActiveUsers') !== null) {              
                showOnlyActiveUsers = sessionStorage.getItem('showOnlyActiveUsers');
            }

            $(document).ready(function () {
                if (showOnlyActiveUsers === "true") {
                    $("#checkboxEmpl").attr("checked", true);
                } else {
                    $("#checkboxEmpl").attr("checked", false);
                }
                applyWindowResolution();
                loadSelectedLanguageSuperViewer();
                prepareAddingUserReportRowU();
                prepareSelectEmployee();
                prepareSelectYear();
            });
        </script>
        <script src="js\js_libraries\jquery.alertable.js"></script>
    </head>
    <body>
        <div class="divFixPart">
            <H2><%= ((Employee) session.getAttribute("employee")).getName()%></H2>

            <form id="ref_logout" class="headButtonLogout" action="logout" method="POST">       
                <input type="image" value="Logout" name="logout" id="logoutImageButton" src="images/logout.png"/>
            </form>

            <select class="headButtonLang" id="lang_select" onchange="updateLanguage()">
            </select>
            <!--
            <div id="langPseudoButton">
                <img src="images/lang.png" alt="" id="languageImageButton"/>
            </div>
            -->

            <form id="ref_main" class="headButton" action="main" method="POST">       
                <input type="submit" value="Main" name="main" />
            </form>

            <form id="ref_reports" class="headButton" action="reports" method="POST">       
                <input type="submit" value="My report" name="reports" />
            </form>

            <form id="ref_superViewerPage" class="headButton" action="superViewerPage" method="POST">
                <% if (((Employee) session.getAttribute("employee")).getRole().equals("admin")) { %>
                <input type="submit" value="SuperViewer" name="to_superViewerPage" />
                <% }%>             
            </form>

            <form id="ref_adminPage" class="headButton" action="adminPage" method="POST">
                <% if (((Employee) session.getAttribute("employee")).getRole().equals("admin")) { %>
                <input type="submit" value="AdminPage" name="to_adminPage" />
                <% }%>             
            </form>

            <hr class="hrBetweenBlocksTop1">
            <button type="button" id="myReportLoad" onclick="superViewerReportLoad()" disabled>Load data</button>
            <div id="post_message" style="display: none;"><span id="post_message_span"></span></div>

            <div id="divLoadToExcelbox" >
                <p id="header_select_work_group_by">Select group by: </p>
                <select id="select_work_group_by">
                </select>
                <div id="verticalLine"></div>
                <button type="button" id="saveDetailedReportToExcel" onclick="saveDetailedReportToExcel()">Load detailed report to excel</button>              
            </div>

            <div class = "filter">
                <p id="dateFrom" class="filterDate"> Date from: </p> <input type="text" class="filterDateSuperViewer" id="filterDateFrom">
                <p id="dateTo" class="filterDate"> Date to: </p> <input type="text" class="filterDateSuperViewer" id="filterDateTo">
            </div>
            <div id="divCheckboxEmpl">
                <p id="headerCheckboxEmpl">Show only active employees: </p>
                <input id = "checkboxEmpl" type="checkbox"/>
            </div>
        </div>

        <div id="id_divScrollPart" class="divScrollPart">
            <table id="super_viewer_employees_hours_table"></table>
            <div class="lineToSeparateTables2"></div>
            <table id="planCompare_table"></table>
            <div class="lineToSeparateTables"></div>
            <table id="names_header_for_table_union"></table>
            <table id="super_viewer_employees_table_union"></table>
            <!--       <button onclick="saveToExcel()">Export</button> -->
        </div>

        <div class="divFixPartBottom">
            <hr class="hrBetweenBlocksBottom1">
            <div id="unitedPart">
                <select name="work_type_select_U" id="work_type_select_U" required onchange="getWorkTitlesU()">
                </select>
                <img id="work_type_select_arrow" src="images/Arrow.png">

                <select name="work_title_select_U" id="work_title_select_U" required onchange="getWorkFormsU()">
                </select>
                <img id="work_title_select_arrow" src="images/Arrow.png">

                <select name="work_form_select_U" id="work_form_select_U" required onchange="getWorkInstanceDetailsU()">
                </select>
                <img id="work_form_select_arrow" src="images/Arrow.png">

                <select name="work_instance_select_U" id="work_instance_select_U" required>
                </select>

                <button id="add_user_report_row_button_U" onclick="addSuperViewerReportRowU()">Add new row</button>
            </div>
            <div id="divPlanCompare" >
                <p id="header_PlanCompare">Compare with plan data</p>
                <select id="select_PlanCompare">
                </select>
                <button type="button" id="button_PlanCompare" onclick="planCompareLoad()">Compare</button>              
            </div>
        </div>
        <select id="select_employee" multiple="multiple" onchange="getSelectedEmployees()">
        </select>

        <div id="hideLinePartSuperViewerPage"></div>
    </body>
</html>
