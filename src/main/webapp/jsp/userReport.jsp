<%@page import="yyy.util.WebLogger"%>
<%@ page import="Domain.Employee" %>
<%@ page import="Service.EmployeesExchangeService" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Main</title>
        <link rel="shortcut icon" href="favicon.png">
        <link rel="stylesheet" type="text/css" href="css\userReport.css">
        <link rel="stylesheet" type="text/css" href="css\jquery.alertable.css">
        <script src="js\js_libraries\jquery-3.1.1.min.js"></script>
        <script src="js\js_libraries\spin.js"></script>  
        <script src="js\util.js"></script>
        <script src="js\userReportProcessor.js"></script>
        <script src="js\languages.js"></script>
        <script>
            var userReport; //holds current table data
            var userReportFromPreviousPeriod = {}; //holds previous table data
            var sumColumnListObjectFromPreviousPeriod = []; //holds previous sum column data to check if 0 and remove
            var tempList;
            var reportModified = false; //flag any unsaved modifications to report
            var showRowsFromPreviousPeriod = false;

            var lang;
            if (sessionStorage.getItem('lang') == "ua") {
                    lang = ua;
            } else {
                    if (sessionStorage.getItem('lang') == "ru") {
                            lang = ru;
                    } else {
                            lang = en;
                    }
            }

            if (sessionStorage.getItem('checkboxState') !== null) {
                    showRowsFromPreviousPeriod = sessionStorage.getItem('checkboxState');
            }

            $(document).ready(function () {
                    if (showRowsFromPreviousPeriod === "true") {
                            $("#checkbox").attr("checked", true);
                    } else {
                            $("#checkbox").attr("checked", false);
                    }
                    applyWindowResolution();
                    loadSelectedLanguage();
                    setTopButtonActionsBeforeLeaveUnsavedTable();
                    populateDefaultReport();
            });

            $(window).bind('beforeunload', function (e) {
                    if (reportModified) {
                        var message = "You didn't save the report for current period";
                        e.returnValue = message;
                        return message; //in Chrome this will display the default dialog
                    }
                });
        </script>
        <script src="js\js_libraries\jquery.alertable.js"></script>
    </head>

    <body>
        <% EmployeesExchangeService employeesExchangeService
                    = (EmployeesExchangeService) application.getAttribute("employeesExchangeService");

            if (session.getAttribute("employee") == null) {
                String emplAccount = request.getRemoteUser();
                session.setAttribute("employee", employeesExchangeService.getEmployeeByCode(emplAccount));
            }
            Employee employee = (Employee) session.getAttribute("employee");
        %>
        <div class="divFixPart">
            <H2><%= employee.getName()%></H2>

            <form id="ref_logout" class="headButtonLogout" action="logout" method="POST">       
                <input type="image" value="Logout" name="logout" id="logoutImageButton" src="images/logout.png"/>
            </form>

            <select class="headButtonLang" id="lang_select" onchange="updateLanguage()">
            </select>
            <!--
            <div id="langPseudoButton">
                <img src="images/lang.png" alt="" id="languageImageButton"/>
            </div>
            -->

            <form id="ref_main" class="headButton" action="main" method="POST">       
                <input type="submit" value="Main" name="main" />
            </form>

            <form id="ref_reports" class="headButton" action="reports" method="POST">       
                <input type="submit" value="My report" name="reports" />
            </form>

            <form id="ref_superViewerPage" class="headButton" action="superViewerPage" method="POST">
                <% if (employee.getRole().equals("admin")) { %>
                <input type="submit" value="SuperViewer" name="to_superViewerPage" />
                <% }%>             
            </form>

            <form id="ref_adminPage" class="headButton" action="adminPage" method="POST">
                <% if (employee.getRole().equals("admin")) { %>
                <input type="submit" value="AdminPage" name="to_adminPage" />
                <% }%>             
            </form>

            <hr class="hrBetweenBlocksTop1">
            <div class = "dates_header_block">

                <button type="button" id="post_user_report" onclick="postUserReport()" disabled>Save on server</button>
                <div id="post_user_report_message" style="display: none;"><span id="post_user_report_message_span"></span></div>
                <div class = "dates_header">
                    <button type="button" id="go_backward" class="date_shift_button" onclick="populateReportFromDate('go_backward')"> < </button>
                    <span id="user_report_timeframe"></span>
                    <button type="button" id="go_forward" class="date_shift_button" onclick="populateReportFromDate('go_forward')"> > </button>
                </div>
                <table id="user_report_table_dates"></table>
                <div id="divCheckbox">
                    <p id="headerCheckbox">Show rows from previous period: </p>
                    <input id = "checkbox" type="checkbox"/>
                </div>
            </div>
        </div>

        <div id="id_divScrollPart" class="divScrollPart">
            <table id="user_report_table"></table>
            <table id="sum_column_table"></table>
            <table id="sum_row_table"></table>
        </div>

        <div class="divFixPartBottom">
            <hr class="hrBetweenBlocksBottom1">

            <select name="work_type_select" id="work_type_select" required onchange="getWorkTitles()">
            </select>
            <img id="work_type_select_arrow" src="images/Arrow.png">

            <select name="work_title_select" id="work_title_select" required onchange="getWorkForms()">
            </select>
            <img id="work_title_select_arrow" src="images/Arrow.png">

            <select name="work_form_select" id="work_form_select" required onchange="getWorkInstanceDetails()">
            </select>
            <img id="work_form_select_arrow" src="images/Arrow.png">

            <input list="work_instance_datalist" id="work_instance_details_input" onsubmit="addUserReportRow()">
            <datalist id="work_instance_datalist">
            </datalist>            
            <button id="add_user_report_row_button" onclick="addUserReportRow()">Add new row</button>

        </div>
        <div id="hideLinePartMain"></div>
    </body>
</html>