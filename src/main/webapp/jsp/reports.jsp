<%@ page import="Domain.Employee" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <title>My report</title>
        <link rel="shortcut icon" href="favicon.png">
        <link rel="stylesheet" type="text/css" href="css\userReport.css">
        <link rel="stylesheet" type="text/css" href="css\reports.css">
        <link rel="stylesheet" type="text/css" href="css\jquery.alertable.css">
        <link rel="stylesheet" type="text/css" href="css\jquery-ui.css">
        <script src="js\js_libraries\jquery-3.1.1.min.js"></script>
        <script src="js\js_libraries\jquery-ui.min.js"></script>   
        <script src="js\js_libraries\moment.js"></script>  
        <script src="js\js_libraries\datepicker-uk.js"></script> 
        <script src="js\js_libraries\datepicker-ru.js"></script> 
        <script src="js\js_libraries\spin.js"></script>  
        <script src="js\util.js"></script>
        <script src="js\myReport.js"></script>
        <script src="js\languages.js"></script>
        <script>
            var myReport; //holds myReport
            var myUnitedReport = {}; //holds myUnionReport
            var noDatesError = 0; //flag incorrect input dates: 0 - ok, 1 - err_ordering, 2 - err_format

            var lang;
            if (sessionStorage.getItem('lang') == "ua") {
                lang = ua;
            } else {
                if (sessionStorage.getItem('lang') == "ru") {
                    lang = ru;
                } else {
                    lang = en;
                }
            }

            $(document).ready(function () {
                applyWindowResolution();
                loadButtonBlock();
                loadSelectedLanguage();
                myReportLoad();
                prepareAddingUserReportRowU();
            });
        </script>
        <script src="js\js_libraries\jquery.alertable.js"></script>
    </head>
    <body>
        <div class="divFixPart">
            <H2><%= ((Employee) session.getAttribute("employee")).getName()%></H2>

            <form id="ref_logout" class="headButtonLogout" action="logout" method="POST">       
                <input type="image" value="Logout" name="logout" id="logoutImageButton" src="images/logout.png"/>
            </form>

            <select class="headButtonLang" id="lang_select" onchange="updateLanguage()">
            </select>
            <!--
            <div id="langPseudoButton">
                <img src="images/lang.png" alt="" id="languageImageButton"/>
            </div>
            -->

            <form id="ref_main" class="headButton" action="main" method="POST">       
                <input type="submit" value="Main" name="main" />
            </form>

            <form id="ref_reports" class="headButton" action="reports" method="POST">       
                <input type="submit" value="My report" name="reports" />
            </form>

            <form id="ref_superViewerPage" class="headButton" action="superViewerPage" method="POST">
                <% if (((Employee) session.getAttribute("employee")).getRole().equals("admin")) { %>
                <input type="submit" value="SuperViewer" name="to_superViewerPage" />
                <% }%>             
            </form>

            <form id="ref_adminPage" class="headButton" action="adminPage" method="POST">
                <% if (((Employee) session.getAttribute("employee")).getRole().equals("admin")) { %>
                <input type="submit" value="AdminPage" name="to_adminPage" />
                <% }%>             
            </form>

            <hr class="hrBetweenBlocksTop1">
            <button type="button" id="myReportLoad" onclick="myReportLoad()" disabled>Load data</button>
            <div id="post_message" style="display: none;"><span id="post_message_span"></span></div>
            <div class = "filter">
                <p id="dateFrom" class="filterDate"> Date from: </p> <input type="text" class="filterDate" id="filterDateFrom">
                <p id="dateTo" class="filterDate"> Date to: </p> <input type="text" class="filterDate" id="filterDateTo">
            </div>
        </div>

        <div id="id_divScrollPart" class="divScrollPart">
            <table id="user_summary_report_table"></table>
            <table id="sum_row_table_summary"></table>
            <div class="lineToSeparateTables"></div>
            <table id="user_summary_report_table_union"></table>
        </div>

        <div class="divFixPartBottom">
            <hr class="hrBetweenBlocksBottom1">
            <select name="work_type_select_U" id="work_type_select_U" required onchange="getWorkTitlesU()">
            </select>
            <img id="work_type_select_arrow" src="images/Arrow.png">

            <select name="work_title_select_U" id="work_title_select_U" required onchange="getWorkFormsU()">
            </select>
            <img id="work_title_select_arrow" src="images/Arrow.png">

            <select name="work_form_select_U" id="work_form_select_U" required onchange="getWorkInstanceDetailsU()">
            </select>
            <img id="work_form_select_arrow" src="images/Arrow.png">

            <select name="work_instance_select_U" id="work_instance_select_U" required>
            </select>

            <button id="add_user_report_row_button_U" onclick="addUserReportRowU()">Add new row</button>
        </div>
        <div id="hideLinePartMyReport"></div>
    </body>
</html>
