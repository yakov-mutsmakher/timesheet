var numberOfColumns; //init number of columns in function populateUserReportTable() 
var sumColumnListObject; //array to keep objects sum hours for current period 
var callFromCheckbox = false;
var callFromPopulateReportFromDate = false;
var direction = "";
var messageFromPreviousPeriod = "";
var langLocale = "en-us";

function loadSelectedLanguage() {
    if (lang == ua) {
        langLocale = "uk-ua";
    } else {
        if (lang == ru) {
            langLocale = "ru-RU";
        } else {
            langLocale = "en-us";
        }
    }
    $("#lang_select").append("<option " + ((lang.lang_ru == 'ru') ? "selected" : "") + " value='en'>" + lang.lang_en + "</option>\n\
                              <option " + ((lang.lang_ru == 'рус.') ? "selected" : "") + " value='ru'>" + lang.lang_ru + "</option>\n\
                              <option " + ((lang.lang_ru == 'рос.') ? "selected" : "") + " value='ua'>" + lang.lang_ua + "</option>");
    $(':input[value="Logout"]').val(lang.logout);
    $(':input[value="Main"]').val(lang.main);
    $(':input[value="My report"]').val(lang.my_report);
    $(':input[value="SuperViewer"]').val(lang.super_viewer);
    $(':input[value="AdminPage"]').val(lang.admin_page);
    $("#post_user_report").text(lang.save_on_server);
    $("#headerCheckbox").text(lang.show_rows_from_prev_period);
    $("#add_user_report_row_button").text(lang.add_new_row);
}

//option to save report when enter button clicked in input field
$(function () {
    $(window).keypress(function (e) {
        var key = e.which;
        var activeObj = document.activeElement;
        if (key == '13' && activeObj.tagName === "INPUT" && $("#post_user_report").attr('disabled') != "disabled") {
            postUserReport();
        }
    });
});

$(function () {
    $("#checkbox").change(function () {
        showRowsFromPreviousPeriod = $("#checkbox")[0].checked;
        sessionStorage.setItem('checkboxState', showRowsFromPreviousPeriod);
        if (showRowsFromPreviousPeriod) {
            addRowsFromPreviousPeriod();
        } else {
            callFromCheckbox = true;
            populateReportCheckboxFalse();
        }
    });
});

// block current page button in menu 
$(function () {
    blockCurrentPageButton("ref_main");
});

//when the page is first loaded
function setTopButtonActionsBeforeLeaveUnsavedTable() {
    $("#ref_reports").submit(function (event) {
        if (reportModified) {
            actionBeforeLeaveUnsavedTable();
            event.preventDefault();
        }
    });
    $("#ref_superViewerPage").submit(function (event) {
        if (reportModified) {
            actionBeforeLeaveUnsavedTable();
            event.preventDefault();
        }
    });
    $("#ref_logout").submit(function (event) {
        if (reportModified) {
            actionBeforeLeaveUnsavedTable();
            event.preventDefault();
        }
    });
    $("#ref_adminPage").submit(function (event) {
        if (reportModified) {
            actionBeforeLeaveUnsavedTable();
            event.preventDefault();
        }
    });
}

//when the page is first loaded
function populateDefaultReport() {
    // calling spinner
    var target = document.getElementById('id_divScrollPart');
    var spinner = new Spinner(opts).spin(target);

    $.getJSON("user-report", function (json) {
        getUserReport(json);
        populateReportTimeFrame();
        populateUserReportTable();
        prepareAddingUserReportRow();
    }).done(function () {
        prepareDataInput();
        spinner.stop();
    }).fail(function () {
        spinner.stop();
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });
}

//helping function for parse UserReport
function getUserReport(json) {
    var JSONstr = JSON.stringify(json);
    //save to global variable userReport declared at <head> of userReport.jsp 
    userReport = JSON.parse(JSONstr, function (key, value) {
        if (key.search("date") > -1 && typeof value === 'number') {
            return new Date(value);
        } else {
            return value;
        }
    });
}

function populateReportFromDate(thisDirection) {
    direction = thisDirection;
    if (reportModified) {
        callFromPopulateReportFromDate = true;
        actionBeforeLeaveUnsavedTable();
    } else {
        // calling spinner
        var target = document.getElementById('id_divScrollPart');
        var spinner = new Spinner(opts).spin(target);

        $("#date_shift_button").attr('disabled', 'disabled');
        var dateToStartFrom = userReport.date_from;
        if (direction == "go_backward") {
            dateToStartFrom.setDate(dateToStartFrom.getDate() - 14);
        } else if (direction == "go_forward") {
            dateToStartFrom.setDate(dateToStartFrom.getDate() + 14);
        }
        userReportFromPreviousPeriod = userReport;
        sumColumnListObjectFromPreviousPeriod = sumColumnListObject;
        $.getJSON("user-report",
                {dateToStartFrom: dateToStartFrom.toISOString()},
                function (json) {
                    getUserReport(json);
                    populateReportTimeFrame();
                    populateUserReportTable();
                    $("#date_shift_button").removeAttr('disabled');

                    if (showRowsFromPreviousPeriod) {
                        addRowsFromPreviousPeriod();
                    }
                    if (messageFromPreviousPeriod != "") {
                        $("#post_user_report_message").show();
                        $("#post_user_report_message_span").text(messageFromPreviousPeriod);
                        messageFromPreviousPeriod = "";
                    }
                    spinner.stop();
                }).fail(function () {
            spinner.stop();
            $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
        });
    }
}

function populateReportCheckboxFalse() {
    if (reportModified) {
        actionBeforeLeaveUnsavedTable();
    } else {
// calling spinner
        var target = document.getElementById('id_divScrollPart');
        var spinner = new Spinner(opts).spin(target);

        $("#user_report_table").empty();
        var dateToStartFrom = userReport.date_from;
        $.getJSON("user-report",
                {dateToStartFrom: dateToStartFrom.toISOString()},
                function (json) {
                    getUserReport(json);
                    populateUserReportTable();
                    $("#post_user_report").attr('disabled', 'disabled');
                    reportModified = false;
                    spinner.stop();
                }).fail(function () {
            spinner.stop();
            $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
        });
    }
    callFromCheckbox = false;
}

function addRowsFromPreviousPeriod() {
    var source = "userReportFromPreviousPeriod"; //detector to show message if this row already exist
    $.each(userReportFromPreviousPeriod, function (wi, userReportRow) {
        if (wi !== "date_from" && wi !== "date_to") {
            workInstance = userReportRow.work_instance;
            for (var i = 0; i < sumColumnListObjectFromPreviousPeriod.length; i++) {
                if (workInstance.id === sumColumnListObjectFromPreviousPeriod[i].wi && sumColumnListObjectFromPreviousPeriod[i].sumHours > 0) {
                    addUserReportRowIfWI(workInstance, source);
                    break;
                }
            }
        }
    });
    fillingSumColumnTable();
}

function actionBeforeLeaveUnsavedTable() {
    $.alertable.confirm(lang.SAVE_CURRENT_PERIOD_PROMPT).then(function () {
        postUserReport();
        if (callFromCheckbox) {
            $("#checkbox")[0].checked = true;
            showRowsFromPreviousPeriod = $("#checkbox")[0].checked;
            callFromCheckbox = false;
        }
    }, function () {
        // calling spinner
        var target = document.getElementById('id_divScrollPart');
        var spinner = new Spinner(opts).spin(target);

        $("#user_report_table").empty();
        var dateToStartFrom = userReport.date_from;
        $.getJSON("user-report",
                {dateToStartFrom: dateToStartFrom.toISOString()},
                function (json) {
                    getUserReport(json);
                    populateUserReportTable();
                    $("#post_user_report").attr('disabled', 'disabled');
                    $("#post_user_report_message").show();
                    $("#post_user_report_message_span").text(lang.changes_cleared);
                    reportModified = false;
                    spinner.stop();
                    //go to next period after confirm box (stay at current before)
                    if (callFromPopulateReportFromDate) {
                        callFromPopulateReportFromDate = false;
                        messageFromPreviousPeriod = lang.changes_prev_period_cleared;
                        populateReportFromDate(direction);
                    }
                }).fail(function () {
            spinner.stop();
            $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
        });
    });
}

function populateReportTimeFrame() {
    var options = {
        /*weekday: 'long', */
        year: 'numeric',
        month: 'long',
        day: 'numeric'};
    if (lang == ua) {
        $("#user_report_timeframe").text(
                userReport.date_from.toLocaleDateString(langLocale, options) + " - " +
                userReport.date_to.toLocaleDateString(langLocale, options));
    } else {
        $("#user_report_timeframe").text(
                userReport.date_from.toLocaleDateString(langLocale, options) + "    -    " +
                userReport.date_to.toLocaleDateString(langLocale, options));
    }

}

function populateUserReportTable() {
    var userReportRecordsLine;

    //clean the table element
    $("#user_report_table").text("");
    $("#user_report_table_dates").text("");
    //clean information mesage
    $("#post_user_report_message").hide();

    numberOfColumns = 0;
    //fill the dates row with the dates from the userReport object
    for (columnDate = new Date(userReport.date_from);
            columnDate <= userReport.date_to;
            columnDate.setDate(columnDate.getDate() + 1)) {
        var optionsLine1 = {weekday: 'short'};
        var optionsLine2 = {day: 'numeric'};
        var optionsLine3 = {month: 'short'};

        var isSunday = (columnDate.getDay() == 0);
        var isSaturday = (columnDate.getDay() == 6);

        numberOfColumns++; //init number of columns

        $("#user_report_table_dates").append("<td id ='" +
                "user_report_table_dates_" + columnDate.getTime() + "'" +
                (isSunday ? " class = 'sunday'" : "") +
                (isSaturday ? " class = 'saturday'" : "") + ">" +
                "<p class='report_record_weekday'>" +
                columnDate.toLocaleDateString(langLocale, optionsLine1) +
                "</p><p class='report_record_monthday'>" +
                columnDate.toLocaleDateString(langLocale, optionsLine2) +
                "</p><p class='report_record_month'>" +
                columnDate.toLocaleDateString(langLocale, optionsLine3) +
                "</p></td>");
    }

    //sorting by header
    var myReportSortForShow = sortUserReportForShow();

    //create a new row if there is another one work instance in the userReport
    $.each(myReportSortForShow, function (i, userReportRow) {
        userReportRecordsLine = new Array(); //array to populate from

        //indicate the work instance details
        $("#user_report_table").append("<tr id = 'wi" + userReportRow.work_instance.id + "'>" +
                "<td class = 'user_report_row_head' title='" +
                userReportRow.work_instance.work_type_title + "\n" +
                userReportRow.work_instance.work_title_title + "\n" +
                userReportRow.work_instance.work_form_title + "\n" +
                userReportRow.work_instance.details +
                "'>" +
                "<p class='work_type_title'>" +
                userReportRow.work_instance.work_type_title +
                "</p><p class='work_title_title'>" +
                userReportRow.work_instance.work_title_title +
                "</p><p class='work_form_title'>" +
                userReportRow.work_instance.work_form_title +
                "</p><p class='work_instance_details'>" +
                userReportRow.work_instance.details +
                "</p></td></tr>");

        //fill the userReportRecordsLine with the report records for currend work instance
        $.each(userReportRow, function (rr_key, reportRecord) {
            if (rr_key != "work_instance") {
                userReportRecordsLine.push(reportRecord);
            }
        });

        userReportRecordsLine.sort(function (a, b) {
            return new Date(a.date_reported) - new Date(b.date_reported);
        });

        //add report records cells to current line
        var sundayDayDetect = 0;
        $.each(userReportRecordsLine, function (indexArray, reportRecord) {
            sundayDayDetect++;
            $("#wi" + userReportRow.work_instance.id).append(
                    "<td id ='wi" + userReportRow.work_instance.id + "_rr" + reportRecord.date_reported.getTime() + "'" +
                    ((sundayDayDetect == 7) ? " class = 'sunday user_report_row_fields'" : " class = 'user_report_row_fields'") +
                    ">" +
                    "<input " +
                    "type = number class = 'report_record_hours_num_input' " +
                    "id = 'input_wi" + userReportRow.work_instance.id + "_rr" + reportRecord.date_reported.getTime() + "' " +
                    "value='" + (reportRecord.hours_num == 0 ? "" : reportRecord.hours_num) + "' " +
                    "min='0' max='24'" +
                    ">" +
                    "</td>");
        });
    });
    fillingSumColumnTable();
}

function sortUserReportForShow() {
    var myReportSortForShow = [];
    $.each(userReport, function (wi_key, userReportRow) {
        if (wi_key !== "date_from" && wi_key !== "date_to") {
            myReportSortForShow.push(userReportRow);
        }
    });
    myReportSortForShow.sort(function (a, b) {
        if (a.work_instance.work_type_title > b.work_instance.work_type_title)
            return 1;
        if (a.work_instance.work_type_title < b.work_instance.work_type_title)
            return -1;
        if (a.work_instance.work_title_title > b.work_instance.work_title_title)
            return 1;
        if (a.work_instance.work_title_title < b.work_instance.work_title_title)
            return -1;
        if (a.work_instance.work_form_title > b.work_instance.work_form_title)
            return 1;
        if (a.work_instance.work_form_title < b.work_instance.work_form_title)
            return -1;
        if (a.work_instance.details > b.work_instance.details)
            return 1;
        if (a.work_instance.details < b.work_instance.details)
            return -1;
        return 0;
    });
    return myReportSortForShow;
}

// statistic function
function fillingSumColumnTable() {
    //clean the table element
    $("#sum_column_table").text("");
    $("#sum_row_table").text("");

    var numberOfElements = 0;
    var numberOFRows = 0;
    $(".report_record_hours_num_input").each(function (index, element) {
        numberOfElements = index + 1;
    });
    numberOFRows = numberOfElements / numberOfColumns;

    sumColumnListObject = [];
    $.each(userReport, function (wi_key, userReportRow) {
        if (wi_key !== "date_from" && wi_key !== "date_to") {
            sumColumnObject = {wi: userReportRow.work_instance.id, sumHours: 0};
            sumColumnListObject.push(sumColumnObject);
        }
    });

    if (numberOFRows > 0) {
        var sumByColumnsArray = new Array();
        var sumByRowsArray = new Array();
        var indexColumns = 0;
        var indexRows = 0;

        // init arrays
        for (var i = 0; i < numberOFRows; i++) {
            sumByColumnsArray[i] = 0;
        }
        for (var i = 0; i < numberOfColumns; i++) {
            sumByRowsArray[i] = 0;
        }

        //filling arrays
        $(".report_record_hours_num_input").each(function (index, element) {
            indexColumns = parseInt(index / numberOfColumns);
            indexRows = index % numberOfColumns;
            var tempID = element.id.substring(element.id.indexOf("_wi") + 3, element.id.lastIndexOf("_"));
            if (!isNaN(element.value) && element.value > 0) {
                sumByColumnsArray[indexColumns] += parseInt(element.value);
                sumByRowsArray[indexRows] += parseInt(element.value);

                for (var i = 0; i < sumColumnListObject.length; i++) {
                    if (tempID == sumColumnListObject[i].wi) {
                        sumColumnListObject[i].sumHours += parseInt(element.value);
                    }
                }
            }
        });

        // filling tables
        for (var i = 0; i < sumByColumnsArray.length; i++) {
            $("#sum_column_table").append("<tr> <td> " + sumByColumnsArray[i] + "</td></tr>");
        }
        $("#sum_row_table").append("<tr id='sum_row_table_row'></tr>");
        for (var i = 0; i < sumByRowsArray.length; i++) {
            var colorIndicatorHours = "";
            if (i == 5 || i == 6 || i == 12 || i == 13) {
                if (sumByRowsArray[i] == 0) {
                    colorIndicatorHours = " class = 'hoursGood'";
                } else {
                    colorIndicatorHours = " class = 'overWork'";
                }
            } else {
                if (sumByRowsArray[i] < 8) {
                    colorIndicatorHours = " class = 'less8hours'";
                } else {
                    if (sumByRowsArray[i] == 8) {
                        colorIndicatorHours = " class = 'hoursGood'";
                    } else {
                        colorIndicatorHours = " class = 'overWork'";
                    }
                }
            }
            $("#sum_row_table_row").append("<td" +
                    ((i == 6) ? " class = 'sunday'" : "") +
                    "><p" + colorIndicatorHours + ">" + sumByRowsArray[i] + "</p></td>");
        }
    }
}

function prepareDataInput() {
    $("#user_report_table").on("change", ".report_record_hours_num_input", function () {
        var wi = (this.id).slice(6, -16);
        var rr = (this.id).slice(-15);
        var hoursNum = parseInt(this.value, 10);
        if (isNaN(hoursNum) || hoursNum <= 0) {
            userReport[wi][rr].hours_num = 0;
            this.value = "";
        } else {
            if (hoursNum > 24) {
                hoursNum = 24;
                this.value = "24";
            }
            userReport[wi][rr].hours_num = hoursNum;
        }
        $("#post_user_report_message").hide();
        $("#post_user_report").removeAttr('disabled');
        reportModified = true;
        fillingSumColumnTable();
    });
}

function postUserReport() {
    // calling spinner
    var target = document.getElementById('id_divScrollPart');
    var spinner = new Spinner(opts).spin(target);

    $.ajax({
        type: "POST",
        url: "user-report",
        data: {
            user_report: JSON.stringify(userReport)
        },
        statusCode: {
            200: function (json) {
                if (json.quantityOfChangedElements === 0) {
                    $("#post_user_report").attr('disabled', 'disabled');
                    $("#post_user_report_message").show();
                    $("#post_user_report_message_span").text(lang.no_fields_modify);
                    reportModified = false;
                } else {
                    if (json.quantityOfChangedElements > 0) {
                        $("#post_user_report").attr('disabled', 'disabled');
                        $("#post_user_report_message").show();
                        if (json.quantityOfChangedElements == 1) {
                            $("#post_user_report_message_span").text(lang.successfully_saved1 + json.quantityOfChangedElements + lang.successfully_saved2);
                        }
                        if (json.quantityOfChangedElements > 1 && lang == en) {
                            $("#post_user_report_message_span").text(lang.successfully_saved1 + json.quantityOfChangedElements + lang.successfully_saved3);
                        }
                        if ((lang == ua || lang == ru) && json.quantityOfChangedElements >= 2 && json.quantityOfChangedElements <= 4) {
                            $("#post_user_report_message_span").text(lang.successfully_saved1 + json.quantityOfChangedElements + lang.successfully_saved3);
                        }
                        if ((lang == ua || lang == ru) && json.quantityOfChangedElements >= 5) {
                            $("#post_user_report_message_span").text(lang.successfully_saved1 + json.quantityOfChangedElements + lang.successfully_saved4);
                        }
                        reportModified = false;
                    } else {
                        spinner.stop();
                        $.alertable.alert(lang.DB_CONNECTION_ERROR);
                    }
                }
                spinner.stop();
                //go to next period after confirm box (stay at current before)
                if (callFromPopulateReportFromDate) {
                    callFromPopulateReportFromDate = false;
                    if (json.quantityOfChangedElements == 1) {
                        messageFromPreviousPeriod = lang.prev_period_successfully_saved1 + json.quantityOfChangedElements + lang.successfully_saved2;
                    }
                    if (json.quantityOfChangedElements > 1 && lang == en) {
                        messageFromPreviousPeriod = lang.prev_period_successfully_saved1 + json.quantityOfChangedElements + lang.successfully_saved3;
                    }
                    if ((lang == ua || lang == ru) && json.quantityOfChangedElements >= 2 && json.quantityOfChangedElements <= 4) {
                        messageFromPreviousPeriod = lang.prev_period_successfully_saved1 + json.quantityOfChangedElements + lang.successfully_saved3;
                    }
                    if ((lang == ua || lang == ru) && json.quantityOfChangedElements >= 5) {
                        messageFromPreviousPeriod = lang.prev_period_successfully_saved1 + json.quantityOfChangedElements + lang.successfully_saved4;
                    }
                    populateReportFromDate(direction);
                }
            },
            500: function () {
                spinner.stop();
                $.alertable.alert(lang.REPORT_POST_500);
            },
            403: function () {
                spinner.stop();
                $.alertable.alert(lang.REPORT_POST_403);
            }
        },
        dataType: 'json'
    });
    spinner.stop();
}

function prepareAddingUserReportRow() {
    $.getJSON("work-types", function (json) {
        tempList = json;
        $("#work_type_select").empty();
        $("#work_type_select_arrow").hide();
        $("#work_title_select").hide();
        $("#work_title_select_arrow").hide();
        $("#work_form_select").hide();
        $("#work_form_select_arrow").hide();
        $("#work_instance_details_input").hide();
        $("#add_user_report_row_button").hide();
        $("#work_type_select")
                .append("<option value='0' disabled selected>" + lang.add_row + "</option>");
        $.each(tempList, function (indexInArray, value) {
            $("#work_type_select")
                    .append("<option value=\"" + value.id + "\">" + value.title + "</option>");
        });
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });
}

function getWorkTitles() {
    var workTypeid = $("#work_type_select").val();

    $.getJSON("work-titles?work-type-id=" + workTypeid, function (json) {
        tempList = json;
        $("#work_type_select option[value='0']").remove();
        $("#work_type_select_arrow").show();
        $("#work_title_select").empty();
        $("#work_title_select").show();
        $("#work_title_select_arrow").hide();
        $("#work_form_select").hide();
        $("#work_form_select_arrow").hide();
        $("#work_instance_details_input").hide();
        $("#add_user_report_row_button").hide();
        $("#work_title_select")
                .append("<option value='0' disabled selected >" + lang.select_title + "</option>");
        $.each(tempList, function (indexInArray, value) {
            $("#work_title_select")
                    .append("<option value=\"" + value.id + "\" >" + value.title + "</option>");
        });
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });
}

function getWorkForms() {
    var workTypeid = $("#work_type_select").val();

    $.getJSON("work-forms?work-type-id=" + workTypeid, function (json) {
        tempList = json;
        $("#work_title_select option[value='0']").remove();
        $("#work_title_select_arrow").show();
        $("#work_form_select").empty();
        $("#work_form_select").show();
        $("#work_form_select_arrow").hide();
        $("#work_instance_details_input").hide();
        $("#add_user_report_row_button").hide();
        $("#work_form_select")
                .append("<option value='0' disabled selected>" + lang.select_work_form + "</option>");
        $.each(tempList, function (indexInArray, value) {
            $("#work_form_select")
                    .append("<option value=\"" + value.id + "\">" + value.title + "</option>");
        });
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });
}

function getWorkInstanceDetails() {
    $("#work_form_select option[value='0']").remove();
    var workTitleId = $("#work_title_select").val();
    var workFormId = $("#work_form_select").val();

    $.getJSON("work-instances?work-title-id=" + workTitleId + "&work-form-id=" + workFormId, function (json) {
        tempList = json;
        $("#work_form_select_arrow").show();
        $("#work_instance_datalist").empty();
        $("#work_instance_details_input").show();
        $("#add_user_report_row_button").show();
        $.each(tempList, function (indexInArray, value) {
            if (value.details != "") {
                $("#work_instance_datalist")
                        .prepend("<option value=\'" + value.details + "\' id=\'" + value.id + "\'>");
            }
        });
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });
}

function addUserReportRow() {
    var workInstanceDetails = $("#work_instance_details_input").val();

    var foundWIsWithoutDetails = $.grep(tempList, function (workInstance) {
        return workInstance.details == workInstanceDetails;
    });

    if (foundWIsWithoutDetails.length) {
        foundWIsWithoutDetails.sort(function (a, b) {
            return b.id - a.id;
        });
        addUserReportRowIfWI(foundWIsWithoutDetails[0]);
    } else {
        addUserReportRowWhereNOWi(workInstanceDetails);
    }
    getWorkInstanceDetails();
    fillingSumColumnTable();
}

function addUserReportRowIfWI(workInstance, source) {

    if (source === "userReportFromPreviousPeriod") {
        if ($("#wi" + workInstance.id).length) {
            return;
        }
    } else {
        if ($("#wi" + workInstance.id).length) {
            //alert(lang.ROW_ALREADY_EXISTS_ERROR);
            $.alertable.alert(lang.ROW_ALREADY_EXISTS_ERROR);
            return;
        }
    }

    $("#user_report_table").append("<tr id = 'wi" + workInstance.id + "'>" +
            "<td class = 'user_report_row_head' title='" +
            workInstance.work_type_title + "\n" +
            workInstance.work_title_title + "\n" +
            workInstance.work_form_title + "\n" +
            workInstance.details +
            "'>" +
            "<p class='work_type_title'>" +
            workInstance.work_type_title +
            "</p><p class='work_title_title'>" +
            workInstance.work_title_title +
            "</p><p class='work_form_title'>" +
            workInstance.work_form_title +
            "</p><p class='work_instance_details'>" +
            workInstance.details +
            "</p></td></tr>");
    var sundayDayDetect = 0;
    for (columnDate = new Date(userReport.date_from);
            columnDate <= userReport.date_to;
            columnDate.setDate(columnDate.getDate() + 1)) {
        sundayDayDetect++;
        $("#wi" + workInstance.id).append(
                "<td id ='wi" + workInstance.id + "_rr" + columnDate.getTime() + "' " +
                ((sundayDayDetect == 7) ? " class = 'sunday user_report_row_fields'" : " class = 'user_report_row_fields'") +
                ">" +
                "<input " +
                "type = number class = 'report_record_hours_num_input' " +
                "id = 'input_wi" + workInstance.id + "_rr" + columnDate.getTime() + "' " +
                "value='" + "" + "' " +
                "min='0' max='24'" +
                ">" +
                "</td>");
    }
    newRowToUserReporObj(workInstance);
}

function newRowToUserReporObj(workInstance) {
    userReport["wi" + workInstance.id] = {};
    userReport["wi" + workInstance.id]["work_instance"] = workInstance;
    for (columnDate = new Date(userReport.date_from);
            columnDate <= userReport.date_to;
            columnDate.setDate(columnDate.getDate() + 1)) {
        userReport["wi" + workInstance.id]["rr" + columnDate.getTime()] = {
            date_reported: columnDate.getTime(),
            hours_num: 0,
            work_instance_id: workInstance.id
        };
    }
}

function addUserReportRowWhereNOWi(workInstanceDetails) {
    $.ajax({
        type: "POST",
        url: "work-instances",
        data: {
            work_title_id: $("#work_title_select").val(),
            work_form_id: $("#work_form_select").val(),
            details: workInstanceDetails
        },
        success: function (workInstance) {
            addUserReportRowIfWI(workInstance);
        },
        dataType: "json"
    });
}

