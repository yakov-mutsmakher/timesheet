//spinner properties
var opts = {
    lines: 12               // The number of lines to draw
    , length: 15             // The length of each line
    , width: 3              // The line thickness
    , radius: 10            // The radius of the inner circle
    , scale: 1.5            // Scales overall size of the spinner
    , corners: 1            // Roundness (0..1)
    , color: '#517599'         // #rgb or #rrggbb
    , opacity: 1 / 4        // Opacity of the lines
    , rotate: 0             // Rotation offset
    , direction: 1          // 1: clockwise, -1: counterclockwise
    , speed: 1              // Rounds per second
    , trail: 100            // Afterglow percentage
    , fps: 20               // Frames per second when using setTimeout()
    , zIndex: 2e9           // Use a high z-index by default
    , className: 'spinner'  // CSS class to assign to the element
    , top: '50%'            // center vertically
    , left: '50%'           // center horizontally
    , shadow: true         // Whether to render a shadow
    , hwaccel: false        // Whether to use hardware acceleration (might be buggy)
    , position: 'absolute'  // Element positioning
};

//change resolution of scroll part when windows resize
$(function () {
    $(window).resize(function () {
        applyWindowResolution();
    });
});

function applyWindowResolution() {
    var windH = $(window).height();
    var windW = $(window).width();
    $(".divScrollPart").height(windH - 170 - 60);
    //$(".divScrollPart").width(windW);
    //console.log($(".divScrollPart").height() + "   " + $(".divScrollPart").width());
}

// functions for language switch
function updateLanguage() {
    var language = $("#lang_select").val();
    sessionStorage.setItem('lang', language);
    location.reload();
}
/* //removed because 'var e = document.createEvent("MouseEvents");' doesn't work more in Chrome version > 53
 
//emulation to click language select menu
$(function () {
    $('#langPseudoButton').click(function () {
        $("#lang_select option").css("visibility", "visible");
        open($('#lang_select'));
    });
});

function open(elem) {
    if (document.createEvent) {
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        elem[0].dispatchEvent(e);
    } else if (elem.fireEvent) {
        elem[0].fireEvent("onmousedown");
    }
}
*/

// block current page button in menu 
function blockCurrentPageButton(button) {
    $("#" + button + " input").css({"pointer-events": "none",
        "background": "-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #80b5ea), color-stop(1, #404040))",
        "box-shadow": "1px 3px 8px 2px rgba(0, 0, 0, 0.5)",
        "position": "relative",
        "top": "-1px"});
    $("#" + button).css({"height": "60px",
        "box-shadow": "1px 0 2px 0px rgba(70, 70, 70, 1), -1px 0 2px 0px rgba(70, 70, 70, 1)",
        "border-right": "1px solid #4b545c",
        "border-left": "1px solid #4b545c",
        "border-top": "1px solid #4b545c",
        "border-top-left-radius": "3px",
        "border-top-right-radius": "3px"});
}


