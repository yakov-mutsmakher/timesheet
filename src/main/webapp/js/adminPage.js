var newRole;
var newRoleAdd;
var login = "";
var newEmployeeName = "";
var oldRole;
var id; //id employee of changed role
var changingAccount = "";

$(function () {
    $("#inputSearch").focus(function () {
        $("#searchDialogMessage").hide();
    });
});

// block current page button in menu 
$(function () {
    blockCurrentPageButton("ref_adminPage");
});

$(function () {
    $("#lang_select").append("<option " + ((lang.lang_ru == 'ru') ? "selected" : "") + " value='en'>" + lang.lang_en + "</option>\n\
                              <option " + ((lang.lang_ru == 'рус.') ? "selected" : "") + " value='ru'>" + lang.lang_ru + "</option>\n\
                              <option " + ((lang.lang_ru == 'рос.') ? "selected" : "") + " value='ua'>" + lang.lang_ua + "</option>");
    $(':input[value="Logout"]').val(lang.logout);
    $(':input[value="Main"]').val(lang.main);
    $(':input[value="My report"]').val(lang.my_report);
    $(':input[value="SuperViewer"]').val(lang.super_viewer);
    $(':input[value="AdminPage"]').val(lang.admin_page);
    $("#myReportLoad").text(lang.change_role);
    $("#headerSearchNewEmployee").text(lang.add_new_employee);
    $("#inputSearchNewEmployee").text(lang.input_login);
    $("#buttonSearchNewEmployee").text(lang.search_employee);
    $("#updatePlanDataFromFile").text(lang.choose_file);
    $("#buttonAddNewEmployee").text(lang.add_employee);

    switch ($("#spanFile").text().trim()) {
        case "":
            $("#spanFile").text("");
            break;
        case "file_was_not_selected":
            $("#spanFile").text(lang.file_was_not_selected);
            break;
        case "incorrect_file_format":
            $("#spanFile").text(lang.incorrect_file_format);
            break;
        case "ukrainian_version_was_selected":
            $("#spanFile").text(lang.ukrainian_version_was_selected);
            break;
        case "incorrect_year":
            $("#spanFile").text(lang.incorrect_year);
            break;
        case "error_updating_DB":
            $("#spanFile").text(lang.error_updating_DB);
            break;
        default :
            arr = $("#spanFile").text().trim().split(',');
            $("#spanFile").text(lang.file_updated_res_1 + arr[0] + lang.file_updated_res_2 + arr[1] + lang.file_updated_res_3 + arr[2]);
    }
});

function prepareTableEmployee() {
    $("#employeeStatusChanging_table").empty();
    employeesList = [];
    var counter = 0;
    $.getJSON("employees?onlyActive=false", function (json) {
        $("#employeeStatusChanging_table").append("<tr class='tr_head'>\n\
                         <td class = 'counter'> № </td>\n\
                         <td> " + lang.employee_name + "</td>\n\
                         <td class = 'login'>" + lang.employee_login + "</td>\n\
                         <td class='selectColumn'>" + lang.role + "</td>\n\
                         <td class='adminList' id='load00' hidden></td>\n\
                         </tr>");
        $.each(json, function (indexInArray, value) {
            $("#employeeStatusChanging_table").append("<tr><td class = 'counter'>" + (++counter) + "</td>\n\
                                                           <td class = 'nameColumn'>" + value.name + "</td>\n\
                                                           <td class = 'login'>" + value.account + "</td>\n\
                                                           <td class='selectColumn'><select class='selectEl' id='" + value.id + "' " + (value.name == $("H2").text() ? "disabled" : "") + " onchange = 'onChangeRole(this)' ondblclick = 'onClickRole(this)'>\n\
                                                                       <option" + ((value.role === "admin") ? " selected disabled hidden" : " value='a" + value.id + "'") + ">" + lang.admin + "</option>\n\
                                                                       <option" + ((value.role === "user") ? " selected disabled hidden" : " value='b" + value.id + "'") + ">" + lang.user + "</option>\n\
                                                                       <option" + ((value.role === "not active") ? " selected disabled hidden" : " value='c" + value.id + "'") + ">" + lang.not_active + "</option></select></td>\n\
                                                           <td class='adminList' id='load" + value.id + "' hidden></td></tr>");
            var employeesListElement = {id: value.id, name: value.name, account: value.account, role: value.role};
            employeesList.push(employeesListElement);
        });
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });
}

function onClickRole(el) {
    id = $(el).attr("id");
    oldRole = el.value;
    newRole = el.value;
    for (var i = 0; i < employeesList.length; i++) {
        if ((employeesList[i]).id == id)
            continue;
        $("#load" + (employeesList[i]).id).hide();
    }
    $("#myReportLoad").attr('disabled', 'disabled');

    if (oldRole === lang.admin) {
        $.getJSON("adminSelectedID?id=" + id, function (json) {
            $("#load" + id).empty();
            $("#load" + id).show();
            $("#load" + id).append("<select id='m" + id + "' multiple='multiple' onchange='getSelectedEmployeesAdmin(" + id + ")'></select>");
            for (var i = 0; i < employeesList.length; i++) {
                $("#m" + id).append("<option value=\"" + (employeesList[i]).id + "\" " +
                        ($.inArray((employeesList[i]).id, json) > -1 || $.inArray(0, json) > -1 ? "selected" : "") +
                        " >  " + (employeesList[i]).name + "</option>");
            }
            $("#m" + id).multiselect({
                // text to use in dummy input
                placeholder: lang.bind_employees,
                // how many columns should be use to show options
                columns: 1,
                // add select all option
                selectAll: true,
                // display the checkbox to the user
                showCheckbox: true
            });
        }).fail(function () {
            $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
        });
    }
}

function onChangeRole(sel) {
    var valueString = "";
    valueString = sel.value;
    newRole = (valueString.substr(0, 1) === "a") ? "admin" : (valueString.substr(0, 1) === "b") ? "user" : "not active";
    id = valueString.substring(1, valueString.length);
    oldRole = "";
    $("#" + id).empty();
    $("#" + id).append("<option" + ((newRole === "admin") ? " selected disabled hidden" : " value='a" + id + "'") + ">" + lang.admin + "</option>\n\
                        <option" + ((newRole === "user") ? " selected disabled hidden" : " value='b" + id + "'") + ">" + lang.user + "</option>\n\
                        <option" + ((newRole === "not active") ? " selected disabled hidden" : " value='c" + id + "'") + ">" + lang.not_active + "</option>");
    for (var i = 0; i < employeesList.length; i++) {
        if ((employeesList[i]).id == id) {
            oldRole = (employeesList[i]).role;
            continue;
        }
        $("#load" + (employeesList[i]).id).hide();
    }
    if (oldRole !== newRole) {

        for (var i = 0; i < employeesList.length; i++) {
            if ((employeesList[i]).id == id)
                continue;
            $("#" + (employeesList[i]).id).attr('disabled', 'disabled');
        }

        if (newRole === "admin") {
            $("#load" + id).empty();
            $("#load" + id).show();
            $("#load" + id).append("<select id='m" + id + "' multiple='multiple' onchange='getSelectedEmployeesAdmin(" + id + ")'></select>");
            for (var i = 0; i < employeesList.length; i++) {
                $("#m" + id).append("<option value=\"" + (employeesList[i]).id + "\">  "
                        + (employeesList[i]).name + "</option>");
            }
            $("#m" + id).multiselect({
                // text to use in dummy input
                placeholder: lang.bind_employees,
                // how many columns should be use to show options
                columns: 1,
                // add select all option
                selectAll: true,
                // display the checkbox to the user
                showCheckbox: true
            });
        } else {
            selectedEmployeesID = [];
            $("#load" + id).hide();
            for (var i = 0; i < employeesList.length; i++) {
                if ((employeesList[i]).id == id)
                    changingAccount = (employeesList[i]).account;
            }
            $("#post_message").hide();
            $("#myReportLoad").text(lang.change_role_for + changingAccount);
            $("#myReportLoad").removeAttr('disabled');
        }
    } else {
        $("#load" + id).hide();
        $("#myReportLoad").attr('disabled', 'disabled');
        for (var i = 0; i < employeesList.length; i++) {
            if ((employeesList[i]).name == $("H2").text())
                continue;
            $("#" + (employeesList[i]).id).removeAttr('disabled');
        }
    }
}

$(function () {
    //******* import data from xlxs part *******   
    $("#fileChooser").click(function () {
        $("#file").trigger('click');
    });

    $("#file").change(function () {
        $("#fileName").text(this.files[0].name);
        vertikalAlignment();
        // calling spinner
        var target = document.getElementById('id_divScrollPart');
        var spinner = new Spinner(opts).spin(target);

        $("#fileSubmit").trigger('click');
    });

    vertikalAlignment();
    // *****************
});

function vertikalAlignment() {
    if ($("#spanFile").text().startsWith(" ")) {
        $("#spanFile").css('color', 'green');
        $("#spanFile").css('padding', '0px 0px');
    } else {
        $("#spanFile").css('color', 'red');
        $("#spanFile").css('padding', '8px 0px');
    }

    if ($("#fileName").text().length <= 33) {
        $("#fileName").css('height', '25px');
        $("#fileName").css('padding-top', '10px');
    } else {
        $("#fileName").css('padding-top', '0px');
        $("#fileName").css('height', '35px');
    }
}

function getSelectedEmployeesAdmin(id) {
    selectedEmployeesID = [];
    var selected = $("#m" + id + " option:selected");
    selected.each(function () {
        selectedEmployeesID.push($(this).val());
    });
    if (selectedEmployeesID.length > 0) {
        for (var i = 0; i < employeesList.length; i++) {
            if ((employeesList[i]).id == id)
                changingAccount = (employeesList[i]).account;
        }
        $("#post_message").hide();
        $("#myReportLoad").text(lang.change_role_for + changingAccount);
        $("#myReportLoad").removeAttr('disabled');
    } else {
        $("#myReportLoad").attr('disabled', 'disabled');
    }
}

function changeRole() {

    // calling spinner
    var target = document.getElementById('id_divScrollPart');
    var spinner = new Spinner(opts).spin(target);
    $.getJSON("changeRole",
            {id: id,
                newRole: newRole,
                oldRole: oldRole,
                changingAccount: changingAccount,
                selectedEmployeesID: JSON.stringify(selectedEmployeesID)},
            function (json) {
                for (var i = 0; i < employeesList.length; i++) {
                    if ((employeesList[i]).name == $("H2").text())
                        continue;
                    $("#" + (employeesList[i]).id).removeAttr('disabled');
                }
                $("#load" + id).hide();
                $("#myReportLoad").attr('disabled', 'disabled');
                $("#post_message").show();
                switch (json.result) {
                    case "error_updating_DB":
                        $("#post_message_span").text(lang.error_updating_DB);
                        break;
                    case "successfully_changed":
                        $("#post_message_span").text(lang.successfully_changed);
                        break;
                    case "error_with_adding_new_admin_roles":
                        $("#post_message_span").text(lang.error_with_adding_new_admin_roles);
                        break;
                    case "error_with_deleting_old_admin_roles":
                        $("#post_message_span").text(lang.error_with_deleting_old_admin_roles);
                        break;
                }

                prepareTableEmployee();
                spinner.stop();
            })
            .fail(function () {
                spinner.stop();
                $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
            });
}

function searchNewEmployee() {
    $("#addNewUser_table").empty();
    $("#searchDialogMessage").hide();
    $("#addNewUserBox").hide();
    $("#adminRoleList").hide();
    $("#buttonAddNewEmployee").attr('disabled', 'disabled');
    newEmployeeName = "";
    login = "";
    login = $("#inputSearch").val();
    if (login === "") {
        $("#searchDialogMessage").show();
        $("#searchDialogMessage").text(lang.input_login_cl);
        return;
    }
    if (!login.match("^[A-Za-z]{1}[0-9]+$")) {
        $("#searchDialogMessage").show();
        $("#searchDialogMessage").text(lang.incorrect_login_format);
        return;
    }

    $.getJSON("searchEmployee?login=" + login, function (json) {
        if (json.result == "notExist") {
            $("#searchDialogMessage").show();
            $("#searchDialogMessage").text(lang.this_login_does_not_exist);
        } else {
            newEmployeeName = json.result;
            $("#addNewUserBox").show();
            $("#addNewUser_table").append("<tr><td class = 'nameColumn'>" + newEmployeeName + "</td>\n\
                                               <td class = 'loginSmall'>" + login + "</td>\n\
                                               <td class = 'selectColumn'><select class='selectEl' id='newUserID' onchange = 'onChangeRoleAdd(this)' >\n\
                                                                       <option value='Choose role' disabled selected>" + lang.choose_role + "</option>\n\
                                                                       <option value='admin'>" + lang.admin + "</option>\n\
                                                                       <option value='user'>" + lang.user + "</option>\n\
                                                                       <option value='not active'>" + lang.not_active + "</option></select></td></tr>");
        }
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });
}

function onChangeRoleAdd(sel) {
    newRoleAdd = sel.value;
    if (newRoleAdd === "admin") {
        $("#buttonAddNewEmployee").attr('disabled', 'disabled');
        $("#adminRoleList").empty();
        $("#adminRoleList").show();
        $("#adminRoleList").append("<select id='m' multiple='multiple' onchange='getSelectedEmployeesAdminAdd()'></select>");
        for (var i = 0; i < employeesList.length; i++) {
            $("#m").append("<option value=\"" + (employeesList[i]).id + "\">  "
                    + (employeesList[i]).name + "</option>");
        }
        $("#m").multiselect({
            // text to use in dummy input
            placeholder: lang.bind_employees,
            // how many columns should be use to show options
            columns: 1,
            // add select all option
            selectAll: true,
            // display the checkbox to the user
            showCheckbox: true
        });
    } else {
        selectedEmployeesIDAdd = [];
        $("#adminRoleList").hide();
        $("#buttonAddNewEmployee").removeAttr('disabled');
    }
}

function getSelectedEmployeesAdminAdd() {
    selectedEmployeesIDAdd = [];
    var selected = $("#m option:selected");
    selected.each(function () {
        selectedEmployeesIDAdd.push($(this).val());
    });
    if (selectedEmployeesIDAdd.length > 0) {
        $("#buttonAddNewEmployee").removeAttr('disabled');
    } else {
        $("#buttonAddNewEmployee").attr('disabled', 'disabled');
    }
}

function addNewEmployee() {
    // calling spinner
    var target = document.getElementById('id_divScrollPart');
    var spinner = new Spinner(opts).spin(target);
    $.getJSON("addNewEmployee",
            {newRoleAdd: newRoleAdd,
                login: login,
                newEmployeeName: newEmployeeName,
                selectedEmployeesIDAdd: JSON.stringify(selectedEmployeesIDAdd)},
            function (json) {
                $("#addNewUserBox").hide();
                $("#adminRoleList").hide();
                $("#buttonAddNewEmployee").attr('disabled', 'disabled');
                if (json.result == "userAlreadyExist") {
                    $("#searchDialogMessage").show();
                    $("#searchDialogMessage").text(lang.employee_already__registered);
                }
                if (json.result == "Error: updating DB") {
                    $("#searchDialogMessage").show();
                    $("#searchDialogMessage").text(lang.error_updating_DB);
                }
                if (json.result == "Successfully added") {
                    $("#myReportLoad").attr('disabled', 'disabled');
                    $("#post_message").show();
                    $("#post_message_span").text(lang.successfully_added);
                    prepareTableEmployee();
                }
                spinner.stop();
            })
            .fail(function () {
                spinner.stop();
                $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
            });

}

