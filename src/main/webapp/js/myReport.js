var DEFAULT_NUM_DAYS_REPORTED = 13;

// init datePicker and check if changed
$(function () {
    if (lang == ua) {
        $("input.filterDate").datepicker($.datepicker.regional["uk"]); //"" - eng, "uk" - ukr, "ru" - rus
    } else {
        if (lang == ru) {
            $("input.filterDate").datepicker($.datepicker.regional["ru"]);
        } else {
            $("input.filterDate").datepicker($.datepicker.regional[""]);
        }
    }
    $("input.filterDate").datepicker("option", "changeMonth", true);
    $("input.filterDate").datepicker("option", "changeYear", true);
    $("input.filterDate").datepicker("option", "firstDay", 1);
    $("input.filterDate").datepicker("option", "dateFormat", "dd-mm-yy");
    setDayPickerDefaultEndOfWeek();

    $("input.filterDate").change(function () {
        $("#post_message").hide();
        $("#myReportLoad").removeAttr('disabled');
        checkErrors();
    });

});

// block current page button in menu 
function loadButtonBlock() {
    blockCurrentPageButton("ref_reports");
}

function loadSelectedLanguage() {
    $("#lang_select").append("<option " + ((lang.lang_ru == 'ru') ? "selected" : "") + " value='en'>" + lang.lang_en + "</option>\n\
                              <option " + ((lang.lang_ru == 'рус.') ? "selected" : "") + " value='ru'>" + lang.lang_ru + "</option>\n\
                              <option " + ((lang.lang_ru == 'рос.') ? "selected" : "") + " value='ua'>" + lang.lang_ua + "</option>");
    $(':input[value="Logout"]').val(lang.logout);
    $(':input[value="Main"]').val(lang.main);
    $(':input[value="My report"]').val(lang.my_report);
    $(':input[value="SuperViewer"]').val(lang.super_viewer);
    $(':input[value="AdminPage"]').val(lang.admin_page);
    $("#myReportLoad").text(lang.load_data);
    $("#dateFrom").text(lang.date_from);
    $("#dateTo").text(lang.date_to);
    $("#add_user_report_row_button_U").text(lang.add_new_row);
}

function setDayPickerDefaultEndOfWeek() {
    var today = new Date();
    var dayFromDefault = new Date();
    var dayToDefault = new Date();
    dayToDefault.setDate(today.getDate() + (7 - today.getDay()));
    dayFromDefault.setDate(today.getDate() + (7 - today.getDay()) - DEFAULT_NUM_DAYS_REPORTED);
    $("#filterDateTo").datepicker("setDate", dayToDefault);
    $("#filterDateFrom").datepicker("setDate", dayFromDefault);
}

function checkErrors() {
    noDatesError = 0;
    $("input.filterDate").css('border-color', '#517599');
    $("input.filterDateSuperViewer").css('border-color', '#517599');  //for superViewerPage

    var from_date_format = $("#filterDateFrom").datepicker("getDate");
    var to_date_format = $("#filterDateTo").datepicker("getDate");
    if (from_date_format > to_date_format) {
        noDatesError = 1;
    }

    var x = document.getElementById("filterDateFrom").value;
    var y = document.getElementById("filterDateTo").value;
    if (!moment(x, "DD-MM-YYYY", true).isValid()) {
        noDatesError = 2;
        $("#filterDateFrom").css('border-color', 'red');
    }
    if (!moment(y, "DD-MM-YYYY", true).isValid()) {
        noDatesError = 2;
        $("#filterDateTo").css('border-color', 'red');
    }
    if (noDatesError == 1) {
        $("input.filterDate").css('border-color', 'red');
        $("input.filterDateSuperViewer").css('border-color', 'red'); //for superViewerPage
    }
}

function myReportLoad() {
    switch (noDatesError) {
        case 0:
            var from = document.getElementById("filterDateFrom").value; //string date format dd-mm-yyyy
            var to = document.getElementById("filterDateTo").value; //string date format dd-mm-yyyy
            var from_date_format = $("#filterDateFrom").datepicker("getDate"); //date format
            var to_date_format = $("#filterDateTo").datepicker("getDate"); //date format

            // calling spinner
            var target = document.getElementById('id_divScrollPart');
            var spinner = new Spinner(opts).spin(target);

            $.getJSON("reports-my-report",
                    {dateFrom: from, dateTo: to, myUnitedReport: JSON.stringify(myUnitedReport)},
                    function (json) {
                        getMyReport(json);
                        // transmitted parameters are needed to calculate workHours in function fillingSumColumnTableSummary()
                        populateSummaryReportTable(from_date_format, to_date_format);
                        populateUnitedReportTable();

                        var countElements = $.map(myReport, function (n, i) {
                            return i;
                        }).length;
                        if (countElements == 2) {
                            $("#post_message").show();
                            $("#post_message_span").text(lang.no_elements_in_period);
                        } else {
                            $("#post_message").hide();
                        }

                        spinner.stop();
                    })
                    .fail(function () {
                        spinner.stop();
                        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
                    });
            $("#myReportLoad").attr('disabled', 'disabled');
            break;
        case 1:
            $.alertable.alert(lang.DATE_ORDERING_ERROR);
            break;
        case 2:
            $.alertable.alert(lang.DATE_FORMAT_ERROR);
            break;
    }
}

function getMyReport(json) {
    var JSONstr = JSON.stringify(json.firstTable);
    //save to global variable myReport declared at <head> of reports.jsp 
    myReport = JSON.parse(JSONstr, function (key, value) {
        if (key.search("date") > -1 && typeof value === 'number') {
            return new Date(value);
        } else {
            return value;
        }
    });
    myUnitedReport = json.secondTable;
}

function populateSummaryReportTable(from_date_format, to_date_format) {
    var userReportRecordsLine;

    //clean the table element
    $("#user_summary_report_table").text("");

    //sorting by header
    var myReportSortForShow = sortMyReportSortForShow();

    //create a new row if there is another one work instance in the userReport
    $.each(myReportSortForShow, function (i, userReportRow) {
        userReportRecordsLine = new Array(); //array to populate from
        $("#user_summary_report_table").append("<tr id = 'wi" + userReportRow.work_instance.id + "'>" +
                "<td class = 'user_report_row_head' title='" +
                userReportRow.work_instance.work_type_title + "\n" +
                userReportRow.work_instance.work_title_title + "\n" +
                userReportRow.work_instance.work_form_title + "\n" +
                userReportRow.work_instance.details +
                "'>" +
                "<p class='work_type_title'>" +
                userReportRow.work_instance.work_type_title +
                "</p><p class='work_title_title'>" +
                userReportRow.work_instance.work_title_title +
                "</p><p class='work_form_title'>" +
                userReportRow.work_instance.work_form_title +
                "</p><p class='work_instance_details'>" +
                userReportRow.work_instance.details +
                "</p></td></tr>");

        //fill the userReportRecordsLine with the report records for currend work instance
        $.each(userReportRow, function (rr_key, reportRecord) {
            if (rr_key != "work_instance") {
                userReportRecordsLine.push(reportRecord);
            }
        });

        userReportRecordsLine.sort(function (a, b) {
            return new Date(a.date_reported) - new Date(b.date_reported);
        });

        //add report records cells to current line
        $.each(userReportRecordsLine, function (indexArray, reportRecord) {
            $("#wi" + userReportRow.work_instance.id).append("<td class = 'user_report_row_fields'>" + reportRecord.hours_num + "</td>");
        });
    });
    fillingSumColumnTableSummary(from_date_format, to_date_format);
}

function sortMyReportSortForShow() {
    var myReportSortForShow = [];
    $.each(myReport, function (wi_key, userReportRow) {
        if (wi_key !== "date_from" && wi_key !== "date_to") {
            myReportSortForShow.push(userReportRow);
        }
    });
    myReportSortForShow.sort(function (a, b) {
        if (a.work_instance.work_type_title > b.work_instance.work_type_title)
            return 1;
        if (a.work_instance.work_type_title < b.work_instance.work_type_title)
            return -1;
        if (a.work_instance.work_title_title > b.work_instance.work_title_title)
            return 1;
        if (a.work_instance.work_title_title < b.work_instance.work_title_title)
            return -1;
        if (a.work_instance.work_form_title > b.work_instance.work_form_title)
            return 1;
        if (a.work_instance.work_form_title < b.work_instance.work_form_title)
            return -1;
        if (a.work_instance.details > b.work_instance.details)
            return 1;
        if (a.work_instance.details < b.work_instance.details)
            return -1;
        return 0;
    });
    return myReportSortForShow;
}

// statistic function
function fillingSumColumnTableSummary(from_date_format, to_date_format) {

    //clean the table element
    $("#sum_row_table_summary").text("");

    var numberOFRows = 0;
    var numberOFWorkHours = 0;

    $("#user_summary_report_table tr").each(function (index, element) {
        numberOFRows = index + 1;
    });

    //calculate workHours for period
    for (columnDate = from_date_format;
            columnDate <= to_date_format;
            columnDate.setDate(columnDate.getDate() + 1)) {
        if (columnDate.getDay() != 0 && columnDate.getDay() != 6) { //not sunday and saturday
            numberOFWorkHours += 8; // 8 working hours per day
        }
    }

    if (numberOFRows > 0) {
        var sumByRows = 0;

        //filling sumByRows
        $("#user_summary_report_table .user_report_row_fields").each(function (index, element) {
            if (!isNaN(element.innerHTML) && element.innerHTML > 0) {
                sumByRows += parseInt(element.innerHTML);
            }
        });

        var colorIndicatorHoursSummary = "";
        if (sumByRows < numberOFWorkHours) {
            colorIndicatorHoursSummary = " class = 'less8hours'";
        } else {
            if (sumByRows == numberOFWorkHours) {
                colorIndicatorHoursSummary = " class = 'hoursGood'";
            } else {
                colorIndicatorHoursSummary = " class = 'overWork'";
            }
        }

        $("#sum_row_table_summary").append("<tr id='sum_row_table_row'><td><p" +
                colorIndicatorHoursSummary + ">" +
                sumByRows + "/" + numberOFWorkHours + "</p></td></tr>");
    }
}

function populateUnitedReportTable() {

    //clean the table element
    $("#user_summary_report_table_union").text("");

    //sorting by header
    var myUnitedReportSortForShow = sortMyUnitedReportSortForShow();

    $.each(myUnitedReportSortForShow, function (i, myUnitedReportElement) {
        $("#user_summary_report_table_union").append("<tr id = '" + myUnitedReportElement.union_row_id + "'>" +
                "<td class = 'user_report_row_head' title='" +
                myUnitedReportElement.workType + "\n" +
                myUnitedReportElement.workTitle + "\n" +
                myUnitedReportElement.workForm + "\n" +
                myUnitedReportElement.workInstance + "\n" +
                "'>" +
                "<p class='work_type_title'>" +
                myUnitedReportElement.workType +
                "</p><p class='work_title_title'>" +
                myUnitedReportElement.workTitle +
                "</p><p class='work_form_title'>" +
                myUnitedReportElement.workForm +
                "</p><p class='work_instance_details'>" +
                myUnitedReportElement.workInstance +
                "</p></td></tr>");
        $("#" + myUnitedReportElement.union_row_id).append("<td id='" + myUnitedReportElement.union_row_id + "_cell" +
                "' class = 'user_report_row_fields'>" +
                myUnitedReportElement.unionHours + "</td>");
    });
}

function sortMyUnitedReportSortForShow() {
    var myUnitedReportSortForShow = [];
    $.each(myUnitedReport, function (union_row_id, myUnitedReportElement) {
        myUnitedReportSortForShow.push(myUnitedReportElement);
    });
    myUnitedReportSortForShow.sort(function (a, b) {
        if (a.workType > b.workType)
            return 1;
        if (a.workType < b.workType)
            return -1;
        if (a.workTitle > b.workTitle)
            return 1;
        if (a.workTitle < b.workTitle)
            return -1;
        if (a.workForm > b.workForm)
            return 1;
        if (a.workForm < b.workForm)
            return -1;
        if (a.workInstance > b.workInstance)
            return 1;
        if (a.workInstance < b.workInstance)
            return -1;
        return 0;
    });
    return myUnitedReportSortForShow;
}

/* ------------- union table block  ----------------- */

function prepareAddingUserReportRowU() {
    $.getJSON("work-types", function (json) {
        $("#work_type_select_U").empty();
        $("#work_type_select_arrow").hide();
        $("#work_title_select_U").hide();
        $("#work_title_select_arrow").hide();
        $("#work_form_select_U").hide();
        $("#work_form_select_arrow").hide();
        $("#work_instance_select_U").hide();
        $("#add_user_report_row_button_U").hide();
        $("#work_type_select_U").append("<option value='0' disabled selected>" + lang.add_row + "</option>");
        $.each(json, function (indexInArray, value) {
            $("#work_type_select_U").append("<option value=\"" + value.id + "\">" + value.title + "</option>");
        });
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });
}

function getWorkTitlesU() {
    var workTypeid = $("#work_type_select_U").val();

    $.getJSON("work-titles?work-type-id=" + workTypeid, function (json) {
        $("#work_type_select_U option[value='0']").remove();
        $("#work_type_select_arrow").show();
        $("#work_title_select_U").empty();
        $("#work_title_select_U").show();
        $("#work_title_select_arrow").hide();
        $("#work_form_select_U").hide();
        $("#work_form_select_arrow").hide();
        $("#work_instance_select_U").hide();
        $("#add_user_report_row_button_U").show();
        $("#work_title_select_U").append("<option value='0' disabled selected >" + lang.select_title + "</option>");
        $.each(json, function (indexInArray, value) {
            $("#work_title_select_U").append("<option value=\"" + value.id + "\" >" + value.title + "</option>");
        });
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });

    $("#work_title_select_U").val("");
    $("#work_form_select_U").val("");
    $("#work_instance_select_U").val("");
}

function getWorkFormsU() {
    var workTypeid = $("#work_type_select_U").val();

    $.getJSON("work-forms?work-type-id=" + workTypeid, function (json) {
        $("#work_title_select_U option[value='0']").remove();
        $("#work_title_select_arrow").show();
        $("#work_form_select_U").empty();
        $("#work_form_select_U").show();
        $("#work_form_select_arrow").hide();
        $("#work_instance_select_U").hide();
        $("#work_form_select_U").append("<option value='0' disabled selected>" + lang.select_work_form + "</option>");
        $.each(json, function (indexInArray, value) {
            $("#work_form_select_U").append("<option value=\"" + value.id + "\">" + value.title + "</option>");
        });
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });

    $("#work_form_select_U").val("");
    $("#work_instance_select_U").val("");
}

function getWorkInstanceDetailsU() {
    $("#work_form_select_U option[value='0']").remove();
    var workTitleId = $("#work_title_select_U").val();
    var workFormId = $("#work_form_select_U").val();

    $.getJSON("work-instances?work-title-id=" + workTitleId + "&work-form-id=" + workFormId, function (json) {
        $("#work_form_select_arrow").show();
        $("#work_instance_select_U").empty();
        $("#work_instance_select_U").show();
        if (json.length === 0) {
            $("#work_instance_select_U").append("<option value='0' disabled selected>" + lang.no_saved_details + "</option>");
        } else {
            $("#work_instance_select_U").append("<option value='0' disabled selected>" + lang.select_details + "</option>");
        }
        $.each(json, function (indexInArray, value) {
            if (value.details !== "") {
                $("#work_instance_select_U").append("<option value=\"" + value.id + "\">" + value.details + "</option>");
            }
        });
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });

    $("#work_instance_select_U").val("");
}

function addUserReportRowU() {
    var workTypeId = $("#work_type_select_U").val();
    var workTitleId = $("#work_title_select_U").val() === null ? 0 : $("#work_title_select_U").val();
    var workFormId = $("#work_form_select_U").val() === null ? 0 : $("#work_form_select_U").val();
    var workInstanceId = $("#work_instance_select_U").val() === null ? 0 : $("#work_instance_select_U").val();
    var union_row_id = workTypeId + "_" + workTitleId + "_" + workFormId + "_" + workInstanceId;

    //check for dublicate
    for (item in myUnitedReport) {
        if (item == union_row_id) {
            $.alertable.alert(lang.FORM_HAS_THIS_ROW_ERROR);
            return;
        }
    }

    var myUnitedReportElement = {
        union_row_id: union_row_id,
        workType: $("#work_type_select_U :selected").text(),
        workTitle: (workTitleId === 0 ? "" : $("#work_title_select_U :selected").text()),
        workForm: (workFormId === 0 ? "" : $("#work_form_select_U :selected").text()),
        workInstance: (workInstanceId === 0 ? "" : $("#work_instance_select_U :selected").text()),
        unionHours: 0};
    myUnitedReport[union_row_id] = myUnitedReportElement;

    $("#user_summary_report_table_union").append("<tr id = '" + union_row_id + "'>" +
            "<td class = 'user_report_row_head' title='" +
            myUnitedReportElement.workType + "\n" +
            myUnitedReportElement.workTitle + "\n" +
            myUnitedReportElement.workForm + "\n" +
            myUnitedReportElement.workInstance + "\n" +
            "'>" +
            "<p class='work_type_title'>" +
            myUnitedReportElement.workType +
            "</p><p class='work_title_title'>" +
            myUnitedReportElement.workTitle +
            "</p><p class='work_form_title'>" +
            myUnitedReportElement.workForm +
            "</p><p class='work_instance_details'>" +
            myUnitedReportElement.workInstance +
            "</p></td></tr>");

    $("#" + union_row_id).append("<td id='" + union_row_id + "_cell" + "' class = 'user_report_row_fields'></td>");

    $(".lineToSeparateTables").show();
    $("#myReportLoad").removeAttr('disabled');
}
