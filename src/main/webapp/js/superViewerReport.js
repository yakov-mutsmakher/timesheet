// init datePicker and check if changed
$(function () {
    if (lang == ua) {
        $("input.filterDateSuperViewer").datepicker($.datepicker.regional["uk"]); //"" - eng, "uk" - ukr, "ru" - rus
    } else {
        if (lang == ru) {
            $("input.filterDateSuperViewer").datepicker($.datepicker.regional["ru"]);
        } else {
            $("input.filterDateSuperViewer").datepicker($.datepicker.regional[""]);
        }
    }
    $("input.filterDateSuperViewer").datepicker("option", "changeMonth", true);
    $("input.filterDateSuperViewer").datepicker("option", "changeYear", true);
    $("input.filterDateSuperViewer").datepicker("option", "firstDay", 1);
    $("input.filterDateSuperViewer").datepicker("option", "dateFormat", "dd-mm-yy");
    setDayPickerDefaultEndOfWeek();

    $("input.filterDateSuperViewer").change(function () {
        if (selectedEmployeesID.length > 0) {
            $("#post_message").hide();
            $("#myReportLoad").removeAttr('disabled');
            checkErrors();
        } else {
            $("#myReportLoad").attr('disabled', 'disabled');
            $("#post_message").show();
            $("#post_message_span").text(lang.select_employees_to_load_data);
        }
        checkErrors();
    });
});

$(function () {
    $("#checkboxEmpl").change(function () {
        showOnlyActiveUsers = $("#checkboxEmpl")[0].checked.toString();
        sessionStorage.setItem('showOnlyActiveUsers', showOnlyActiveUsers);
        reloadSelectEmployee();
        $("#divLoadToExcelbox").hide();
    });

    $("#myReportLoad").bind("DOMSubtreeModified", function () {
        var attr = $("#myReportLoad").attr('disabled');
        if (typeof attr !== typeof undefined && attr !== false && selectedEmployeesID.length > 0) {
            //  $("#saveDetailedReportToExcel").removeAttr('disabled');
            $("#divLoadToExcelbox").show();
        } else {
            //    $("#saveDetailedReportToExcel").attr('disabled', 'disabled');
            $("#divLoadToExcelbox").hide();
        }
    });
});

// block current page button in menu 
$(function () {
    blockCurrentPageButton("ref_superViewerPage");
});


function loadSelectedLanguageSuperViewer() {
    $("#lang_select").append("<option " + ((lang.lang_ru == 'ru') ? "selected" : "") + " value='en'>" + lang.lang_en + "</option>\n\
                              <option " + ((lang.lang_ru == 'рус.') ? "selected" : "") + " value='ru'>" + lang.lang_ru + "</option>\n\
                              <option " + ((lang.lang_ru == 'рос.') ? "selected" : "") + " value='ua'>" + lang.lang_ua + "</option>");
    $(':input[value="Logout"]').val(lang.logout);
    $(':input[value="Main"]').val(lang.main);
    $(':input[value="My report"]').val(lang.my_report);
    $(':input[value="SuperViewer"]').val(lang.super_viewer);
    $(':input[value="AdminPage"]').val(lang.admin_page);
    $("#myReportLoad").text(lang.load_data);
    $("#header_select_work_group_by").text(lang.select_group_by);
    $("#saveDetailedReportToExcel").text(lang.save_detailed_report_to_excel);
    $("#dateFrom").text(lang.date_from);
    $("#dateTo").text(lang.date_to);
    $("#headerCheckboxEmpl").text(lang.show_only_activ_emloyees);
    $("#add_user_report_row_button_U").text(lang.add_new_row);
    $("#header_PlanCompare").text(lang.compare_with_plan_data);
    $("#button_PlanCompare").text(lang.compare);
}

function prepareSelectEmployee() {
    $("#select_employee").empty();
    employeesList = [];
    $.getJSON("employees?onlyActive=" + showOnlyActiveUsers, function (json) {
        $.each(json, function (indexInArray, value) {
            $("#select_employee").append("<option value=\"" + value.id + "\">  "
                    + value.name + " (" + value.account + ", status : " + value.role + ")</option>");
            var employeesListElement = {id: value.id, name: value.name, account: value.account, role: value.role};
            employeesList.push(employeesListElement);
        });
        $("#select_employee").multiselect({
            // text to use in dummy input
            placeholder: lang.select_employees,
            // how many columns should be use to show options
            columns: 1,
            // include option search box   
            search: true,
            // search filter options
            searchOptions: {
                default: lang.search // search input placeholder text
            },
            // add select all option
            selectAll: true,
            // display the checkbox to the user
            showCheckbox: true
        });
        getSelectedEmployees();
    }).fail(function () {
        $.alertable.alert(GET_JSON_ERROR_MESSAGE);
    });

    //prepare select_work_group_by
    $("#select_work_group_by").append("<option value='work_form_id' selected>" + lang.work_form + "</option>");
    $("#select_work_group_by").append("<option value='work_title_id'>" + lang.work_title + "</option>");
    $("#select_work_group_by").append("<option value='work_type_id' >" + lang.work_type + "</option>");
}

function reloadSelectEmployee() {
    $("#select_employee").empty();
    employeesList = [];
    $.getJSON("employees?onlyActive=" + showOnlyActiveUsers, function (json) {
        $.each(json, function (indexInArray, value) {
            $("#select_employee").append("<option value=\"" + value.id + "\">  "
                    + value.name + " (" + value.account + ", status : " + value.role + ")</option>");
            var employeesListElement = {id: value.id, name: value.name, account: value.account, role: value.role};
            employeesList.push(employeesListElement);
        });
        $("#select_employee").multiselect('reload');
        getSelectedEmployees();
    }).fail(function () {
        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
    });
}

function getSelectedEmployees() {
    selectedEmployeesID = [];
    var selected = $("#select_employee option:selected");
    selected.each(function () {
        selectedEmployeesID.push($(this).val());
    });
    if (selectedEmployeesID.length > 0) {
        $("#post_message").hide();
        $("#myReportLoad").removeAttr('disabled');
        checkErrors();
    } else {
        $("#myReportLoad").attr('disabled', 'disabled');
        $("#post_message").show();
        $("#post_message_span").text(lang.select_employees_to_load_data);
    }
    updateMySuperViewerUnitedReport();
}

function superViewerReportLoad() {
    switch (noDatesError) {
        case 0:
            var from = document.getElementById("filterDateFrom").value; //string date format dd-mm-yyyy
            var to = document.getElementById("filterDateTo").value; //string date format dd-mm-yyyy
            var from_date_format = $("#filterDateFrom").datepicker("getDate"); //date format
            var to_date_format = $("#filterDateTo").datepicker("getDate"); //date format

            //to avoid 400(Bad Request)
            var str = JSON.stringify(selectedEmployeesID) + JSON.stringify(mySuperViewerUnitedReport);
            if (str.length > 2950) {
                $.alertable.alert(lang.GET_JSON_MESSAGE_DATA_OVERLOADED);
                $("#myReportLoad").attr('disabled', 'disabled');
                return;
            }

            // calling spinner
            var target = document.getElementById('id_divScrollPart');
            var spinner = new Spinner(opts).spin(target);
            $.getJSON("superViewerReport",
                    {dateFrom: from,
                        dateTo: to,
                        selectedEmployeesID: JSON.stringify(selectedEmployeesID),
                        mySuperViewerUnitedReport: JSON.stringify(mySuperViewerUnitedReport)},
                    function (json) {
                        mySuperViewerUnitedReport = json.superViewerUnitedReportTable;
                        // transmitted parameters are needed to calculate workHours in function fillingSumColumnTableSummary()
                        populateSuperViewerEmployeesTable(from_date_format, to_date_format, json.superViewerReportTable);
                        populateSuperViewerUnitedReportTable(mySuperViewerUnitedReport);

                        spinner.stop();
                    })
                    .fail(function () {
                        spinner.stop();
                        $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
                    });
            $("#myReportLoad").attr('disabled', 'disabled');
            break;
        case 1:
            $.alertable.alert(lang.DATE_ORDERING_ERROR);
            break;
        case 2:
            $.alertable.alert(lang.DATE_FORMAT_ERROR);
            break;
    }
}

function populateSuperViewerEmployeesTable(from_date_format, to_date_format, json) {
    //clean the table element
    $("#super_viewer_employees_hours_table").text("");

    //calculate workHours for period
    var numberOFWorkHours = 0;
    for (dateCount = from_date_format;
            dateCount <= to_date_format;
            dateCount.setDate(dateCount.getDate() + 1)) {
        if (dateCount.getDay() !== 0 && dateCount.getDay() !== 6) { //not sunday and saturday
            numberOFWorkHours += 8; // 8 working hours per day
        }
    }

    $("#super_viewer_employees_hours_table").append("<tr class='tr_head'>\n\
                         <td>" + lang.employee_name + "</td>\n\
                         <td>" + lang.entered_hours + "</td>\n\
                         <td>" + lang.hours_in_period + "</td>\n\
                         <td>" + lang.done + "</td>\n\
                         </tr>");

    for (var i = 0; i < employeesList.length; i++) {
        for (var item in json) {
            if (employeesList[i].id == item) {
                var hours = json[item];
                var colorIndicatorHoursSummary = "";
                if (hours < numberOFWorkHours) {
                    colorIndicatorHoursSummary = " class = 'less8hoursT'";
                } else {
                    if (hours === numberOFWorkHours) {
                        colorIndicatorHoursSummary = " class = 'hoursGoodT'";
                    } else {
                        colorIndicatorHoursSummary = " class = 'overWorkT'";
                    }
                }

                var precentage = hours / numberOFWorkHours * 100;
                $("#super_viewer_employees_hours_table").append("<tr>\n\
                         <td class = 'firstColumn'>" + employeesList[i].name + "</td>\n\
                         <td>" + hours + "</td>\n\
                         <td>" + numberOFWorkHours + "</td>\n\
                         <td" + colorIndicatorHoursSummary + ">" + precentage.toFixed(2) + "%</td>\n\
                         </tr>");
            }
        }
    }

    if ($("#planCompare_table").text() !== "") {
        $(".lineToSeparateTables2").show();
        $("#planCompare_table").css('margin-top', '30px');
    }
}

function populateSuperViewerUnitedReportTable(mySuperViewerUnitedReport) {
    //clean the table element
    $("#super_viewer_employees_table_union").text("");

    //sorting by header
    var mySuperViewerUnitedReportSortForShow = sortMySuperViewerUnitedReportSortForShow();

    $.each(mySuperViewerUnitedReportSortForShow, function (i, mySuperViewerUnitedReportElement) {
        $("#super_viewer_employees_table_union").append("<tr id = '" + mySuperViewerUnitedReportElement.union_row_id + "'>" +
                "<td class = 'super_viewer_row_head' title='" +
                mySuperViewerUnitedReportElement.workType + "\n" +
                mySuperViewerUnitedReportElement.workTitle + "\n" +
                mySuperViewerUnitedReportElement.workForm + "\n" +
                mySuperViewerUnitedReportElement.workInstance + "\n" +
                "'>" +
                "<p class='work_type_title'>" +
                mySuperViewerUnitedReportElement.workType +
                "</p><p class='work_title_title'>" +
                mySuperViewerUnitedReportElement.workTitle +
                "</p><p class='work_form_title'>" +
                mySuperViewerUnitedReportElement.workForm +
                "</p><p class='work_instance_details'>" +
                mySuperViewerUnitedReportElement.workInstance +
                "</p></td></tr>");
        var selectedEmployeesHoursInstanceList = mySuperViewerUnitedReportElement.selectedEmployeesHoursInstance;

        //using employeesList to sort hours by employee name (employeesList has sorted by name)
        for (var i = 0; i < employeesList.length; i++) {
            for (var item in selectedEmployeesHoursInstanceList) {
                if (employeesList[i].id == item) {
                    $("#" + mySuperViewerUnitedReportElement.union_row_id).append("<td class = 'superViewerUnited_user_report_row_fields'>" +
                            selectedEmployeesHoursInstanceList[item] + "</td>");
                }
            }
        }
    });
}

function sortMySuperViewerUnitedReportSortForShow() {
    var mySuperViewerUnitedReportSortForShow = [];
    $.each(mySuperViewerUnitedReport, function (union_row_id, myUnitedReportElement) {
        mySuperViewerUnitedReportSortForShow.push(myUnitedReportElement);
    });
    mySuperViewerUnitedReportSortForShow.sort(function (a, b) {
        if (a.workType > b.workType)
            return 1;
        if (a.workType < b.workType)
            return -1;
        if (a.workTitle > b.workTitle)
            return 1;
        if (a.workTitle < b.workTitle)
            return -1;
        if (a.workForm > b.workForm)
            return 1;
        if (a.workForm < b.workForm)
            return -1;
        if (a.workInstance > b.workInstance)
            return 1;
        if (a.workInstance < b.workInstance)
            return -1;
        return 0;
    });
    return mySuperViewerUnitedReportSortForShow;
}

function addSuperViewerReportRowU() {
    var workTypeId = $("#work_type_select_U").val();
    var workTitleId = $("#work_title_select_U").val() === null ? 0 : $("#work_title_select_U").val();
    var workFormId = $("#work_form_select_U").val() === null ? 0 : $("#work_form_select_U").val();
    var workInstanceId = $("#work_instance_select_U").val() === null ? 0 : $("#work_instance_select_U").val();
    var union_row_id = workTypeId + "_" + workTitleId + "_" + workFormId + "_" + workInstanceId;
    //check for dublicate
    for (var item in mySuperViewerUnitedReport) {
        if (item === union_row_id) {
            $.alertable.alert(lang.FORM_HAS_THIS_ROW_ERROR);
            return;
        }
    }

    var selectedEmployeesHoursInstance = {};
    var mySuperViewerUnitedReportElement = {
        union_row_id: union_row_id,
        workType: $("#work_type_select_U :selected").text(),
        workTitle: (workTitleId === 0 ? "" : $("#work_title_select_U :selected").text()),
        workForm: (workFormId === 0 ? "" : $("#work_form_select_U :selected").text()),
        workInstance: (workInstanceId === 0 ? "" : $("#work_instance_select_U :selected").text()),
        selectedEmployeesHoursInstance: selectedEmployeesHoursInstance};
    mySuperViewerUnitedReport[union_row_id] = mySuperViewerUnitedReportElement;

    $("#super_viewer_employees_table_union").append("<tr id = '" + union_row_id + "'>" +
            "<td class = 'super_viewer_row_head' title='" +
            mySuperViewerUnitedReportElement.workType + "\n" +
            mySuperViewerUnitedReportElement.workTitle + "\n" +
            mySuperViewerUnitedReportElement.workForm + "\n" +
            mySuperViewerUnitedReportElement.workInstance + "\n" +
            "'>" +
            "<p class='work_type_title'>" +
            mySuperViewerUnitedReportElement.workType +
            "</p><p class='work_title_title'>" +
            mySuperViewerUnitedReportElement.workTitle +
            "</p><p class='work_form_title'>" +
            mySuperViewerUnitedReportElement.workForm +
            "</p><p class='work_instance_details'>" +
            mySuperViewerUnitedReportElement.workInstance +
            "</p></td></tr>");
//    $("#" + union_row_id).append("<td id='" + union_row_id + "_cell" + "' class = 'user_report_row_fields'></td>");
    $(".lineToSeparateTables").show();
    if (selectedEmployeesID.length > 0) {
        $("#post_message").hide();
        $("#myReportLoad").removeAttr('disabled');
        checkErrors();
    } else {
        $("#myReportLoad").attr('disabled', 'disabled');
        $("#post_message").show();
        $("#post_message_span").text(lang.select_employees_to_load_data);
    }
    updateMySuperViewerUnitedReport();
}

function updateMySuperViewerUnitedReport() {
    $("#names_header_for_table_union").text("");
    var selectedEmployeesHoursInstance = {};
    for (var i = 0; i < selectedEmployeesID.length; i++) {
        selectedEmployeesHoursInstance[selectedEmployeesID[i]] = 0;
    }

    if (Object.keys(mySuperViewerUnitedReport).length !== 0) {
        for (var x in mySuperViewerUnitedReport) {
            mySuperViewerUnitedReport[x].selectedEmployeesHoursInstance = selectedEmployeesHoursInstance;
        }

        $("#names_header_for_table_union").append("<tr id='names_header_for_table_union_row'></tr>");
        for (var i = 0; i < selectedEmployeesID.length; i++) {
            for (var j = 0; j < employeesList.length; j++) {
                if (employeesList[j].id == selectedEmployeesID[i]) {
                    $("#names_header_for_table_union_row").append("<td><p class = 'rotateText'>" + employeesList[j].name + "</p></td>");
                }
            }
        }
    }
}

//function saveToExcel(){
//    $("#super_viewer_employees_hours_table").table2excel({ 
//    exclude: ".noExl", // exclude CSS class
//    filename: "HoursSummarizedByEmployees" //do not include extension
//    });
//}

function saveDetailedReportToExcel() {
    var from = document.getElementById("filterDateFrom").value; //string date format dd-mm-yyyy
    var to = document.getElementById("filterDateTo").value; //string date format dd-mm-yyyy
    var from_date_format = $("#filterDateFrom").datepicker("getDate"); //date format
    var to_date_format = $("#filterDateTo").datepicker("getDate"); //date format
    var groupBy = $("#select_work_group_by option:selected").val();

    // calling spinner
    var target = document.getElementById('id_divScrollPart');
    var spinner = new Spinner(opts).spin(target);
    $.getJSON("saveDetailedReportToExcel",
            {dateFrom: from,
                dateTo: to,
                selectedEmployeesID: JSON.stringify(selectedEmployeesID),
                groupBy: groupBy},
            function (json) {
                if (json.result == "loaded") {
//                    $("#post_message").show();
//                    $("#post_message_span").text(" The Excel file has been \n successfully generated ");
                    spinner.stop();
                    document.location = json.webPath;
                } else {
                    spinner.stop();
                    $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE_SAVE_TO_EXCEL);
                    console.log(lang.ERROR_SAVE_TO_EXCEL + json.result);
                }
            })
            .fail(function () {
                spinner.stop();
                $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
            });
}

function prepareSelectYear() {
    var currentYear = (new Date).getFullYear();
    $("#select_PlanCompare").append("<option value='" + currentYear + "' selected>" + currentYear + "</option>");
    for (var i = currentYear - 1; i > currentYear - 10; i--) {
        $("#select_PlanCompare").append("<option value='" + i + "'>" + i + "</option>");
    }
}

function planCompareLoad() {
    var yearPlanCompare = $("#select_PlanCompare").val();

    // calling spinner
    var target = document.getElementById('id_divScrollPart');
    var spinner = new Spinner(opts).spin(target);

    $.getJSON("planCompare",
            {yearPlanCompare: yearPlanCompare},
            function (planCompare) {
                populatePlanCompareTable(planCompare);
                spinner.stop();
            })
            .fail(function () {
                spinner.stop();
                $.alertable.alert(lang.GET_JSON_ERROR_MESSAGE);
            });
}

function populatePlanCompareTable(planCompare) {
    //clean the table element
    $("#planCompare_table").text("");

    $("#planCompare_table").append("<tr class='tr_head'>\n\
                         <td>" + lang.work_title + "</td>\n\
                         <td>" + lang.entered_hours + "</td>\n\
                         <td>" + lang.hours_in_period + "</td>\n\
                         <td>" + lang.done + "</td>\n\
                         </tr>");

    var enteredHoursSum = 0;
    var hoursInPlanSum = 0;
    $.each(planCompare, function (indexInArray, value) {
        $("#planCompare_table").append("<tr><td class = 'firstColumnPlanCompare' title='" + value.workTitle + "'>" + value.workTitle + "</td>" +
                "<td>" + value.completedHours + "</td>" +
                "<td>" + ((value.planHours === null) ? lang.not_exist : value.planHours) + "</td>" +
                "<td>" + ((value.planHours === null || value.planHours === 0) ? lang.not_define : (value.completedHours / value.planHours * 100).toFixed(2) + "%") + "</td></tr>");
        enteredHoursSum += value.completedHours;
        hoursInPlanSum += value.planHours;
    });
    $("#planCompare_table").append("<tr><td class = 'firstColumnPlanCompareEnd'>" + lang.amount + " </td>" +
            "<td>" + enteredHoursSum + "</td>" +
            "<td>" + hoursInPlanSum + "</td>" +
            "<td>" + ((hoursInPlanSum === 0) ? lang.not_define : (enteredHoursSum / hoursInPlanSum * 100).toFixed(2) + "%") + "</td></tr>");

    if ($("#super_viewer_employees_hours_table").text() !== "") {
        $(".lineToSeparateTables2").show();
        $("#planCompare_table").css('margin-top', '30px');
    }

}