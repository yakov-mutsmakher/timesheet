Timesheet - Java project using the web interface for accounting of employees� working hours. This program allows:   
- Employees to add working hours to the existing hierarchy of types of activities;
- To update and delete the imputed hours, with a possibility to get information about the edited working hours, add new types of activities;
- Getting statistic about employee�s working hours situation through calendar filter or types of activities filter, comparison with the planned number of hours for the employee;
- Login to program using active directory authentication and DB authorization;
- Each employee has the role, building roles tree from observer roles, division into departments, department heads, users;
- There is �another reports page� available only for observer roles to get reports from his roles subtree using filters by calendar, types of activities, employees;
- Extract a detailed report to excel with the ability to group selected types of activities;
- Comparison of the current situation of filled hours with data from department year plan;
- There is �administration page� available only for respective roles to edit roles, to add new employees from active directory, to set roles, to load new work entities and year plan from annual plan excel file;
- Switch interface languages.

For starting timesheet_test, use:
login: admin
pass: 1